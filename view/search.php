<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Compiled and minified CSS -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">

    <!-- Compiled and minified JavaScript -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>

    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

    <link rel="stylesheet" href="./css/style.css">
    <link rel="stylesheet" href="./css/login.css">
    <link rel="stylesheet" href="css/footer.css">
    <link rel="stylesheet" href="./css/index.css">
    <style>
        .card {
        padding-top: 10px;
        /* min-height: 400px; */
        cursor: pointer;
        box-shadow: none;
        }

        .card:hover {
        border-radius: 2px;
        --webkit-box-shadow: 0 2px 2px 0 rgba(0, 0, 0, 0.14),
            0 3px 1px -2px rgba(0, 0, 0, 0.12), 0 1px 5px 0 rgba(0, 0, 0, 0.2);
        box-shadow: 0 2px 2px 0 rgba(0, 0, 0, 0.14),
            0 3px 1px -2px rgba(0, 0, 0, 0.12), 0 1px 5px 0 rgba(0, 0, 0, 0.2);
        /* transition: box-shadow 0.25s, -webkit-box-shadow 0.25s; */
        }

        .card .card-image {
        max-width: 240px;
        max-height: 240px;
        margin: 0 auto;
        }

        .card .card-product-name {
        color: #000;
        }
        .card .card-product-name:hover {
        color: var(--primary-orange);
        }

        .card .card-product-price {
        font-weight: 700;
        }

        .card .rating {
        color: var(--primary-orange);
        }
    </style>
    <title>Tìm kiếm</title>
</head>

<body>
    <?php include('./header.php'); ?>
    <?php
        $servername = "localhost";
        $username = "root";
        $password = "";
        $dbname = "demo";
        // $dbname = "db_web_assignment";
        if (isset($_GET['search'])) {
            $name = $_GET['search'];
        } else {
            $name = '';
        }
        $conn=mysqli_connect($servername,$username,$password,$dbname);
        // Check connection
        if (mysqli_connect_error()){
            echo "Failed to connect to MySQL: " . mysqli_connect_error();
            die();
        }

        $sql = "SELECT * from product WHERE name LIKE '%$name%' " ;
        $total_result_sql = "SELECT COUNT(*) from product WHERE name LIKE '%$name%' " ;
        $result = mysqli_query($conn,$total_result_sql);
        $total_result = mysqli_fetch_array($result)[0];
        $res_data = mysqli_query($conn,$sql);?>
        <div class="container">
        <div class="row">
        <ul class="mybreadcrumb">
            <li><a href="./homepage.php">Trang chủ</a></li>
            <li><span>Kết quả tìm kiếm với từ khóa "<?php echo $name ?>"</span></li>
        </ul>
        <div style='padding-left: 16px'>Có tổng cộng <span style='font-weight: bold'><?php echo $total_result ?></span> kết quả phù hợp!</div>
            <?php
                while($row = mysqli_fetch_array($res_data)){
                    ?>
                    <div class="col s6 m6 l3 center-align">
                        <div class="card hoverable">
                            <div class="card-image">
                                <img src="<?php echo $row['thumbnailurl']; ?>" alt="">
                                <!-- <a class="btn-floating halfway-fab waves-effect waves-light red"><i class="material-icons">add</i></a> -->
                            </div>
                            <div class="card-content" style="padding:24px 20px">
                                <a href="#" class="card-product-name"><?php echo $row['name']; ?></a>
                                <div class="rating">
                                    <?php for($i = 1; $i <= (int)$row['rating']; $i++) echo '<i class="material-icons">star</i>';?>
                                    <?php if($row['rating'] - (int)$row['rating'] >= 0.5) echo '<i class="material-icons">star_half</i>';?>
                                    <?php for($i = round($row['rating']); $i < 5; $i++) echo '<i class="material-icons">star_border</i>';?>
                                </div>
                                <p class="card-product-price"><?php echo number_format(($row['price']),0,",",".")." ";?>₫</p>
                            </div>
                        </div>
                    </div>
                <?php
                }
                ?>
        </div>
        </div>
        <?php include('./footer.php'); ?>
    </body>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"
        integrity="sha512-bLT0Qm9VnAYZDflyKcBaQ2gg0hSYNQrJ8RilYldYQ1FxQYoCLtUjuuRuZo+fjqhx/qtq/1itJ0C2ejDxltZVFg=="
        crossorigin="anonymous">
    </script>
    <script src="./js/jquerymatchHeight.js"></script>
    <script>
        $(function () {
            $('.card').matchHeight();
            $('.card-image').matchHeight();
        });
    </script>
</html>