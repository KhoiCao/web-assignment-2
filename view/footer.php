<footer class="footer">
        <div class="container-products">
            <div class="row">
    
                <div class="col s12 m3 l5">
                    <div class="footer-widget">
                        <h4 class="footer-logo">About shop</h4>
                        <p>
                            Moza chuyên kinh doanh sản phẩm âm nhạc thời thượng đẳng cấp. Với tiêu chi CHẤT LƯỢNG cho từng khách hàng, chúng tôi
                            đang nỗ lực để ngày một hoàn thiện,cảm ơn quý khách hàng đã luôn đồng hành hỗ trợ.
                        </p>
                    </div>
                </div>
                <div class="col l7 m9">
                    <div class="col s12 m5 l5">
                        <div class="footer-widget">
                            <h4>Hỗ trợ</h4>
                            <ul class="list-menu">
                                <li>Vui lòng liên hệ với chúng tôi nếu bạn có bất kỳ vấn đề gì.</li>
                                <li><strong>Địa chỉ: </strong>
                                    Phòng 214, Chung cư B3, Phường Quan Hoa , Cầu Giấy, Hà Nội
                                </li>
                                <li><strong>Email: </strong>
                                    <a href="mailto:songvangvietnam@gmail.com">songvangvietnam@gmail.com </a>
                                </li>
                                <li><strong>SĐT: </strong>
                                    <a href="#"> 0902068068 </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="col s12 m3 l3">
                        <div class="footer-widget">
                            <h4>Hướng dẫn</h4>
                            <ul class="list-menu">
                                <li><a href="./index.html">Trang chủ</a></li>
                                <li><a href="./about.html">Giới thiệu</a></li>
                                <li><a href="./products.html">Sản phẩm</a></li>
                                <li><a href="./products.html">Guitar</a></li>
                                <li><a href="./products.html">Piano-Organs</a></li>
                                <li><a href="./products.html">Trống-Bộ gõ</a></li>
                                <li><a href="./contact.html">Liên hệ</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col s12 m4 l4">
                        <div class="footer-widget">
                            <h4>Thanh toán</h4>
                            <ul class="list-menu">
                                <li><a href="/maestro"><img src="./images/icons/maestro.png" alt="maestro"></a></li>
                                <li><a href="/paypal"><img src="./images/icons/paypal.png" alt="paypal"></a></li>
                                <li><a href="/visa"><img src="./images/icons/visa.png" alt="visa"></a></li>
                                <li><a href="/cirrus"><img src="./images/icons/cirrus.png" alt="cirrus"></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col s12">
                    <a href="/cirrus"><img src="./images/icons/icon-social.png" alt="social"></a>
                </div>
            </div>
        </div>
    </footer>