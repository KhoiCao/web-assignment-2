function validateInput() {
    var age, phone, firstname, lastname, address;
    age = myForm.age.value;
    phone = myForm.phone.value;
    firstname = myForm.firstname.value;
    lastname = myForm.lastname.value;
    address = myForm.address.value;

    var regName = /[a-zA-Z]/;
    var firstnameError = document.getElementById("firstnameError");
    if(firstname !== ''){
      if(regName.test(firstname) == false){
        firstnameError.innerHTML = "Họ của bạn không hợp lệ!";
        return false;
      } else {
        firstnameError.innerHTML = "";
      }
    } else {
      firstnameError.innerHTML = "Bạn chưa điền họ!";
      return false;
    }

    var lastnameError = document.getElementById("lastnameError");
    if(lastname !== ''){
      if(regName.test(lastname) == false){
        lastnameError.innerHTML = "Tên của bạn không hợp lệ!";
        return false;
      } else {
        lastnameError.innerHTML = "";
      }
    } else {
      lastnameError.innerHTML = "Bạn chưa điền tên!";
      return false;
    }

    var ageError = document.getElementById("ageError");
    if (isNaN(age) || age < 1) {
      ageError.innerHTML = "Tuổi của bạn không hợp lệ";
      return false;
    } else {
      ageError.innerHTML = "";
    }

    var addressError = document.getElementById("addressError");
    if (address == '') {
      addressError.innerHTML = "Địa chỉ của bạn không hợp lệ";
      return false;
    } else {
      addressError.innerHTML = "";
    }

    var vnf_regex = /((09|03|07|08|05)+([0-9]{8})\b)/g;
    var phoneError = document.getElementById("phoneError");
    if(phone !==''){
        if (vnf_regex.test(phone) == false) {
          phoneError.innerHTML = 'Số điện thoại của bạn không đúng định dạng!';
          return false;
        } else {
          phoneError.innerHTML = "";
        }
    } else {
      phoneError.innerHTML = 'Bạn chưa điền số điện thoại!';
    //   return false;
    }
    
    return true;
}
