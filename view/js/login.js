const loginBtn = document.getElementById('login-btn');
loginBtn.addEventListener('click', function (e) {
    const email = document.querySelector('#email').value;
    const password = document.querySelector('#password').value;
    e.preventDefault();
    loginBtn.setAttribute('disabled','true');
    $.post('../controller/login.php', {email: email, password: password} , function (data, status) {
        data = JSON.parse(data);
        loginBtn.removeAttribute('disabled');
        if(data.success) {
            (data.isAdmin == 1) ? window.location.replace('../view/admin/listuser.php') : window.location.replace('../view/homepage.php');
        }
        else alert('Incorrect username or password');
    })
})