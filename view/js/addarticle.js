function previewFile() {
  const preview = document.querySelector("img#imageUpload");
  const file = document.querySelector("input[type=file]").files[0];
  const reader = new FileReader();

  reader.addEventListener(
    "load",
    function () {
      // convert image file to base64 string
      preview.src = reader.result;
      preview.alt = "image-product";
    },
    false
  );

  if (file) {
    reader.readAsDataURL(file);
  }
}


function returnToPreviousPage() {
    window.history.back();
}

function getExtension(filename) {
    let parts = filename.split('.');
    return parts[parts.length - 1];
}

function isImage(filename) {
    let ext = getExtension(filename);
    switch (ext.toLowerCase()) {
      case 'jpg':
      case 'gif':
      case 'bmp':
      case 'png':
        //etc
        return true;
    }
    return false;
}

const checkForm = () => {
    let form = document.forms[0];
    let titleP = form.querySelector('input[name="title"]').value;
    let contentP = document.getElementById('desc').html();
    let filePath = form.querySelector('input[id="filesImage"]').value;
    let partsFile = filePath.split('\\');
    let fileName = partsFile[partsFile.length - 1]

    if(titleP.length < 3 ||  titleP.length > 50) {
        alert("Tên bài viết tối thiểu 3 ký tự và không quá 50 ký tự");
        // returnToPreviousPage();
        return false;
    }
    else if (contentP.length == 0) {
        alert("Nội dung không được để trống");
    }
    else if (!isImage(fileName)) {
        alert("File phải là ảnh");
        // returnToPreviousPage();
        return false;
    } else {
        alert("Thêm bài viết thành công");
        return true;
    }
}

