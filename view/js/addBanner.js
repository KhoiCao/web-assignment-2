function previewFile() {
    const preview = document.querySelector("img#imageUpload");
    const file = document.querySelector("input[type=file]").files[0];
    const reader = new FileReader();
  
    reader.addEventListener(
      "load",
      function () {
        // convert image file to base64 string
        preview.src = reader.result;
        preview.alt = "image-product";
      },
      false
    );
  
    if (file) {
      reader.readAsDataURL(file);
    }
}

function getExtension(filename) {
    let parts = filename.split('.');
    return parts[parts.length - 1];
}

function isImage(filename) {
    let ext = getExtension(filename);
    switch (ext.toLowerCase()) {
      case 'jpg':
      case 'gif':
      case 'bmp':
      case 'png':
        //etc
        return true;
    }
    return false;
}

const checkForm = () => {
    let form = document.forms[0];
    let filePath = form.querySelector('input[id="fileImage"]').value;
    let partsFile = filePath.split('\\');
    let fileName = partsFile[partsFile.length - 1]

    if (!isImage(fileName)) {
        alert("File phải là ảnh");
        return false;
    } else {
        alert("Thêm banner thành công");
        return true;
    }
}