function previewFile() {
    const preview = document.querySelector("img#imageUpload");
    const file = document.querySelector("input[type=file]").files[0];
    const reader = new FileReader();
  
    reader.addEventListener(
      "load",
      function () {
        // convert image file to base64 string
        preview.src = reader.result;
        preview.alt = "image-product";
      },
      false
    );
  
    if (file) {
      reader.readAsDataURL(file);
    }
  }
  
  
  function returnToPreviousPage() {
      window.history.back();
  }
  
  function getExtension(filename) {
      let parts = filename.split('.');
      return parts[parts.length - 1];
  }
//   let idP = form.querySelector('input[name="id"]').value;
//   let tagP = form.querySelectorAll('input[name="tag[]"]');
//   Array.from(tagP).forEach(function(tag){
//       tag.addEventListener('click', function(e){
//         if(e.target.checked == true) {
            
//         } else {

//         }
//       }); 
//   });
  
  function isImage(filename) {
      let ext = getExtension(filename);
      switch (ext.toLowerCase()) {
        case 'jpg':
        case 'gif':
        case 'bmp':
        case 'png':
          //etc
          return true;
      }
      return false;
  }
  
  const checkForm = () => {
      let form = document.forms[0];
      let nameP = form.querySelector('input[name="name"]').value;
      let priceP = form.querySelector('input[name="price"]').value;
      let qtyP = form.querySelector('input[name="qty"]').value;
      let tagP = form.querySelectorAll('input[name="tag[]"]:checked');
      let typeP = form.querySelector('input[name="typeProduct"]:checked');
      let filePath = form.querySelector('input[id="filesImage"]').value;
      let partsFile = filePath.split('\\');
      let fileName = partsFile[partsFile.length - 1]
  
      if(nameP.length < 3 ||  nameP.length > 50) {
          alert("Tên sản phẩm tối thiểu 3 ký tự và không quá 20 ký tự");
          returnToPreviousPage();
          return false;
      } else if (priceP == 0) {
          alert("Giá sản phẩm phải lớn hơn 0");
          returnToPreviousPage();
          return false;
      } else if (priceP % 1 != 0) {
          alert("Giá sản phẩm phải là số nguyên");
          returnToPreviousPage();
          return false;
      } else if (qtyP == 0) {
          alert("Số lượng sản phẩm phải lớn hơn 0");
          returnToPreviousPage();
          return false;
      } else if (qtyP % 1 != 0) {
          alert("Số lượng sản phẩm phải là số nguyên");
          returnToPreviousPage();
          return false;
      } else if (tagP.length == 0) {
          alert("Hãy chọn tag sản phẩm");
          returnToPreviousPage();
          return false;
      } else if (typeP == null ) {
          alert("Hãy chọn loại sản phẩm");
          returnToPreviousPage();
          return false;
      } else if (fileName) {
          if (!isImage(fileName)) {
            alert("File phải là ảnh");
            returnToPreviousPage();
            return false;
          } else {
            alert("Cập nhật sản phẩm thành công");
            return true;
          }
      } else {
          alert("Cập nhật sản phẩm thành công");
          return true;
      }
  }
  
  