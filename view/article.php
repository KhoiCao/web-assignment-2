<?php 
    include('../model/article.php');
    include('../model/user.php');
    $articleModel = new ArticleModel();
    $userModel = new UserModel();
    if(isset($_GET['id'])) $articleId = $_GET['id'];
    else header('location: ./articles.php');
    $article = $articleModel->getArticle($articleId);
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Compiled and minified CSS -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">
    
    <!-- Compiled and minified JavaScript -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>
    
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

    <link rel="stylesheet" href="./css/style.css">
    <link rel="stylesheet" href="./css/footer.css">
    <style>
        body{
            display: flex;
            flex-direction: column;
            min-height: 100vh;
        }
        main {
            flex-grow: 1;
        }
        .article-title{
            font-size: 34px;
        }
        .article-thumbnail {
            max-width: 400px;
        }
        .article-content {
            margin-top: 40px;
            font-size: 16px;
        }
        .article-comments{
            margin-top: 40px;
        }
        .article-comment-box {
            border: 1px solid #e9e9e9;
            display: inline-block;
            /* margin-bottom: 30px; */
            width: 100%;
            }
        .user-avatar {
            text-align: center;
            margin-bottom: 0;
            width: 50px;
            float: left;
            position: relative;
            margin-left: 10px
        }
        .comment-info {
            text-align: left;
            margin-left: 30px;
            float: left;
            width: calc(100% / 3 * 2 - 30px);
            background: transparent;
            border-top: none;
            padding: 5px 0;
        }
        #loadmore, #comment-btn {
            background-color: var(--primary-orange);
            margin: 10px 0;
        }
        h1 {
            font-size: 30px;
        }
        h2 {
            font-size: 26px;
        }
        h3 {
            font-size: 22px;
        }
    </style>
    <title><?php echo $article['title']; ?></title>
</head>
<body>
    <?php 
        include('./header.php');
        $page = 1;
        $page_size = 2;
        $offset = ($page-1) * $page_size;
        $articlecomments = $articleModel->getArticleCommentsByRange($articleId, $offset, $page_size);
    ?>
    <main>
        <div class="article-content">
            <h1 class="article-title center-align"><?php echo $article['title']; ?> </h1>
            <div class="center-align"><img class="responsive-image article-thumbnail" src=<?php echo $article['thumbnailurl'];?> alt=""></div>
            <div class="article-content container">
                <?php echo $article['content']; ?>
            </div>
            <div id="comment-section" class="article-comments container">
                <h3 style="font-size: 20px;">Bình luận: </h3>
                <?php foreach($articlecomments as $comment) { $user = $userModel->getUser($comment['userid']); ?>
                    <div class="col s12 article-comment-box">
                        <div class="user-avatar">
                            <img src=<?php if ($user['avatarurl'] != '') echo "./images/users/".$user['avatarurl']; else echo "https://www.unionmedicalcentre.com.au/wp-content/uploads/2019/04/avatar-male.jpg";?> alt="" class="responsive-img circle">
                        </div>
                        <div class="comment-info">
                            <div class="user-name">
                                <?php echo $user['firstname']." ".$user['lastname']; ?>
                            </div>
                            <div class="comment-content">
                                <?php echo $comment['content']; ?>
                            </div>
                        </div>
                    </div>
                <?php } ?>

            </div>
            <div class="container center-align"><button id="loadmore" class="btn waves-effect waves-light">Tải thêm bình luận</button></div>
            <div class="write-comment container">
                <div class="row">
                    <form class="col s12" id="comment">
                        <div class="row">
                            <div class="input-field col s12">
                                <textarea id="content" class="materialize-textarea" name="content"></textarea>
                                <label for="content">Thêm bình luận</label>
                            </div>
                            <button id="comment-btn" class="btn">Bình luận</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </main>
    <?php include('./footer.php'); ?>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"
        integrity="sha512-bLT0Qm9VnAYZDflyKcBaQ2gg0hSYNQrJ8RilYldYQ1FxQYoCLtUjuuRuZo+fjqhx/qtq/1itJ0C2ejDxltZVFg=="
        crossorigin="anonymous">
    </script>
    <script>
        $('.article-nav').addClass('active');
    </script>
    <script>
        let articleId = <?php echo $articleId; ?>;
        const userId = <?php if($userId == '')  echo "''" ; else echo $userId;?>;
    </script>
    <script>
        const commentBtn = document.getElementById('comment-btn');
        commentBtn.addEventListener('click', function (e) {
            if (userId != '') {
                const content = document.querySelector('#content').value;
                e.preventDefault();
                if (content.length > 0) {
                    commentBtn.setAttribute('disabled','true');
                    $.post('../controller/addarticlecomment.php', {articleid: articleId, content: content} , function (data, status) {
                        data = JSON.parse(data);
                        commentBtn.removeAttribute('disabled');
                        if(data.success) {
                            window.location.href = `./article.php?id=${articleId}`;
                        }
                        else alert('Bình luận thất bại');
                    })
                }
                else {
                    alert('Vui lòng không để trống phần bình luận');
                }
            }
            else alert('Vui lòng đăng nhập để bình luận');
            
        });
        page = 2; page_size = 2;
        offset = (page-1) * page_size;
        const loadMoreBtn = document.getElementById('loadmore');
        const commentSection = document.getElementById('comment-section');
        loadMoreBtn.addEventListener('click', function (e) {
            loadMoreBtn.setAttribute('disabled','true');
            $.post('../controller/loadmorecomments.php', {articleid: articleId, offset: offset, page_size: page_size} , function (data, status) {
                loadMoreBtn.removeAttribute('disabled');
                if(data){
                    page++;
                    offset = (page-1) * page_size;
                    commentSection.innerHTML += data;
                }
            })
        })
    </script>
</body>
</html>