<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Compiled and minified CSS -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">

    <!-- Compiled and minified JavaScript -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>

    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

    <link rel="stylesheet" href="./css/style.css">
    <link rel="stylesheet" href="./css/login.css">
    <link rel="stylesheet" href="./css/footer.css">
    <link rel="stylesheet" href="css/editprofile.css">
    <title>Thông tin người dùng</title>
</head>

<body>
    <?php
    define('SITE_ROOT', realpath(dirname(__FILE__)));
    include 'header.php';
    include '../model/user.php';
    $userModel = new UserModel;
    $user = $userModel->getUser($_SESSION['userId']);
    // Validate
    const REQUIRED_MSG = "bắt buộc!";

    $firstnameErr = $lastnameErr = $ageErr = $phoneErr = $addressErr = "";
    $firstname = $lastname = $phone = "";

    if ($_SERVER["REQUEST_METHOD"] == "POST") {

        if (isset($_POST['save_info'])) {
            $error = FALSE;
            if (empty($_POST["firstname"])) {
                $firstnameErr = "Họ của bạn là " . REQUIRED_MSG;
                $error = TRUE;
            } else {
                $firstname = test_input($_POST["firstname"]);
                if (!preg_match("/^[a-zA-Z ]*$/", $firstname)) {
                    $firstnameErr = "Họ của bạn không hợp lệ!";
                }
            }

            if (empty($_POST["lastname"])) {
                $lastnameErr = "Tên của bạn là " . REQUIRED_MSG;
                $error = TRUE;
            } else {
                $lastname = test_input($_POST["lastname"]);
                if (!preg_match("/^[a-zA-Z ]*$/", $lastname)) {
                    $lastnameErr = "Tên của bạn không hợp lệ!";
                }
            }

            if (empty($_POST["age"])) {
                $ageErr = "Tuổi của bạn là " . REQUIRED_MSG;
                $error = TRUE;
            } else {
                if (!is_numeric($_POST["age"]) || $_POST["age"] <= 0) {
                    $ageErr = "Tuổi của bạn không hợp lệ!";
                }
            }

            if (empty($_POST["phone"])) {
                $phoneErr = "Số điện thoại của bạn là " . REQUIRED_MSG;
                $error = TRUE;
            } else {
                $phone = test_input($_POST["phone"]);
                if (!preg_match("/((09|03|07|08|05)+([0-9]{8})\b)/", $phone)) {
                    $phoneErr = "Số điện thoại của bạn không hợp lệ!";
                }
            }

            if (empty($_POST["address"])) {
                $addressErr = "Địa chỉ của bạn là " . REQUIRED_MSG;
                $error = TRUE;
            } else {
                if (strlen($_POST['address']) <=0 ) {
                    $addressErr = "Địa chỉ của bạn không hợp lệ!";
                    $error = TRUE;
                }
            }

            $user["firstname"] = $_POST["firstname"];
            $user["lastname"] = $_POST["lastname"];
            $user["address"] = $_POST["address"];
            $user["age"] = $_POST["age"];
            $user["phone"] = $_POST["phone"];
            $user["email"] = $_POST["email"];
            $user["gender"] = $_POST["gender"];

            $profileImageName = time() . '-' . $_FILES["profileImage"]["name"];
            $target_dir = "\\images\\users\\";
            $target_file = SITE_ROOT . $target_dir . basename($profileImageName);

            // Upload image only if no errors
            if ($error == FALSE) {
                $userModel->updateProfile($user, $_SESSION['userId']);
                if (move_uploaded_file($_FILES["profileImage"]["tmp_name"], $target_file)) {
                    $user['avatarurl'] = $profileImageName;
                    $userModel->updateProfile($user, $_SESSION['userId']);
                }
            }
        }

    }
    $avatarUrl = $user["avatarurl"];
    $imgSrc = "";
    if (is_null($avatarUrl) || $avatarUrl == "") {
        $imgSrc = "./images/users/user-male.jpg";
    } else {
        $imgSrc = "./images/users/" . $avatarUrl;
    }
    

    function test_input($data)
    {
        $data = trim($data);
        $data = stripslashes($data);
        $data = htmlspecialchars($data);
        return $data;
    }


?>

    <div class="container">
        <div class="row">
            <form class="info" name="myForm" method="POST" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" id="profile-form" enctype="multipart/form-data" onsubmit="return validateInput()">
                <div class="col s3">
                    <?php if (!empty($msg)) : ?>
                        <div class="alert <?php echo $msg_class ?>" role="alert">
                            <?php echo $msg; ?>
                        </div>
                    <?php endif; ?>
                    <div class="form-group text-center" style="position: relative;">
                        <span class="img-div">
                            <img src="<?php echo $imgSrc; ?>" onClick="triggerClick()" id="profileDisplay">
                        </span>
                        <input type="file" name="profileImage" onChange="displayImage(this)" id="profileImage" class="form-control" style="display: none;">
                    </div>
                </div>

                <div class="col s9">
                    <div class="row">
                        <div class="input-field col s4 offset-s2">
                            <input placeholder="First Name" id="firstname" name="firstname" type="text" class="validate" value="<?php echo $user["firstname"]; ?>">
                            <label for="first_name">Họ</label>
                            <span>
                                <p id="firstnameError" class="field-error" style="color: red;"><?php echo $firstnameErr;?></p>
                            </span>
                        </div>
                        <div class="input-field col s4">
                            <input placeholder="Last name" id="lastname" name="lastname" type="text" class="validate" value="<?php echo $user["lastname"]; ?>">
                            <label for="last_name">Tên</label>
                            <span>
                                <p id="lastnameError" class="field-error" style="color: red;"><?php echo $lastnameErr;?></p>
                            </span>
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col s4 offset-s2">
                            <input placeholder="Age" id="age" name="age" type="text" class="validate" value="<?php echo $user["age"]; ?>">
                            <label for="age">Tuổi</label>
                            <span>
                                <p id="ageError" class="field-error" style="color: red;"><?php echo $ageErr;?></p>
                            </span>
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col s8 offset-s2">
                            <input placeholder="kane@vinova.com.sg" id="email" name="email" type="email" class="validate" value="<?php echo $user["email"]; ?>">
                            <label for="email">Email</label>
                            <span>
                                <p id="emailError" class="field-error" style="color: red;"></p>
                            </span>
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col s8 offset-s2">
                            <input id="address" type="text" name="address" class="validate" value="<?php echo $user["address"]; ?>">
                            <label for="address">Địa chỉ</label>
                            <span>
                                <p id="addressError" class="field-error" style="color: red;"><?php echo $addressErr;?></p>
                            </span>
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col s4 offset-s2">
                            <input id="phone" type="tel" name="phone" class="validate" value="<?php echo $user["phone"]; ?>">
                            <label for="phone">Điện thoại</label>
                            <span>
                                <p id="phoneError" class="field-error" style="color: red;"><?php echo $phoneErr;?></p>
                            </span>
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col s4 offset-s2">
                            <h6>Giới tính</h6>
                            <p>
                                <label>
                                    <input name="gender" type="radio" value="male" <?php if ($user['gender'] == 'male') echo 'checked' ?>>
                                    <span>Nam</span>
                                </label>
                            </p>
                            <p>
                                <label>
                                    <input name="gender" type="radio" value="female" <?php if ($user['gender'] == 'female') echo 'checked' ?>>
                                    <span>Nữ</span>
                                </label>
                            </p>
                        </div>
                    </div>
                    <div class="row">
                        <button class="btn waves-effect waves-light col s2 offset-s5" type="submit" name="save_info">Save

                        </button>
                    </div>
                </div>


            </form>

        </div>
    </div>

    <?php include('footer.php'); ?>

    <button class="btn-floating waves-effect waves-light" onclick="topFunction()" id="scrollTop" title="Go to top"><i class="material-icons">arrow_upward</i></button>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js" integrity="sha512-bLT0Qm9VnAYZDflyKcBaQ2gg0hSYNQrJ8RilYldYQ1FxQYoCLtUjuuRuZo+fjqhx/qtq/1itJ0C2ejDxltZVFg==" crossorigin="anonymous">
    </script>
    <script src="./js/scrollToTop.js"></script>
    <script src="./js/register.js"></script>
    <script src="js/imageClick.js"></script>
    <script>
        document.addEventListener('DOMContentLoaded', function() {
            var elems = document.querySelectorAll('.sidenav');
            var instances = M.Sidenav.init(elems);
        });
    </script>
    <script>
        if (window.history.replaceState) {
            window.history.replaceState(null, null, window.location.href);
        }
    </script>
    <script src="js/validateProfile.js"></script>
</body>

</html>
