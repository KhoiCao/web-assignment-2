<!-- <html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Đăng tải bài viết</title> -->
    <!-- Compiled and minified CSS -->
    <!-- <link
      rel="stylesheet"
      href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css"
    />
    <script src="//cdn.ckeditor.com/4.15.1/standard/ckeditor.js"></script> -->

    <!-- Compiled and minified JavaScript -->
    <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>
    <link
      href="https://fonts.googleapis.com/icon?family=Material+Icons"
      rel="stylesheet"
    /> -->
    <!-- <link
      rel="stylesheet"
      href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/css/all.min.css"
    /> -->
    <!-- <link rel="stylesheet" href="./css/style.css">
    <link rel="stylesheet" href="./css/index.css">
    <link rel="stylesheet" href="./css/footer.css">
    <script
      src="https://code.jquery.com/jquery-3.5.1.min.js"
      integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0="
      crossorigin="anonymous"
    ></script>
  </head>
  <body>
    
    <div class="container">
      <h2 class="center" style="font-size: 35px;">Đăng tải bài viết</h2>
      <div class="row">
        <form enctype="multipart/form-data" class="col s12" onsubmit="return checkForm();" method="post" action="../../controller/addProduct.php">
          <div class="row">
            <div class="col s12 input-field">
              <label for="name">Tên sản phẩm:* </label>
              <input type="text" id="name" name="name" required/>
            </div>
          </div>
          <div class="" style="margin-bottom: 20px">
            <label style="font-size: 16px">Mô tả sản phẩm</label>
            <textarea name="desc" id="desc"></textarea>
            <script>
              CKEDITOR.replace("desc");
            </script>
          </div>
          <div class="row">
            <div class="submit-container center" style="margin-top: 20px;">
              <button class="btn waves-effect wave-light" type="submit" name="submit">
                Đăng<i class="material-icons right">send</i>
              </button>
            </div>
          </div>
        </form>
      </div>
    </div>
  </body>
</html> -->

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Compiled and minified CSS -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">
    
    <!-- Compiled and minified JavaScript -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>
    
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

    <script src="//cdn.ckeditor.com/4.15.1/standard/ckeditor.js"></script>

    <link rel="stylesheet" href="./css/style.css">
    <link rel="stylesheet" href="./css/footer.css">
    <title>Đăng bài viết</title>
</head>
<body>
    <?php 
      include('./header.php');
      if(!isset($_SESSION['userId'])) header('location: ./articles.php');
    ?>
    <div class="container">
      <h2 class="center" style="font-size: 35px;">Đăng tải bài viết</h2>
      <div class="row">
        <form enctype="multipart/form-data" class="col s12" onsubmit="return checkForm();" method="post" action="../controller/addarticle.php">
          <div class="row">
            <div class="col s12 input-field">
              <label for="title">Tên bài viết:* </label>
              <input type="text" id="title" name="title" required/>
            </div>
          </div>
          <div class="row">
            <div class="col s12 file-field input-field">
              <div class="btn">
                <span>Tải hình ảnh</span>
                <input
                  id="filesImage"
                  type="file"
                  name='fileUpload'
                  onchange="previewFile()"
                  accept="image/*"
                  required
                />
              </div>
              <div class="file-path-wrapper">
                <input
                  class="file-path validate"
                  type="text"
                  placeholder="Thêm thumbnail cho bài viết"
                />
              </div>
            </div>
            <img
              class="col s4 offset-s4 responsive-img center-align"
              id="imageUpload"
            />
            <span class="col s4"></span>
          </div>
          <div class="" style="margin-bottom: 20px">
            <label style="font-size: 16px">Nội dung: </label>
            <textarea name="content" id="desc"></textarea>
            <script>
              CKEDITOR.replace("desc");
            </script>
          </div>
          <div class="row">
            <div class="submit-container center" style="margin-top: 20px;">
              <button class="btn waves-effect wave-light" type="submit" name="submit">
                Đăng<i class="material-icons right">send</i>
              </button>
            </div>
          </div>
        </form>
      </div>
    </div>
    <?php include('./footer.php'); ?>
    <script src="./js/addarticle.js"></script>
</body>
</html>