<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Compiled and minified CSS -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">

    <!-- Compiled and minified JavaScript -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>

    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

    <link rel="stylesheet" href="./css/style.css">
    <link rel="stylesheet" href="./css/login.css">
    <link rel="stylesheet" href="./css/footer.css">
    <title>Đăng ký tài khoản</title>
</head>

<body>
    <?php include('./header.php'); ?>
    <div class="container">
        <div class="login-form">
            <h5 class="form-title center-align">Tạo tài khoản</h5>
            <form class="" method="POST">
                <div class="row">
                    <div class="input-field col s12">
                        <input id="fname" name="fname" type="text" class="validate">
                        <label for="fname">Họ (ít nhất 1 ký tự, chỉ được chứa chữ cái)</label>
                    </div>
                    <div class="input-field col s12">
                        <input id="lname" name="lname" type="text" class="validate">
                        <label for="lname">Tên (ít nhất 1 ký tự, chỉ được chứa chữ cái)</label>
                    </div>
                </div>
                <div class="row">
                    <div class="input-field col s12">
                        <input id="email" name="email" type="email" class="validate">
                        <label for="email">Email</label>
                    </div>
                </div>
                <div class="row">
                    <div class="input-field col s12">
                        <input id="password" name="password" type="password" class="validate">
                        <label for="password">Mật khẩu (ít nhất 6 ký tự)</label>
                    </div>
                </div>
                <div class="row">
                    <div class="input-field col s12">
                        <input id="retype_password" type="password" class="validate">
                        <label for="retype_password">Nhập lại mật khẩu</label>
                    </div>
                </div>

                <div class="center-align">
                    <button id="register-btn" class="btn waves-effect waves-light" type="submit" name="action">Đăng kí
                        <i class="material-icons right">send</i>
                    </button>
                </div>
            </form>
            <div class="support_options center-align">
                <div>
                    <a href="./login.php">Đã có tài khoản? Đăng nhập tại đây</a>
                </div>
            </div>
        </div>
    </div>

    <?php include('./footer.php'); ?>

    <button class="btn-floating waves-effect waves-light" onclick="topFunction()" id="scrollTop" title="Go to top"><i
            class="material-icons">arrow_upward</i></button>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"
        integrity="sha512-bLT0Qm9VnAYZDflyKcBaQ2gg0hSYNQrJ8RilYldYQ1FxQYoCLtUjuuRuZo+fjqhx/qtq/1itJ0C2ejDxltZVFg=="
        crossorigin="anonymous">
    </script>
    <script src="./js/scrollToTop.js"></script>
    <script src="./js/register.js"></script>
    <script>
        document.addEventListener('DOMContentLoaded', function () {
            var elems = document.querySelectorAll('.sidenav');
            var instances = M.Sidenav.init(elems);
        });
    </script>
</body>

</html>