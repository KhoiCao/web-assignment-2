<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Compiled and minified CSS -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">
    
    <!-- Compiled and minified JavaScript -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>
    
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

    <link rel="stylesheet" href="./css/style.css">
    <link rel="stylesheet" href="./css/index.css">
    <link rel="stylesheet" href="./css/footer.css">
    <title>Trang chủ</title>
</head>
<body>
    <?php 
        include('./header.php');
        include('../model/product.php');
        include('../model/banner.php');
        $productModel = new ProductModel();
        $bannerModel = new BannerModel();
        $banners = $bannerModel->getAllBanners();
        $from = 0; $limit = 8;
        $showcaseProducts = $productModel->getProductsByRange($from, $limit);
        $guitars = $productModel->getProductsByType('Guitar', $from, $limit);
        $pianos = $productModel->getProductsByType('Piano', $from, $limit);
        $violins = $productModel->getProductsByType('Violin', $from, $limit);
        $amplys = $productModel->getProductsByType('Amply', $from, $limit);
        $keyboards = $productModel->getProductsByType('Keyboard', $from, $limit);
        $favorites = $productModel->getBestSoldProducts();
    ?>

    <div class="carousel carousel-slider">
        <!-- <a class="carousel-item" href="#one!"><img src="./images/sliders/slider_1.jpg" alt="" class="responsive-img"></a>
        <a class="carousel-item" href="#two!"><img src="./images/sliders/slider_2.jpg" alt="" class="responsive-img"></a> -->
        <?php foreach($banners as $banner) {?>
            <a class="carousel-item" href="#"><img src="<?php echo $banner['bannerurl'];?>" alt="" class="responsive-img"></a>
        <?php } ?>
    </div>

    <div class="showcase-images">
        <div class="row container">
            <div class="col s12 m4">
                <a href="#"><img class="responsive-img" src="./images/products/bn1.jpg" alt=""></a>
            </div>
            <div class="col s12 m4">
                <a href="#"><img class="responsive-img" src="./images/products/bn2.jpg" alt=""></a>
            </div>
            <div class="col s12 m4">
                <a href="#"><img class="responsive-img" src="./images/products/bn3.jpg" alt=""></a>
            </div>
        </div>
    </div>
    <div class="products-showcase">
        <div class="container row">
            <div style="font-size: 1.2em;" class="col s12 center-align">
                <p>Tất cả sản phẩm</p>
            </div>
            <div class="col s12 container">
                <ul class="tabs vertical-tabs">
                    <li class="tab col s12 m2 offset-m1"><a href="#all"><span>Tất cả sản phẩm</span></a></li>
                    <li class="tab col s12 m2"><a href="#guitar"><span>Guitar</span></a></li>
                    <li class="tab col s12 m2"><a href="#keyboard"><span>Keyboard</span></a></li>
                    <li class="tab col s12 m2"><a href="#piano"><span>Piano</span></a></li>
                    <li class="tab col s12 m2"><a href="#amply"><span>Amply - Effect</span></a></li>
                </ul>
            </div>
            <div id="all" class="col s12 row center-align">
                <?php foreach($showcaseProducts as $product) { ?>
                    <div class="col s6 m4 l3">
                        <div class="card hoverable">
                            <div class="card-image">
                                <img src=<?php echo $product['thumbnailurl']; ?> alt="">
                            </div>
                            <div class="card-content">
                                <a href="<?php echo './productInfo.php?id='.$product['id'];?>" class="card-product-name"> <?php echo $product['name']; ?> </a>
                                <div class="rating">
                                    <?php for($i = 1; $i <= (int)$product['rating']; $i++) echo '<i class="material-icons">star</i>';?>
                                    <?php if($product['rating'] - (int)$product['rating'] >= 0.5) echo '<i class="material-icons">star_half</i>';?>
                                    <?php for($i = round($product['rating']); $i < 5; $i++) echo '<i class="material-icons">star_border</i>';?>

                                </div>
                                <p class="card-product-price"><?php echo number_format(($product['price']),0,",",".")." "; ?>₫</p>
                            </div>
                        </div>
                    </div>
                <?php } ?>
            </div>
            <div id="guitar" class="col s12 row center-align">
                <?php foreach($guitars as $guitar) { ?>
                    <div class="col s6 m4 l3">
                        <div class="card hoverable">
                            <div class="card-image">
                                <img src=<?php echo $guitar['thumbnailurl']; ?> alt="">
                            </div>
                            <div class="card-content">
                                <a href="<?php echo './productInfo.php?id='.$guitar['id'];?>" class="card-product-name"> <?php echo $guitar['name']; ?> </a>
                                <div class="rating">
                                    <?php for($i = 1; $i <= (int)$guitar['rating']; $i++) echo '<i class="material-icons">star</i>';?>
                                    <?php if($guitar['rating'] - (int)$guitar['rating'] >= 0.5) echo '<i class="material-icons">star_half</i>';?>
                                    <?php for($i = round($guitar['rating']); $i < 5; $i++) echo '<i class="material-icons">star_border</i>';?>

                                </div>
                                <p class="card-product-price"><?php echo number_format(($guitar['price']),0,",",".")." "; ?>₫</p>
                            </div>
                        </div>
                    </div>
                <?php } ?>
            </div>
            <div id="keyboard" class="col s12 row center-align">
                <?php foreach($keyboards as $keyboard) { ?>
                    <div class="col s6 m4 l3">
                        <div class="card hoverable">
                            <div class="card-image">
                                <img src=<?php echo $keyboard['thumbnailurl']; ?> alt="">
                            </div>
                            <div class="card-content">
                                <a href="<?php echo './productInfo.php?id='.$keyboard['id'];?>" class="card-product-name"> <?php echo $keyboard['name']; ?> </a>
                                <div class="rating">
                                    <?php for($i = 1; $i <= (int)$keyboard['rating']; $i++) echo '<i class="material-icons">star</i>';?>
                                    <?php if($keyboard['rating'] - (int)$keyboard['rating'] >= 0.5) echo '<i class="material-icons">star_half</i>';?>
                                    <?php for($i = round($keyboard['rating']); $i < 5; $i++) echo '<i class="material-icons">star_border</i>';?>

                                </div>
                                <p class="card-product-price"><?php echo number_format(($keyboard['price']),0,",",".")." "; ?>₫</p>
                            </div>
                        </div>
                    </div>
                <?php } ?>
            </div>
            <div id="piano" class="col s12 row center-align">
                <?php foreach($pianos as $piano) { ?>
                    <div class="col s6 m4 l3">
                        <div class="card hoverable">
                            <div class="card-image">
                                <img src=<?php echo $piano['thumbnailurl']; ?> alt="">
                            </div>
                            <div class="card-content">
                                <a href="<?php echo './productInfo.php?id='.$piano['id'];?>" class="card-product-name"> <?php echo $piano['name']; ?> </a>
                                <div class="rating">
                                    <?php for($i = 1; $i <= (int)$piano['rating']; $i++) echo '<i class="material-icons">star</i>';?>
                                    <?php if($piano['rating'] - (int)$piano['rating'] >= 0.5) echo '<i class="material-icons">star_half</i>';?>
                                    <?php for($i = round($piano['rating']); $i < 5; $i++) echo '<i class="material-icons">star_border</i>';?>

                                </div>
                                <p class="card-product-price"><?php echo number_format(($piano['price']),0,",",".")." "; ?>₫</p>
                            </div>
                        </div>
                    </div>
                <?php } ?>
            </div>
            <div id="amply" class="col s12 row center-align">
                <?php foreach($amplys as $amply) { ?>
                    <div class="col s6 m4 l3">
                        <div class="card hoverable">
                            <div class="card-image">
                                <img src=<?php echo $amply['thumbnailurl']; ?> alt="">
                            </div>
                            <div class="card-content">
                                <a href="<?php echo './productInfo.php?id='.$amply['id'];?>" class="card-product-name"> <?php echo $amply['name']; ?> </a>
                                <div class="rating">
                                    <?php for($i = 1; $i <= (int)$amply['rating']; $i++) echo '<i class="material-icons">star</i>';?>
                                    <?php if($amply['rating'] - (int)$amply['rating'] >= 0.5) echo '<i class="material-icons">star_half</i>';?>
                                    <?php for($i = round($amply['rating']); $i < 5; $i++) echo '<i class="material-icons">star_border</i>';?>

                                </div>
                                <p class="card-product-price"><?php echo number_format(($amply['price']),0,",",".")." "; ?>₫</p>
                            </div>
                        </div>
                    </div>
                <?php } ?>
            </div>
            <div class="center-align">
                <a href="#" class="waves-effect waves-light btn continue-btn pulse">Xem tiếp</a>
            </div>
        </div>
    </div>
    <div class="favorite">
        <div class="container">
            <div class="text center-align">
                <h5>Sản phẩm bán chạy</h5>
                <p>Nhu cầu số đông lựa chọn theo những sản phẩm dưới , sản phẩm được ưu tiên lựa chọn nhiều nhất trong Moza nhạc
                        cụ</p>
            </div>
            <div class="favorite-products">
                <div class="row">
                <?php $iter = count($favorites) <= 4 ? count($favorites) : 4;
                    for ($i = 0; $i < $iter; $i++){ ?>
                    <div class="<?php if ($i == 0) echo "col m3 s12"; else echo "col m3 s12 hide-on-small-only"?>">
                        <div class="product-box">
                            <div class="product-image">
                                <img class="responsive-img" src="<?php echo $favorites[$i]['thumbnailurl'];?>" alt="">
                            </div>
                            <div class="product-detail">
                                <a href="<?php echo './productInfo.php?id='.$favorites[$i]['id'];?>" class="product-name"><?php echo $favorites[$i]['name'];?></a>
                                <p class="product-price"><?php echo number_format(($favorites[$i]['price']),0,",",".")." "; ?>₫</p>
                                <div class="product-features">
                                    <?php echo $favorites[$i]['description'];?>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php } ?>
                    <!-- <div class="col m3 s12">
                        <div class="product-box">
                            <div class="product-image">
                                <img class="responsive-img" src="./images/products/144781619662-4925508.jpg" alt="">
                            </div>
                            <div class="product-detail">
                                <a href="#" class="product-name">Bộ trống Jazz Deviser JZGD22RD</a>
                                <p class="product-price">10.500.500đ</p>
                                <div class="product-features">
                                    <ul>
                                        <li>Thiết kế ấn tượng, đẹp mắt</li>
                                        <li>Chất âm acoustic tuyệt vời</li>
                                        <li>Phù hợp với phong cách nhạc rock</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col m3 hide-on-small-only">
                        <div class="product-box">
                            <div class="product-image">
                                <img class="responsive-img" src="./images/products/1459234325585-2028627.jpg" alt="">
                            </div>
                            <div class="product-detail">
                                <a href="#" class="product-name">Trống điện Medeli DD650</a>
                                <p class="product-price">18.855.000đ</p>
                                <div class="product-features">
                                    <ul>
                                        <li>Thiết kế kiểu dáng đẹp, nhỏ gọn</li>
                                        <li>Âm thanh sống động, chân thực</li>
                                        <li>Tích hợp tính năng chơi nhạc chuyên nghiệp</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col m3 hide-on-small-only">
                        <div class="product-box">
                            <div class="product-image">
                                <img class="responsive-img" src="./images/products/1497258329432-6514934.jpg" alt="">
                            </div>
                            <div class="product-detail">
                                <a href="#" class="product-name">Violin gỗ size 1/4' KBD 34A5 (Nâu cánh gián)</a>
                                <p class="product-price">1.080.000đ</p>
                                <div class="product-features">
                                    <ul>
                                        <li>Kích thước nhỏ, âm thanh cao</li>
                                        <li>Mặt gỗ với sắc nâu cánh gián đẹp mắt</li>
                                        <li>Size 1/4 phù hợp với trẻ em</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col m3 hide-on-small-only">
                        <div class="product-box">
                            <div class="product-image">
                                <img class="responsive-img" src="./images/products/144548251371-8695719.jpg" alt="">
                            </div>
                            <div class="product-detail">
                                <a href="./productInfo.html" class="product-name">Đàn piano điện Casio PX-780M</a>
                                <p class="product-price">25.000.000đ</p>
                                <div class="product-features">
                                    <ul>
                                        <li>Nguồn âm thanh Morphing AiR đa chiều</li>
                                        <li>Bàn phím hoạt động đầu cần ba cảm biến II</li>
                                        <li>Các phím đàn mô phỏng mun đen và trắng ngà</li>
                                    </ul>
                                </div>
                            </div>
                        </div> -->
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
    <div class="posts"></div>

    <?php include('./footer.php') ?>

    <button class="btn-floating waves-effect waves-light" onclick="topFunction()" id="scrollTop" title="Go to top"><i class="material-icons">arrow_upward</i></button>
    <script src="./js/scrollToTop.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"
        integrity="sha512-bLT0Qm9VnAYZDflyKcBaQ2gg0hSYNQrJ8RilYldYQ1FxQYoCLtUjuuRuZo+fjqhx/qtq/1itJ0C2ejDxltZVFg=="
        crossorigin="anonymous">
    </script>
    <script src="./js/jquerymatchHeight.js"></script>
    <script>
        document.addEventListener('DOMContentLoaded', function () {
            var carousel = document.querySelectorAll('.carousel');
            var carousleOptions = {fullWidth: true}
            var instances = M.Carousel.init(carousel, carousleOptions);
            var tabs = document.querySelectorAll('.tabs');
            var instance = M.Tabs.init(tabs, {});
        });
        $(function () {
            $('.card').matchHeight();
            $('.card-image').matchHeight();
        });

        document.addEventListener('DOMContentLoaded', function () {
            var elems = document.querySelectorAll('.sidenav');
            var instances = M.Sidenav.init(elems);
        });
    </script>
    <script>
        $('.homepage-nav').addClass('active');
    </script>
</body>
</html>