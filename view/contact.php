<!DOCTYPE html>
<html lang="en">

<head>
    <!--Import Google Icon Font-->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!--Import materialize.css-->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/contact.css">
    <link rel="stylesheet" href="css/footer.css">
    <!--Let browser know website is optimized for mobile-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Liên hệ | Moza</title>
</head>

<body>
    <?php include('./header.php');?>
    <div class="contact__container">
        <ul class="mybreadcrumb">
            <li><a href="./index.html">Trang chủ</a></li>
            <li><span>Liên hệ</span></li>
        </ul>

        <div class="contact__content">
            <div class="row">
                <form class="col s12 l6 offset-l1 row">
                    <div class="row">
                        <p class="col s12">Bạn hãy điền nội dung tin nhắn vào form dưới đây và gửi cho chúng tôi.
                            Chúng tôi sẽ trả lời bạn sau khi nhận được.</p>
                    </div>
                    <div class="row">
                        <div class="input-field col s12">
                            <i class="material-icons prefix">account_circle</i>
                            <input id="name" type="text" class="validate">
                            <label for="name">Họ và tên</label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col s12">
                            <i class="material-icons prefix">email</i>
                            <input id="email" type="text" class="validate">
                            <label for="email">Email</label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col s12">
                            <i class="material-icons prefix">border_color</i>
                            <input id="content" type="text" class="validate">
                            <label for="content">Nội dung</label>
                        </div>
                    </div>
                    <div class="row center-align">
                        <!-- <div class="col s2 offset-s5"> -->
                            <button class="btn green waves-effect waves-light" type="submit" name="action">Gửi
                                <i class="material-icons right">send</i>
                            </button>
                        <!-- </div> -->
                    </div>
                </form>
            
                <div class="col s12 l4 row contact-info">
                    <div class="col s12">
                        <img src="images/contact/logo-contact.png" alt="" class="responsive-img">
                    </div>
                    <div class="col s12">
                        <p> <i class="tiny material-icons prefix">location_on</i> Phòng 214, Chung cư B3, Phường Quan Hoa , Cầu Giấy, Hà Nội</p>
                    </div>
                    <div class="col s12">
                        <a href="#" id="a_telephone"><i class="left tiny material-icons prefix">local_phone</i>
                            0902068068</a>
                    </div>
                    <div class="col s12">
                        <a href="mailto:songvangvietnam@gmail.com " id="a_email"> <i
                                class="left tiny material-icons prefix">email</i> songvangvietnam@gmail.com</a>
                    </div>
                </div>
            
            </div>
        </div>
    </div>
    <div class="box-maps">
        <iframe
            src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2214.234101995328!2d105.80640409923018!3d21.037365453004117!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3135ab15a9a9e3c1%3A0x85597af3d21a3b04!2zNDQyIMSQ4buZaSBD4bqlbiwgQ-G7kW5nIFbhu4ssIEJhIMSQw6xuaCwgSMOgIE7hu5lpLCBWaeG7h3QgTmFt!5e0!3m2!1svi!2s!4v1468478721163"
            style='width:100%' height="450" allowfullscreen=""></iframe>
    </div>
    <footer class="footer">
        <div class="container-products">
            <div class="row">
    
                <div class="col s12 m3 l5">
                    <div class="footer-widget">
                        <h4 class="footer-logo">About shop</h4>
                        <p>
                            Moza chuyên kinh doanh sản phẩm âm nhạc thời thượng đẳng cấp. Với tiêu chi CHẤT LƯỢNG cho từng
                            khách hàng, chúng tôi
                            đang nỗ lực để ngày một hoàn thiện,cảm ơn quý khách hàng đã luôn đồng hành hỗ trợ.
                        </p>
                    </div>
                </div>
                <div class="col l7 m9">
                    <div class="col s12 m5 l5">
                        <div class="footer-widget">
                            <h4>Hỗ trợ</h4>
                            <ul class="list-menu">
                                <li>Vui lòng liên hệ với chúng tôi nếu bạn có bất kỳ vấn đề gì.</li>
                                <li><strong>Địa chỉ: </strong>
                                    Phòng 214, Chung cư B3, Phường Quan Hoa , Cầu Giấy, Hà Nội
                                </li>
                                <li><strong>Email: </strong>
                                    <a href="mailto:songvangvietnam@gmail.com">songvangvietnam@gmail.com </a>
                                </li>
                                <li><strong>SĐT: </strong>
                                    <a href="#"> 0902068068 </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="col s12 m3 l3">
                        <div class="footer-widget">
                            <h4>Hướng dẫn</h4>
                            <ul class="list-menu">
                                <li><a href="./index.html">Trang chủ</a></li>
                                <li><a href="./about.html">Giới thiệu</a></li>
                                <li><a href="./products.html">Sản phẩm</a></li>
                                <li><a href="./products.html">Guitar</a></li>
                                <li><a href="./products.html">Piano-Organs</a></li>
                                <li><a href="./products.html">Trống-Bộ gõ</a></li>
                                <li><a href="./contact.html">Liên hệ</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col s12 m4 l4">
                        <div class="footer-widget">
                            <h4>Thanh toán</h4>
                            <ul class="list-menu">
                                <li><a href="/maestro"><img src="./images/icons/maestro.png" alt="maestro"></a></li>
                                <li><a href="/paypal"><img src="./images/icons/paypal.png" alt="paypal"></a></li>
                                <li><a href="/visa"><img src="./images/icons/visa.png" alt="visa"></a></li>
                                <li><a href="/cirrus"><img src="./images/icons/cirrus.png" alt="cirrus"></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col s12">
                    <a href="/cirrus"><img src="./images/icons/icon-social.png" alt="social"></a>
                </div>
            </div>
        </div>
    </footer>
    <button class="btn-floating waves-effect waves-light" onclick="topFunction()" id="scrollTop" title="Go to top"><i
            class="material-icons">arrow_upward</i></button>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"
        integrity="sha512-bLT0Qm9VnAYZDflyKcBaQ2gg0hSYNQrJ8RilYldYQ1FxQYoCLtUjuuRuZo+fjqhx/qtq/1itJ0C2ejDxltZVFg=="
        crossorigin="anonymous">
    </script>
    <script src="./scrollToTop.js"></script>
    <!--JavaScript at end of body for optimized loading-->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>
    <script>
        document.addEventListener('DOMContentLoaded', function () {
            var elems = document.querySelectorAll('.sidenav');
            var instances = M.Sidenav.init(elems);
        });
        $('.contact-nav').addClass('active');
    </script>
</body>

</html>