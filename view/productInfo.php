<!DOCTYPE html>
<html lang="en">
<?php
    include('../model/product.php');
    include('../model/tag.php');
    include('../model/user.php');
    $productModel = new ProductModel();
    $userModel = new UserModel();
    $productModel->conn->set_charset("utf8");
    $review_num = 2;
    if(isset($_GET['id'])) {
        $product = $productModel->getProduct($_GET['id']);
        $reviews = $productModel->getProductReviews($_GET['id'], 0, $review_num);
    }
?>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Compiled and minified CSS -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">

    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">


    <link rel="stylesheet" href="./css/style.css">
    <link rel="stylesheet" href="./css/productinfo.css">
    <link rel="stylesheet" href="css/footer.css">
    <style>
        #tab-content h1 {
            font-size: 30px;
        }
        #tab-content h2 {
            font-size: 26px;
        }
        #tab-content h3 {
            font-size: 22px;
        }
    </style>
    <script src="https://kit.fontawesome.com/e8a340032d.js" crossorigin="anonymous"></script>
    <title><?php echo $product['name'];?></title>
</head>

<body>
    <?php
        include('./header.php');
        if (isset($_SESSION['userId'])) $userId = $_SESSION['userId']; else $userId = '';
        // include('../model/product.php');
        // include('../model/tag.php');
        // include('../model/user.php');
        // if (isset($_SESSION['userId'])) $userId = $_SESSION['userId']; else $userId = '';
        // $productModel = new ProductModel();
        // $userModel = new UserModel();
        // $productModel->conn->set_charset("utf8");
        // $review_num = 2;
        // if(isset($_GET['id'])) {
        //     $product = $productModel->getProduct($_GET['id']);
        //     $reviews = $productModel->getProductReviews($_GET['id'], 0, $review_num);
        // }
    ?>


    <div class="container details-product">
        <ul class="mybreadcrumb">
            <li><a href="./homepage.php">Trang chủ</a></li>
            <li><a href="./products.php">Sản phẩm</a></li>
            <!-- <li><span>Đàn Piano điện Casio PX-780M</span></li> -->
            <li><span><?php echo $product['name']; ?></span></li>
        </ul>
        <div class="row">
            <div class="col l4 m5" id="left-image">
                <div class="large-image">
                    <img src="<?php echo $product['thumbnailurl']; ?>" alt="jpg" class="responsive-img" id="large-image-id">
                </div>
                
                <!-- <div class="list-thumbnail">
                    <div class="col s3 thbnail">
                        <img src="./images/products/thumbnail1.jpg" alt="jpg" id="thumbnail1" class="responsive-img"
                            onclick="fnClickChangeImg('./images/products/thumbnail1.jpg')">
                    </div>
                    <div class="col s3 thbnail">
                        <img src="./images/products/thumbnail2.jpg" alt="jpg" id="thumbnail2" class="responsive-img"
                            onclick="fnClickChangeImg('./images/products/thumbnail2.jpg')">
                    </div>
                    <div class="col s3 thbnail">
                        <img src="./images/products/thumbnail3.jpg" alt="jpg" id="thumbnail3" class="responsive-img"
                            onclick="fnClickChangeImg('./images/products/thumbnail3.jpg')">
                    </div>
                    <div class="col s3 thbnail">
                        <img src="./images/products/thumbnail4.jpg" alt="jpg" id="thumbnail4" class="responsive-img"
                            onclick="fnClickChangeImg('./images/products/thumbnail4.jpg')">
                    </div>
                </div> -->
            </div>
            <div class="col l5 m7 details-pro">
                <h5 style="margin-top: 0;"><?php echo $product['name']; ?></h5>
                <div class="inventory-quantity">
                    <span><?php echo $product['inStock'] > 0 ? "Còn hàng" : "Hết hàng" ; ?></span>
                </div>
                <div class="price-box">
                    <span class="special-price">
                        <span class="product-price"><?php echo number_format($product['price'], 0, ',', '.')."<u>đ</u>"; ?></span>
                    </span>
                    <!-- <span class="old-price">
                        <del
                            class="product-price-old"><?php echo number_format($product['price'], 0, ',', '.')."<u>đ</u>"; ?></del>
                    </span> -->
                </div>
                <div class="description">
                    <ul>
                        <?php echo $product['description']; ?>
                    </ul>
                </div>
                <div class="form-product">
                    <?php if($product['inStock'] > 0): ?>
                    <form action="../controller/addtocart.php" id="add-to-cart-form" method="POST">
                        <div class="form-group">
                            <div class="custom-btn-number">
                                <label>Số lượng</label>
                                <input class="center-align" type="number" id="qty" value="1" min="1" max=<?php echo $product['inStock']; ?>
                                    name="quantity">
                            </div>
                            <input type="hidden" name="productid" value="<?php echo $product['id']; ?>" />
                            <button type="submit" class="add-to-cart pulse">Thêm vào giỏ hàng</button>
                        </div>
                    </form>
                    <?php endif; ?>
                    <div class="tag-product">
                        <label>Tags: </label>
                        <?php
                        $tagsP = $productModel->getProductTags($_GET['id']);
                        $tagmodel = new TagModel();
                        $tagmodel->conn->set_charset("utf8");
                        $count = 1;
                        foreach ($tagsP as $tagP):
                            $tagId = $tagP['tagid'];
                            $tag = $tagmodel->getTag($tagId);
                            if($count>1) {
                                echo '<a href="#">'.', '.$tag['tagname'].'</a>'; 
                            } else {
                                echo '<a href="#">'.$tag['tagname'].'</a>';
                            }
                            $count++;
                        endforeach;
                        ?>
                        <!-- <a href="#">Đàn piano</a> -->
                    </div>
                </div>
            </div>
            <div class="col l3 hide-on-med-and-down best-product">
                <h5><span>Sản phẩm bán chạy</span></h5>
                <?php 
                    $bestSolds = $productModel->getBestSoldProducts();
                    $iter = count($bestSolds) <= 4 ? count($bestSolds) : 4;
                    for ($i = 0; $i < $iter; $i++):
                ?>
                <div class="row best-item">
                    <div class="col l5 product-box">
                        <div class="product-thumbnail">
                            <a href="./productInfo.php?id=<?php echo $bestSolds[$i]['id']; ?>">
                                <img src="<?php echo $bestSolds[$i]['thumbnailurl']; ?>" alt="jpg"
                                    class="responsive-img">
                            </a>
                        </div>
                    </div>
                    <div class="col l7 product-info">
                        <h6 class="product-name">
                            <a href="./productInfo.php?id=<?php echo $bestSolds[$i]['id']; ?>"><?php echo $bestSolds[$i]['name']; ?></a>
                        </h6>
                        <div class="price-box">
                            <div class="special-price">
                                <span class="product-price"><?php echo number_format($bestSolds[$i]['price'], 0, ',', '.')."<u>đ</u>"; ?></span>
                            </div>
                        </div>
                    </div>
                </div>
                <?php 
                endfor;
                ?>
            </div>
        </div>
        <div class="row">
            <div class="col s12">
                <div class="product-tab">
                    <ul class="tabs">
                        <li class="tab col s2"><a class="active" href="#tab-content">Mô tả</a></li>
                        <li class="tab col s2"><a href="#tab-opt">Tùy chỉnh</a></li>
                        <li class="tab col s2"><a href="#tab-review">Đánh giá</a></li>
                    </ul>
                </div>
            </div>
            <div id="tab-content" class="col s12">
                <!-- <h5><span style="font-size: 15px;">Thông số sản phẩm</span></h5>
                <table class="responsive-table">
                    <thead>
                        <tr>
                            <th>Chất liệu</th>
                            <th>Model</th>
                            <th>Kích thước</th>
                            <th>Màu sắc</th>
                            <th>Trọng lượng</th>
                        </tr>
                    </thead>

                    <tbody>
                        <tr>
                            <td>Gỗ, nhựa, kim loại</td>
                            <td>PX-780
                            </td>
                            <td>135.7 x 29.9 x 83.3 cm
                            </td>
                            <td>Đen
                            </td>
                            <td>31,5 (kg)
                            </td>
                        </tr>
                    </tbody>
                </table> -->
                <h4><span style="font-size: 15px;"><?php echo $product['name']; ?>
                    </span></h4>
                <?php echo $product['detail']; ?>
            </div>
            <div id="tab-opt" class="col s12">
                <h5><span style="font-size: 15px;">Các nội dung hướng dẫn mua hàng viết ở đây</span></h5>
            </div>
            <div id="tab-review" class="col s12">
                <div id="review-section">
                    <!-- <h5><span style="font-size: 15px;">Đánh giá sản phẩm</span></h5> -->
                    <input type="text" name="reviewcontent" id="reviewcontent" placeholder="Viết đánh giá">
                    <!-- <hr style="color: #adadad;"> -->
                    <div class="product-reviews right-align">
                        <!-- <input type="button" id="btnnewreview" value="Viết đánh giá"> -->
                        <button id="add-review-btn" class="btn waves-effect waves-light" style="background-color: var(--primary-orange);">Viết đánh giá</button>
                    </div>
                    <?php foreach($reviews as $review) { 
                        $reviewer = $userModel->getUser($review['userid']);
                    ?>
                        <div class="user-review">
                            <div class="row">
                                <div class="user-box">
                                    <div class="user-image">
                                        <img src="<?php echo "./images/users/".$reviewer['avatarurl']; ?>" alt="user.png"
                                            class="circle responsive-img">
                                    </div>
                                    <div class="user-name center-align">
                                        <span style="font-size: 15px;"><?php echo $reviewer['lastname']; ?></span>
                                    </div>
                                </div>
                                <div class="review-box">
                                    <div class="review-title center-align">
                                        <span style="font-size: 15px;"><?php echo $review['content']; ?></span>
                                    </div>
                                    <div class="review-star center-align">
                                        <span><i class="fas fa-star checked"></i></span>
                                        <span><i class="fas fa-star checked"></i></span>
                                        <span><i class="fas fa-star checked"></i></span>
                                        <span><i class="fas fa-star checked"></i></span>
                                        <span><i class="fas fa-star"></i></span>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="user-comment">
                                    <h6 style="font-size: 15px;"><?php echo $review['createdat'];?></h6>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                </div>
                <div class="container center-align"><button id="loadmore" class="btn waves-effect waves-light">Tải thêm đánh giá</button></div>
            </div>
        </div>
        
        <div class="row related-product">
            <div class="col s12">
                <?php
                    $typeP = $product['type'];
                    $listSameType = $productModel->getProductsByType($typeP, 0, 6);
                    if(count($listSameType)>1):
                ?>
                <div class="heading">
                    <h5><a href="#">Sản phẩm cùng loại</a></h5>
                </div>
                <div class="my carousel">
                <?php
                    foreach ($listSameType as $item):
                        if($item['id']!==$product['id']):
                ?>
                    <div class="my carousel-item">
                        <div class="product-box">
                            <div class="product-thumbnail">
                                <a href="./productInfo.php?id=<?php echo $item['id']; ?>"><img src="<?php echo $item['thumbnailurl']; ?>" alt="jpg"
                                        class="responsive-img"></a>
                            </div>
                            <div class="product-relate-info">
                                <div class="product-info">
                                    <h6 class="product-name center-align"><a href="./productInfo.php?id=<?php echo $item['id']; ?>"><?php echo $item['name']; ?></a></h6>
                                </div>
                                <div class="price-box">
                                    <div class="special-price center-align">
                                        <span class="product-price"><?php echo number_format($item['price'], 0, ',', '.')."<u>đ</u>"; ?></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php endif; endforeach; ?>
                    <!-- <div class="my carousel-item">
                        <div class="product-box">
                            <div class="product-thumbnail">
                                <a href="#"><img src="./images/products/trong-dien-medeli-dd650.jpg" alt="jpg"
                                        class="responsive-img"></a>
                            </div>
                            <div class="product-relate-info">
                                <div class="product-info">
                                    <h6 class="product-name center-align"><a href="#">Trống điện Medeli DD650</a></h6>
                                </div>
                                <div class="price-box">
                                    <div class="special-price center-align">
                                        <span class="product-price">15.855.000₫</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="my carousel-item">
                        <div class="product-box">
                            <div class="product-thumbnail">
                                <a href="#"><img src="./images/products/trong-jazz.jpg" alt="jpg"
                                        class="responsive-img"></a>
                            </div>
                            <div class="product-relate-info">
                                <div class="product-info">
                                    <h6 class="product-name center-align"><a href="#">Bộ trống jazz Deviser JZGD22RD</a>
                                    </h6>
                                </div>
                                <div class="price-box">
                                    <div class="special-price center-align">
                                        <span class="product-price">10.500.000₫</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="my carousel-item">
                        <div class="product-box">
                            <div class="product-thumbnail">
                                <a href="#"><img src="./images/products/trong-dien-medeli-55.jpg" alt="jpg"
                                        class="responsive-img"></a>
                            </div>
                            <div class="product-relate-info">
                                <div class="product-info">
                                    <h6 class="product-name center-align"><a href="#">Trống điện Medeli DD512</a></h6>
                                </div>
                                <div class="price-box">
                                    <div class="special-price center-align">
                                        <span class="product-price">13.500.000₫</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="my carousel-item">
                        <div class="product-box">
                            <div class="product-thumbnail">
                                <a href="#"><img src="./images/products/1497258329432-6514934.jpg" alt="jpg"
                                        class="responsive-img"></a>
                            </div>
                            <div class="product-relate-info">
                                <div class="product-info center-align">
                                    <h6 class="product-name"><a href="#">Violin gỗ size 1/4' KBD 34A5 (Nâu cánh
                                            gián)</a></h6>
                                </div>
                                <div class="price-box">
                                    <div class="special-price center-align">
                                        <span class="product-price">1.080.000₫ </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div> -->
                </div>
                <div class="row">
                    <div class="col s1 center-align">
                        <div class="prev carousel-prev">
                            <i class="fas fa-angle-left"></i>
                        </div>
                    </div>
                    <div class="col s1 offset-s10">
                        <div class="next carousel-next center-align">
                            <i class="fas fa-angle-right"></i>
                        </div>
                    </div>
                </div>
                <?php endif; ?>
            </div>
        </div>
    </div>

    <?php include('./footer.php'); ?>

    <button class="btn-floating waves-effect waves-light" onclick="topFunction()" id="scrollTop" title="Go to top"><i
            class="material-icons">arrow_upward</i>
    </button>

    <script src="./js/scrollToTop.js"></script>

    <script src="https://code.jquery.com/jquery-3.5.1.min.js"
        integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous">
    </script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>

    <script>
        const userId = <?php if($userId == '')  echo "''" ; else echo $userId;?>;
        const productId = <?php echo $_GET['id'];?>;
    </script>

    <script>
        var el = document.querySelector('.tabs');
        var instance = M.Tabs.init(el, {});

        $(document).ready(function() {
            $('.carousel').carousel();
        });

        $('.next').click(function() {
            $('.carousel').carousel('next');
        });
        $('.prev').click(function() {
            $('.carousel').carousel('prev');
        });
        $('.product-nav').addClass('active');

        function fnClickChangeImg(source) {
            document.getElementById("large-image-id").src = source;
        }

        document.addEventListener('DOMContentLoaded', function() {
            var elems = document.querySelectorAll('.sidenav');
            var instances = M.Sidenav.init(elems);
        });

        const addReviewBtn = document.getElementById('add-review-btn');
        addReviewBtn.addEventListener('click', function (e) {
            if (userId != '') {
                const content = document.getElementById('reviewcontent').value;
                if(content.length > 0) {
                    addReviewBtn.setAttribute('disabled','true');
                    $.post('../controller/addreview.php', {userid: userId, productid: productId, content: content} , function (data, status) {
                    if(data){
                        data = JSON.parse(data);
                        addReviewBtn.removeAttribute('disabled');
                        if(data.success) {
                            location.reload();
                        }
                        else alert('Bình luận thất bại');
                    }
                })
                }
            }
            else alert('Vui lòng đăng nhập để bình luận');
        })
        page = 2; page_size = 2;
        offset = (page-1) * page_size;
        const loadMoreBtn = document.getElementById('loadmore');
        const reviewSection = document.getElementById('review-section');
        loadMoreBtn.addEventListener('click', function (e) {
            loadMoreBtn.setAttribute('disabled','true');
            $.post('../controller/loadmorereviews.php', {productid: productId, offset: offset, page_size: page_size} , function (data, status) {
                loadMoreBtn.removeAttribute('disabled');
                if(data){
                    page++;
                    offset = (page-1) * page_size;
                    reviewSection.innerHTML += data;
                }
            })
        })
    </script>
</body>
</html>