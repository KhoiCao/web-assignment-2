<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Compiled and minified CSS -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">
    
    <!-- Compiled and minified JavaScript -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>
    
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

    <link rel="stylesheet" href="./css/style.css">
    <link rel="stylesheet" href="./css/index.css">
    <link rel="stylesheet" href="./css/footer.css">
    <title>Giỏ hàng</title>
    <style>
        body{
            display: flex;
            flex-direction: column;
            min-height: 100vh;
        }
        main {
            flex-grow: 1;
        }
        .order {
            /* margin: 30px 0; */
            text-transform: none;
            padding: 0 25px;
            background-color: #fd923f;
        }
        .order:hover {
            background-color: #fd923f;
        }
        .cart-summary {
            max-width: 600px;
            margin-right: 20;
            margin-left: auto;
            display: flex;
            flex-direction: column;
            justify-content: center;
            align-items: center;
        }
        .cart-summary div {
            margin: 10px 0;
        }
        .remove-item {
            /* border: none;
            background-color: transparent; */
            color: black;
            /* cursor: pointer; */
        }
    </style>
</head>
<body>
    <?php 
        include('./header.php'); include('../model/product.php'); 
        $productModel = new ProductModel();
    ?>

    <?php 
        if (!isset($_SESSION['userId'])) {
            echo '<p style="center-align">Vui lòng đăng nhập để sử dụng tính năng này!</p>';
            $cart_data = array();
        }
        else if (!isset($_COOKIE[$_SESSION['userId']])) {
            // echo '<p>Không có sản phẩm nào trong giỏ đồ!</p>';
            $cart_data = array();
        }
        else {
            $userId = $_SESSION['userId'];
            $cookie_data = stripslashes($_COOKIE[$userId]);
            $cart_data = json_decode($cookie_data, true);
        }
    ?>
    <main>
        <table class="container" style="margin: 50px auto;">
            <!-- <caption style="font-size: 30px">Giỏ hàng</caption> -->
            <thead>
                <tr>
                    <th class="center-align">STT</th>
                    <th class="center-align">Tên sản phẩm</th>
                    <th class="center-align">Đơn giá</th>
                    <th class="center-align">Số lượng</th>
                    <th class="center-align">Thành tiền</th>
                    <th class="center-align">Xóa</th>
                </tr>
            </thead>
            <tbody>
                <?php $i = 1; $total = 0; foreach($cart_data as $item) {    
                    $product = $productModel->getProduct($item['productid']);
                    // number_format(($product['price']),0,",",".")."d"
                ?>
                <tr id="<?php echo $product['id'];?>">
                    <td class="center-align" ><?php echo $i; ?></td>
                    <td class="center-align"><?php echo $product['name']; ?></td>
                    <td class="center-align product-price"><?php echo number_format(($product['price']),0,",",".")." "; ?>₫</td>
                    <td class="center-align"> <input class="center-align quantity_input" type="number" value="<?php echo $item['quantity']; ?>" min="1" max="<?php echo $product['inStock']; ?>" name="quantity" style="width: 40px; border: none"> </td>
                    <td class="center-align price-display"><?php echo number_format(($item['quantity'] * $product['price']),0,",",".")." ";?>₫</td>
                    <!-- <td class="center-align"><a href="<?php echo '../controller/removecartitem.php?id='.$product['id'];?>" class="remove-item"><i class="material-icons small">delete_forever</i></a></td> -->

                    <td class="center-align">
                        <a class="modal-trigger remove-item" href=<?php echo '#modal'.$product['id'];?>><i class="material-icons small">delete_forever</i></a>
                        <div id=<?php echo "modal".$product['id'];?> class="modal">
                            <div class="modal-content">
                                <h4>Xác nhận xóa</h4>
                            </div>
                            <div class="modal-footer">
                                <a class="modal-close waves-effect waves-green btn-flat">Quay lại</a>
                                <a href=<?php echo '../controller/removecartitem.php?id='.$product['id'];?> class="modal-close waves-effect waves-green btn-flat">Đồng ý</a>
                            </div>
                        </div>
                    </td>
                </tr>
                <?php $i++; $total+= $product['price'] * $item['quantity']; } ?>
            </tbody>
        </table>

        <?php if (count($cart_data) > 0) {?>
            <div class="cart-summary">
                <div>
                    <span>Tổng tiền: </span>
                    <div style="display: inline" class="total-price"><?php echo number_format(($total),0,",",".")." "; ?>₫</div>
                </div>
                <div class="btn-option"><a href="../controller/addorder.php" class="waves-effect waves-light btn order pulse">Đặt hàng</a></div>
            </div>
        <?php }?>
    </main>


    <?php include('./footer.php'); ?>

    <button class="btn-floating waves-effect waves-light" onclick="topFunction()" id="scrollTop" title="Go to top"><i class="material-icons">arrow_upward</i></button>
    <script src="./js/scrollToTop.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"
        integrity="sha512-bLT0Qm9VnAYZDflyKcBaQ2gg0hSYNQrJ8RilYldYQ1FxQYoCLtUjuuRuZo+fjqhx/qtq/1itJ0C2ejDxltZVFg=="
        crossorigin="anonymous">
    </script>
    <script src="./js/jquerymatchHeight.js"></script>
    <script>
        document.addEventListener('DOMContentLoaded', function () {
            var sidenav_elems = document.querySelectorAll('.sidenav');
            var sidenav_instances = M.Sidenav.init(sidenav_elems);
        });
        document.addEventListener('DOMContentLoaded', function() {
            var elems = document.querySelectorAll('.modal');
            var instances = M.Modal.init(elems);
        });
    </script>

    <script>
        const userId = <?php echo $_SESSION['userId']; ?>;
        const cart_total = $('.total-price');
        function getCookie(cname) {
            let name = cname + "=";
            let decodedCookie = decodeURIComponent(document.cookie);
            let ca = decodedCookie.split(';');
            for(let i = 0; i <ca.length; i++) {
                let c = ca[i];
                while (c.charAt(0) == ' ') {
                c = c.substring(1);
                }
                if (c.indexOf(name) == 0) {
                return c.substring(name.length, c.length);
                }
            }
            return "";
        }

        function setCookie(cname, cvalue) {
          let d = new Date();
          d.setTime(d.getTime() + (30*24*60*60*1000));
          var expires = "expires="+ d.toUTCString();
          document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
        }

        function render_cart() {

        }

        const cart_data = JSON.parse(getCookie(userId));
        
        $(function() {
            $('.quantity_input').each(function () {
                $(this).on('change', () => {
                    const newQuantity = $(this).val();
                    const productPrice = $(this).parents('td').siblings('.product-price').text().replace(/₫|\./g,'');
                    const priceDisplay = $(this).parents('td').siblings('.price-display');
                    let totalPrice = newQuantity * productPrice;
                    priceDisplay.text(totalPrice.toLocaleString('vi-VN', {currency: 'VND', style: 'currency'}));
                    cart_data.forEach((item) => {
                        if (item.productid == ($(this).parents('tr').attr("id"))) {
                            item.quantity=newQuantity;
                        }
                    });
                    setCookie(userId, JSON.stringify(cart_data));
                    let priceDisplayTD = document.querySelectorAll('.price-display');
                    let totalCost = 0;
                    for (let td of priceDisplayTD) {
                        console.log(td.innerText.replace(/₫|\.|" "/g,''));
                        totalCost += parseInt(td.innerText.replace(/₫|\.|" "/g,''));
                    }
                    $('.total-price').text(totalCost.toLocaleString('vi-VN', {currency: 'VND', style: 'currency'}));
                })
            })
        })
    </script>
</body>
</html>