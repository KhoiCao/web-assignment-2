<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Compiled and minified CSS -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">
    
    <!-- Compiled and minified JavaScript -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>
    
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

    <link rel="stylesheet" href="./css/style.css">
    <link rel="stylesheet" href="./css/footer.css">
    
    <style>
        body{
            display: flex;
            flex-direction: column;
            min-height: 100vh;
        }
        main {
            flex-grow: 1;
        }
        .article-box {
            border: 1px solid #e9e9e9;
            display: inline-block;
            cursor: pointer;
            margin-bottom: 30px;
            width: 100%;
            }
        .img-list {
            text-align: center;
            margin-bottom: 0;
            width: calc(100% / 6);
            float: left;
            position: relative;
        }
        .article-info-list {
            text-align: left;
            margin-left: 30px;
            float: left;
            width: calc(100% / 3 * 2 - 30px);
            background: transparent;
            border-top: none;
            padding: 5px 0;
        }
        .article-info-list a{
            color: black;
            font-size: 20px;
        }
        .article-info-list a:hover{
            color: var(--primary-orange);
        }
        .article-info {
            color: #909090;
            font-size: 14px;
            padding-top: 4px;
            margin-bottom: 15px;
        }
        .pagination .active {
            background-color: var(--primary-orange) !important;
        }
    </style>
    <title>Bài viết</title>
</head>
<body>
    <?php 
        include('./header.php');
        include('../model/article.php'); 
        include('../model/user.php');
        $articleModel = new ArticleModel();
        $userModel = new UserModel();
        if (isset($_GET['page'])) {
            $page = $_GET['page'];
        } else {
            $page = 1;
        }
        $page_size = 1;
        $offset = ($page-1) * $page_size;
        $articles = $articleModel->getArticlesByRange($offset, $page_size);
        $total_rows = $articleModel->getNumberOfArticles();
        $total_pages = ceil($total_rows / $page_size);
    ?>

    <main class="container">
        <div class="list-article">
            <h5>Danh sách bài viết</h5>
            <?php foreach($articles as $article) { $uploader = $userModel->getUser($article['userid']);?>
                <div class="col s12 article-box">
                    <div class="img-list">
                        <a href=<?php echo "./article.php?id=".$article['id']; ?>><img src=<?php echo $article['thumbnailurl'];?> alt="Guitar 1" class="responsive-img"></a>
                    </div>
                    <div class="article-info-list">
                        <a class="article-title" href=<?php echo "./article.php?id=".$article['id']; ?>><?php echo $article['title'];?></a>
                        <div class="article-info">
                            <div>Được đăng tải bởi <?php echo $uploader['firstname']." ".$uploader['lastname'];?></div>
                            <div>Vào lúc <?php echo $article['createdAt'];?></div>
                        </div>
                    </div>
                </div>
            <?php } ?>
        </div>
        <ul class="pagination center-align" style="margin-top: 10px;">
            <li class="<?php if($page == 1){ echo 'disabled'; } else { echo 'waves-effect';} ?>"><a href="?page=1"><i class="material-icons">first_page</i></a></li>
                <li class="<?php if($page <= 1){ echo 'disabled'; } else { echo 'waves-effect';} ?>">
                    <a href="<?php if($page <= 1){ echo '#'; } else { echo "?page=".($page - 1); } ?>"><i class="material-icons">chevron_left</i></a>
                </li>
                <!-- Nút đầu tiên -->
                <?php if($total_pages == 1){ ?>
                    <li class="active"><a href="<?php echo '?page='.$page ?>"><?php echo $page ?></a></li>
                <?php }else if($total_pages == 2){ ?>
                    <li class="<?php if($page == 1){ echo 'active'; } else { echo 'waves-effect';} ?>"><a href="<?php echo '?page=1' ?>"><?php echo 1 ?></a></li>
                <?php } else{ ?>
                <li class="<?php if($page == 1){ echo 'active'; } else { echo 'waves-effect';} ?>"><a href="<?php echo '?page='.($page == 1 ? 1 : ($page == $total_pages ? ($page - 2 > 0 ? $page - 2 : $page - 1) : $page - 1)) ?>"><?php echo $page == 1 ? 1 : ($page == $total_pages ? $page - 2 : $page - 1)?></a></li>
                    <?php } ?>
                <!-- Nút thứ 2 -->
                <?php if($total_pages == 2){ ?>
                <li class="<?php if($page == 1){ echo 'waves-effect'; } else { echo 'active';} ?>"><a href="<?php echo '?page=2' ?>"><?php echo $page == 1 ? 2 : $page?></a></li>
                <?php } ?>

                <?php if($total_pages >= 3){ ?>
                <li class="<?php if($page == 1 || $page == $total_pages){ echo 'waves-effect'; } else { echo 'active';} ?>"><a href="<?php echo '?page='.($page == 1 ? 2 : ($page == $total_pages ? $page - 1 : $page)) ?>"><?php echo $page == 1 ? 2 : ($page == $total_pages ? $page - 1 : $page) ?></a></li>
                <?php } ?>

                <?php if($total_pages >=3){ ?>
                <li class="<?php if($page == $total_pages){ echo 'active'; } else { echo 'wave-effect';} ?>"><a href="<?php echo '?page='.($page == 1 ? 3 : ($page + 1 > $total_pages ? $page : $page + 1)) ?>"><?php echo $page == 1 ? 3 : ($page + 1 > $total_pages ? $page : $page + 1)?></a></li>
                <?php } ?>


                <li class="<?php if($page >= $total_pages){ echo 'disabled'; } else { echo 'waves-effect';} ?>">
                    <a href="<?php if($page >= $total_pages){ echo '#'; } else { echo "?page=".($page + 1); } ?>"><i class="material-icons">chevron_right</i></a>
                </li>
                <li class="<?php if($page == $total_pages){ echo 'disabled'; } else { echo 'waves-effect';} ?>"><a href="?page=<?php echo $total_pages; ?>"><i class="material-icons">last_page</i></a></li>
        </ul>
        <?php if (isset($_SESSION['userId'])) { ?>
        <a class="btn waves-effect waves-light" style="background-color: var(--primary-orange);" href="./addarticle.php">Đăng bài viết mới</a>
        <?php } ?>
    </main>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"
        integrity="sha512-bLT0Qm9VnAYZDflyKcBaQ2gg0hSYNQrJ8RilYldYQ1FxQYoCLtUjuuRuZo+fjqhx/qtq/1itJ0C2ejDxltZVFg=="
        crossorigin="anonymous">
    </script>
    <script>
        $('.article-nav').addClass('active');
    </script>
</body>
</html>