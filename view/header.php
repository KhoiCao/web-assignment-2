<?php session_start(); ?>
    <header class="header">
        <div class="container">
            <div class="row sign-log-in">
                <div class="col s12 right-align row">
                    <?php 
                        if (!isset($_SESSION['userId'])) {
                    ?>
                        <a href="./signup.php" class="signup"> <i class="material-icons">edit</i> Đăng ký </a>
                        <a href="./login.php" class="login"> <i class="material-icons">account_circle</i> Đăng nhập </a>
                    <?php } else { ?>
                        <a href="./editprofile.php" class="signup"> <i class="material-icons">account_circle</i> <?php echo $_SESSION['lastname'] ?> </a>
                        <?php if($_SESSION['isAdmin'] == 0) {?> <a href="./cart.php" class="signup"> <i class="material-icons">shopping_cart</i>Giỏ hàng</a> <?php } else {?>
                        <a href="./admin/listuser.php" class="signup"> <i class="material-icons">settings</i>Trang admin</a> <?php } ?>
                        <a href="../controller/logout.php" class="login"> <i class="material-icons">account_circle</i> Đăng xuất </a>
                    <?php } ?>
                </div>

                <div class="col s12">
                    <div class="logo">
                        <a href="./homepage.php"><img src="./images/icons/logo.png" alt=""></a>
                    </div>
                </div>
            </div>
        </div>
    </header>

    <nav id="main-nav">
        <div class="container">
            <a href="#" data-target="mobile-nav" class="sidenav-trigger"><i class="material-icons">menu</i></a>
            <ul id="nav-mobile" class="hide-on-med-and-down">
                <li class="homepage-nav"><a href="./homepage.php">Trang chủ</a></li>
                <li class="intro-nav"><a href="./about.php">Giới thiệu</a></li>
                <li class="product-nav"><a href="./products.php">Sản phẩm</a></li>
                <li class="guitar-nav"><a href="./guitar.php">Guitar</a></li>
                <li class="piano-nav"><a href="./piano.php">Piano - Organs</a></li>
                <li class="keyboard-nav"><a href="./keyboard.php">Trống - Bộ gõ</a></li>
                <li class="contact-nav"><a href="./contact.php">Liên hệ</a></li>
                <li class="article-nav"><a href="./articles.php">Tin tức</a></li>
            </ul>
            <div class="search-bar">
                <div class="search-form-container">
                    <form>
                        <input type="search" placeholder="Tìm kiếm sản phẩm" class="browser-default nav-search-field" id="search">
                        <span class="nav-search-button-container">
                            <button id="nav-search-btn" class="browser-default search-btn valign-wrapper" onclick="getSearch();" type="button" ><i class="material-icons">search</i></button>
                        </span>
                    </form>
                </div>
            </div>
        </div>
    </nav>
    
    <ul class="sidenav" id="mobile-nav">
        <li class=""><a href="./homepage.php">Trang chủ</a></li>
        <li class=""><a href="./about.php">Giới thiệu</a></li>
        <li class=""><a href="./products.php">Sản phẩm</a></li>
        <li class=""><a href="./guitar.php">Guitar</a></li>
        <li class=""><a href="./piano.php">Piano - Organs</a></li>
        <li class=""><a href="./keyboard.php">Trống - Bộ gõ</a></li>
        <li class=""><a href="./contact.php">Liên hệ</a></li>
        <!-- <li class=""><a href="#">Tin tức</a></li> -->
    </ul>
    <script>
    function getSearch() { 
        location.href='./search.php?search=' + document.getElementById('search').value;
        }
    </script>
