<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Compiled and minified CSS -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">

    <!-- Compiled and minified JavaScript -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>

    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

    <link rel="stylesheet" href="./css/style.css">
    <link rel="stylesheet" href="./css/login.css">
    <link rel="stylesheet" href="css/footer.css">
    <title>Đăng nhập</title>
</head>

<body>
    <?php include('./header.php'); ?>
    <div class="container">
        <div class="login-form">
            <h5 class="form-title center-align">Đăng nhập</h5>
            <form class="" method="POST">
                <div class="row">
                    <div class="input-field col s12">
                        <input id="email" type="email" class="validate" name="email">
                        <label for="email">Email</label>
                    </div>
                </div>
                <div class="row">
                    <div class="input-field col s12">
                        <input id="password" type="password" class="validate" name="password">
                        <label for="password">Mật khẩu</label>
                    </div>
                </div>
                <div class="center-align">
                    <button id="login-btn" class="btn waves-effect waves-light" name="action">Đăng nhập
                        <i class="material-icons right">send</i>
                    </button>
                </div>
            </form>
            <div class="support_options center-align">
                <div>
                    <a href="#">Quên mật khẩu?</a>
                </div>
                <div>
                    <a href="./signup.php">Tạo tài khoản</a>
                </div>
            </div>
        </div>
    </div>
    
    <?php include('./footer.php'); ?>

    <button class="btn-floating waves-effect waves-light" onclick="topFunction()" id="scrollTop" title="Go to top"><i
            class="material-icons">arrow_upward</i></button>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"
        integrity="sha512-bLT0Qm9VnAYZDflyKcBaQ2gg0hSYNQrJ8RilYldYQ1FxQYoCLtUjuuRuZo+fjqhx/qtq/1itJ0C2ejDxltZVFg=="
        crossorigin="anonymous">
    </script>
    <script src="./js/scrollToTop.js"></script>
    <script>
        document.addEventListener('DOMContentLoaded', function () {
            var elems = document.querySelectorAll('.sidenav');
            var instances = M.Sidenav.init(elems);
        });
    </script>
    <script src="./js/login.js"></script>
</body>

</html>