<?php 
    session_start();
    if(!isset($_SESSION['userId'])) header('location: ../login.php');
    if ($_SESSION['isAdmin'] == 0) header('location: ../homepage.php');
?>
    <header class="header">
        <div class="container">
            <div class="row sign-log-in">
                <div class="col s12 right-align row">
                    <a href="../editprofile.php" class="signup"> <i class="material-icons">account_circle</i> <?php echo $_SESSION['lastname'] ?> </a>
                    <a href="../cart.php" class="signup"> <i class="material-icons">shopping_cart</i>Giỏ hàng</a>
                    <a href="../../controller/logout.php" class="login"> <i class="material-icons">account_circle</i> Đăng xuất </a>
                </div>

                <div class="col s12">
                    <div class="logo">
                        <a href="../homepage.php"><img src="../images/icons/logo.png" alt=""></a>
                    </div>
                </div>
            </div>
        </div>
    </header>
    <nav id="main-nav">
        <div class="container">
            <a href="#" data-target="mobile-nav" class="sidenav-trigger"><i class="material-icons">menu</i></a>
            <ul id="nav-mobile" class="hide-on-med-and-down">
                <li class="homepage-nav"><a href="../homepage.php">Trang chủ</a></li>
                <li class="user-nav"><a href="./listuser.php">Danh sách người dùng</a></li>
                <li class="product-nav"><a href="./listproduct.php">Danh sách sản phẩm</a></li>
                <li class="order-nav"><a href="./listorder.php">Danh sách hóa đơn</a></li>
                <li class="tag-nav"><a href="./listtag.php">Danh sách tag</a></li>
                <li class="banner-nav"><a href="./listbanner.php">Danh sách banner</a></li>
            </ul>
        </div>
    </nav>
    
    <ul class="sidenav" id="mobile-nav">
        <li class=""><a href="../homepage.php">Trang chủ</a></li>
        <li class=""><a href="./listuser.php">Danh sách người dùng</a></li>
        <li class=""><a href="./listproduct.php">Danh sách sản phẩm</a></li>
        <li class=""><a href="./listorder.php">Danh sách hóa đơn</a></li>
        <li class=""><a href="./listtag.php">Danh sách tag</a></li>
        <li class=""><a href="./listbanner.php">Danh sách banner</a></li>
    </ul>