<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Cập nhật tag</title>
    <!-- Compiled and minified CSS -->
    <link
      rel="stylesheet"
      href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css"
    />
    <script src="//cdn.ckeditor.com/4.15.1/standard/ckeditor.js"></script>

    <!-- Compiled and minified JavaScript -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>
    <link
      href="https://fonts.googleapis.com/icon?family=Material+Icons"
      rel="stylesheet"
    />
    <link
      rel="stylesheet"
      href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/css/all.min.css"
    />
    <link rel="stylesheet" href="../css/style.css">
    <script src="../js/updateProduct.js"></script>
    <script
      src="https://code.jquery.com/jquery-3.5.1.min.js"
      integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0="
      crossorigin="anonymous"
    ></script>
  </head>
  <body>
  <?php
    include "../../model/tag.php";
    include('./header.php');
    $tagModel = new TagModel();
    $tagModel->conn->set_charset("utf8");
    // $productId = 2;
    // $data = $product->getProduct($productId);
    $tagId = $_GET['id'];
    $tag = $tagModel->getTag($tagId);
  ?>
    <div class="container">
        <h2 class="center" style="font-size: 35px;">Cập nhật tag</h2>
        <div class="row">
            <form class="col s12">
                <div class="row">
                <!-- <input readonly type="hidden" id="id" name="tagid" value="<?php echo $tag['id']; ?>" required/> -->
                <div class="col s12 input-field">
                    <label for="name">Tên tag: </label>
                    <input type="text" id="tagname" name="tagname" value="<?php echo $tag['tagname']; ?>" required/>
                </div>
                </div>
                <div class="row">
                    <div class="submit-container center" style="margin-top: 20px;">
                    <button class="btn waves-effect wave-light" type="submit" name="submit" id="edit-tag-btn">
                        Cập nhật tag<i class="material-icons right">send</i>
                    </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <script>
        const id = <?php echo $tag['id'];?>;
    </script>
    <script>
        const editTagBtn = document.getElementById('edit-tag-btn');
        editTagBtn.addEventListener('click', function (e) {
            const tagName = document.querySelector('#tagname').value;
            e.preventDefault();
            if(tagName.length > 0) {
                editTagBtn.setAttribute('disabled','true');
                $.post('../../controller/edittag.php', {id: id, tagname: tagName} , function (data, status) {
                    data = JSON.parse(data);
                    editTagBtn.removeAttribute('disabled');
                    if(data.success) {
                        window.location.replace('./listtag.php');
                    }
                    else alert('Có lỗi xảy ra, vui lòng thử lại');
                })
            }
            else alert('Vui lòng nhập tên tag');
        })
    </script>
  </body>
</html>
