<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Cập nhật sản phẩm</title>
    <!-- Compiled and minified CSS -->
    <link
      rel="stylesheet"
      href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css"
    />
    <script src="//cdn.ckeditor.com/4.15.1/standard/ckeditor.js"></script>

    <!-- Compiled and minified JavaScript -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>
    <link
      href="https://fonts.googleapis.com/icon?family=Material+Icons"
      rel="stylesheet"
    />
    <link
      rel="stylesheet"
      href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/css/all.min.css"
    />
    <link rel="stylesheet" href="../css/style.css">
    <script src="../js/updateProduct.js"></script>
    <script
      src="https://code.jquery.com/jquery-3.5.1.min.js"
      integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0="
      crossorigin="anonymous"
    ></script>
  </head>
  <body>
  <?php
    include('./header.php');
    include "../../model/product.php";
    $product = new ProductModel();
    $product->conn->set_charset("utf8");
    // $productId = 2;
    // $data = $product->getProduct($productId);
    $productId = $_GET['id'];
    $data = $product->getProduct($productId);
  ?>
    <div class="container">
      <h2 class="center" style="font-size: 35px;">Cập nhật thông tin sản phẩm</h2>
      <div class="row">
        <form enctype="multipart/form-data" class="col s12" onsubmit="return checkForm();" method="post" action="../../controller/updateProduct.php">
          <div class="row">
            <input readonly type="hidden" id="idP" name="idP" value="<?php echo $data['id']; ?>" required/>
            <div class="col s12 input-field">
              <label for="name">Tên sản phẩm: </label>
              <input type="text" id="name" name="name" value="<?php echo $data['name']; ?>" required/>
            </div>
          </div>
          <div class="row">
            <div class="col s6 input-field">
              <label for="price">Giá sản phẩm: </label>
              <input
                type="number"
                id="price"
                name="price"
                min="0"
                value="<?php echo $data['price']; ?>"
                required
              />
            </div>
            <div class="col s6 input-field">
              <label for="qty">Số lượng: </label>
              <input
                type="number"
                id="qty"
                name="qty"
                min="0"
                max="100"
                value="<?php echo $data['inStock']; ?>"
                required
              />
            </div>
          </div>
          <div class="row">
            <div class="col s6 input-field">
              <label for="soldout">Số lượng đã bán: </label>
              <input type="number" id="soldout" name="soldout" value="<?php echo $data['soldAmount']; ?>" />
            </div>
            <div class="col s6 input-field">
              <label for="rating">Bình chọn: </label>
              <input type="number" id="rating" name="rating" step="0.5" min="0" max="5" value="<?php echo $data['rating']; ?>" />
            </div>
          </div>
          <div class="row">
            <div class="col s12 input-field">
              <label for="brand">Thương hiệu: </label>
              <input type="text" id="brand" name="brand" value="<?php echo $data['brand']; ?>" />
            </div>
            <div class="tag-container">
              <label style="font-size: 16px">Tag sản phẩm: </label>
              <br />
              <?php
                $tagsP = $product->getProductTags($productId);
                include "../../model/tag.php";
                $tagmodel = new TagModel();
                $tagmodel->conn->set_charset("utf8");
                $tags = $tagmodel->getAllTags();
                // foreach ($tagsP as $tagP) {
                //   echo $tagP['tagid'];
                // }
                foreach ($tags as $tag):
                  $tagid = $tag['id'];
              ?>
              <p>
                <label for="tag<?php echo $tag['id']; ?>">
                  <input type="checkbox" 
                    name="tag[]" 
                    id="tag<?php echo $tag['id']; ?>" 
                    value="<?php echo $tag['id']; ?>" 
                    <?php foreach ($tagsP as $tagP) { echo $tagP['tagid'] == $tagid ? "checked" : ""; } ?> 
                    />
                  <span><?php echo $tag['tagname']; ?></span>
                </label>
              </p>
              <?php
                endforeach;
              ?>
            </div>
            <div class="type-container">
              <label style="font-size: 16px">Loại sản phẩm: </label>
              <br />
              <label>
                <input class="with-gap" name="typeProduct" type="radio" value="Piano" <?php echo $data['type'] == "Piano" ? "checked" : "" ?> />
                <span>Piano</span>
              </label>
              <label>
                <input class="with-gap" name="typeProduct" type="radio" value="Organ" <?php echo $data['type'] == "Organ" ? "checked" : "" ?> />
                <span>Organ</span>
              </label>
              <label>
                <input class="with-gap" name="typeProduct" type="radio" value="Guitar" <?php echo $data['type'] == "Guitar" ? "checked" : "" ?> />
                <span>Guitar</span>
              </label>
              <label>
                <input class="with-gap" name="typeProduct" type="radio" value="Drum" <?php echo $data['type'] == "Drum" ? "checked" : "" ?> />
                <span>Trống</span>
              </label>
              <label>
                <input class="with-gap" name="typeProduct" type="radio" value="Violin" <?php echo $data['type'] == "Violin" ? "checked" : "" ?> />
                <span>Violin</span>
              </label>
            </div>
            <div class="row">
              <div class="col s12 file-field input-field">
                <div class="btn">
                  <span>Chỉnh sửa hình ảnh</span>
                  <input
                    id="filesImage"
                    type="file"
                    name='fileUpload'
                    onchange="previewFile()"
                    accept="image/*"
                  />
                </div>
                <div class="file-path-wrapper">
                  <input
                    class="file-path validate"
                    type="text"
                    placeholder="Cập nhật hình ảnh cho sản phẩm"
                  />
                </div>
              </div>
              <img
                class="col s4 offset-s4 responsive-img center-align"
                id="imageUpload"
                src="
                <?php
                  $src = explode("/view", $data['thumbnailurl']);
                  echo "..".$src[1]; 
                ?>"
              />
              <span class="col s4"></span>
            </div>
            <div class="desc-product" style="margin-bottom: 20px">
              <label style="font-size: 16px">Mô tả sản phẩm</label>
              <textarea name="desc" id="desc"><?php echo $data['description']; ?></textarea>
              <script>
                CKEDITOR.replace("desc");
              </script>
            </div>
            <div class="detail-product">
              <label style="font-size: 16px">Chi tiết sản phẩm</label>
              <textarea name="detail" id="detail"><?php echo $data['detail']; ?></textarea>
              <script>
                CKEDITOR.replace("detail");
              </script>
            </div>
            <div class="row">
              <div class="submit-container center" style="margin-top: 20px;">
                <button class="btn waves-effect wave-light" type="submit" name="submit">
                  Cập nhật sản phẩm<i class="material-icons right">send</i>
                </button>
              </div>
            </div>
          </form>
        </div>
      </div>
  </body>
</html>
