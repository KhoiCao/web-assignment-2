<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Compiled and minified CSS -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">

    <!-- Compiled and minified JavaScript -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/css/all.min.css">
    <link rel="stylesheet" href="../css/style.css">
    <script
      src="https://code.jquery.com/jquery-3.5.1.min.js"
      integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0="
      crossorigin="anonymous"
    ></script>
    <script src="../js/addBanner.js"></script>
    <title>Danh sách banner</title>
</head>

<body>
    <?php include('./header.php'); ?>
    <div class="container">
        <h4 class="center" ><span style="color: #455a64;">Danh sách banner</span></h4>
        <table class="highlight centered" style="width: 100%">
            <thead>
                <tr>
                    <th style="width: 70%">Hình ảnh</th>
                    <th style="width: 30%">Hành động</th>
                </tr>
            </thead>
            <tbody>
                <?php
                    include("../../model/banner.php");
                    $bannerModel = new BannerModel();
                    $banners = $bannerModel->getAllBanners();
                    if(!empty($banners)) :
                        foreach($banners as $banner) :
                ?>
                <tr>
                    <td><img src="
                            <?php 
                                $src = explode("/view", $banner['bannerurl']);
                                echo "..".$src[1]; 
                            ?>" 
                            alt="jpg" 
                            class="responsive-img">
                    </td>
                    <td>
                        <a href="../../controller/editbanner.php?id=<?php echo $banner['id']; ?>" class='green btn-small'>
                            <i class='fas fa-edit'></i>
                        </a>
                        <a href="../../controller/deletebanner.php?id=<?php echo $banner['id']; ?>" class='red btn-small' onclick='return confirm("Bạn có chắc muốn xóa banner này?");'>
                            <i class='fas fa-trash'></i>
                        </a>
                    </td>
                </tr>
                <?php endforeach; endif; 
                    if(empty($banners)) :
                ?>
                <tr>
                    <td colspan="2">Không có banner nào.</td>
                </tr>
                <?php endif; ?>
            </tbody>
        </table>
        <div class="row">
        <form enctype="multipart/form-data" method="POST" onsubmit="return checkForm();" action="../../controller/addbanner.php">
            <div class="file-field input-field">
                <div class="btn">
                    <span>Tải banner lên</span>
                    <input id="fileImage"
                    type="file"
                    name='fileUpload'
                    onchange="previewFile()"
                    accept="image/*"
                    required>
                </div>
                <div class="file-path-wrapper">
                    <input class="file-path validate" type="text">
                </div>
            </div>
            <img
              class="col s8 offset-s2 responsive-img center-align"
              id="imageUpload"
            />
            <span class="col s2"></span>
            <div class="col s12 center-align" style="margin-top: 15px;">
                <input class="btn" type="submit" value="Thêm banner" name="add_banner">
            </div>
        </form>
        </div>
    </div>

    <script>
        $('.banner-nav').addClass('active');
    </script>
</body>

</html>