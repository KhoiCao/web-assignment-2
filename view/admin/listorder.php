<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Compiled and minified CSS -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">
    
    <!-- Compiled and minified JavaScript -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/css/all.min.css">
    <link rel="stylesheet" href="./css/listuser.css">
    <style>
        body{
            display: flex;
            flex-direction: column;
            min-height: 100vh;
        }
        main {
            flex-grow: 1;
        }
        .pagination li.active {
            background-color: var(--primary-orange) !important;
        }
    </style>
    <link rel="stylesheet" href="../css/style.css">
    <title>Danh sách hóa đơn</title>
</head>
<body>
    <?php 
        include('./header.php');
        include('../../model/order.php'); 
        include('../../model/user.php');
        $orderModel = new OrderModel();
        $userModel = new UserModel();
        if (isset($_GET['page'])) {
            $page = $_GET['page'];
        } else {
            $page = 1;
        }
        $page_size = 10;
        $offset = ($page-1) * $page_size;
        $orders = $orderModel->getOrdersByRange($offset, $page_size);
        $total_rows = $orderModel->getNumberOfOrders();
        $total_pages = ceil($total_rows / $page_size);
    ?>

    <main>
        <div class="container">Có tổng cộng <span style='font-weight: bold'><?php echo $total_pages ?></span> trang</div>
        <table class="table table-striped container">
            <thead>
                <tr>
                    <th class="center-align">Số thứ tự</th>
                    <th class="center-align">Người đặt hàng</th>
                    <th class="center-align">Email</th>
                    <th class="center-align">Thời gian đặt</th>
                    <th class="center-align">Trạng thái</th>
                    <th class="center-align">Hành động</th>
                </tr>
            </thead>
            <tbody>

                <?php
                    $i = 1 + $offset;
                    foreach($orders as $order) {
                        $user = $userModel->getUser($order['userid']);
                ?>
                <tr>
                    <td class="center-align"><?php echo $i; ?></td>
                    <td class="center-align"><?php echo $user['lastname']; ?></td>
                    <td class="center-align"><?php echo $user['email']; ?></td>
                    <td class="center-align"><?php echo $order['createdat']; ?></td>
                    <td class="center-align"><?php if($order['status'] == 'pending') echo 'Đang xử lý'; else if($order['status'] == 'confirmed') echo 'Đã xác nhận'; else echo 'Đã từ chối'; ?></td>
                    <td class="center-align"><a href=<?php echo "./orderdetail.php?id=".$order['id'];?>><i style="color: black;" class="material-icons small">info</i></a></td>
                </tr>
                <?php $i++; } ?>

            </tbody>
        </table>
        <ul class="pagination center-align" style="margin-top: 10px;">
            <li class="<?php if($page == 1){ echo 'disabled'; } else { echo 'waves-effect';} ?>"><a href="?page=1"><i class="material-icons">first_page</i></a></li>
                <li class="<?php if($page <= 1){ echo 'disabled'; } else { echo 'waves-effect';} ?>">
                    <a href="<?php if($page <= 1){ echo '#'; } else { echo "?page=".($page - 1); } ?>"><i class="material-icons">chevron_left</i></a>
                </li>
                <!-- Nút đầu tiên -->
                <?php if($total_pages == 1){ ?>
                    <li class="active"><a href="<?php echo '?page='.$page ?>"><?php echo $page ?></a></li>
                <?php }else if($total_pages == 2){ ?>
                    <li class="<?php if($page == 1){ echo 'active'; } else { echo 'waves-effect';} ?>"><a href="<?php echo '?page=1' ?>"><?php echo 1 ?></a></li>
                <?php } else{ ?>
                <li class="<?php if($page == 1){ echo 'active'; } else { echo 'waves-effect';} ?>"><a href="<?php echo '?page='.($page == 1 ? 1 : ($page == $total_pages ? ($page - 2 > 0 ? $page - 2 : $page - 1) : $page - 1)) ?>"><?php echo $page == 1 ? 1 : ($page == $total_pages ? $page - 2 : $page - 1)?></a></li>
                    <?php } ?>
                <!-- Nút thứ 2 -->
                <?php if($total_pages == 2){ ?>
                <li class="<?php if($page == 1){ echo 'waves-effect'; } else { echo 'active';} ?>"><a href="<?php echo '?page=2' ?>"><?php echo $page == 1 ? 2 : $page?></a></li>
                <?php } ?>

                <?php if($total_pages >= 3){ ?>
                <li class="<?php if($page == 1 || $page == $total_pages){ echo 'waves-effect'; } else { echo 'active';} ?>"><a href="<?php echo '?page='.($page == 1 ? 2 : ($page == $total_pages ? $page - 1 : $page)) ?>"><?php echo $page == 1 ? 2 : ($page == $total_pages ? $page - 1 : $page) ?></a></li>
                <?php } ?>

                <?php if($total_pages >=3){ ?>
                <li class="<?php if($page == $total_pages){ echo 'active'; } else { echo 'wave-effect';} ?>"><a href="<?php echo '?page='.($page == 1 ? 3 : ($page + 1 > $total_pages ? $page : $page + 1)) ?>"><?php echo $page == 1 ? 3 : ($page + 1 > $total_pages ? $page : $page + 1)?></a></li>
                <?php } ?>


                <li class="<?php if($page >= $total_pages){ echo 'disabled'; } else { echo 'waves-effect';} ?>">
                    <a href="<?php if($page >= $total_pages){ echo '#'; } else { echo "?page=".($page + 1); } ?>"><i class="material-icons">chevron_right</i></a>
                </li>
                <li class="<?php if($page == $total_pages){ echo 'disabled'; } else { echo 'waves-effect';} ?>"><a href="?page=<?php echo $total_pages; ?>"><i class="material-icons">last_page</i></a></li>
        </ul>
    </main>
    <script>
        $('.order-nav').addClass('active');
    </script>
</body>
</html>