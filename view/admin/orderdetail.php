<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Compiled and minified CSS -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">
    
    <!-- Compiled and minified JavaScript -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>
    
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href="../css/style.css">
    <style>
        .order-summary {
            max-width: 300px;
            margin-right: 0;
            margin-left: auto;
            display: flex;
            flex-direction: column;
            justify-content: center;
            align-items: center;
        }
        .order-summary div {
            margin: 10px 0;
        }
    </style>
    <title>Chi tiết hóa đơn</title>
</head>
<body>
    <?php include('./header.php'); ?>
    <?php 
        include('../../model/order.php');
        include('../../model/user.php');
        include('../../model/product.php');
        $userModel = new UserModel();
        $orderModel = new OrderModel();
        $productModel = new ProductModel();

        $orderId = $_GET['id'];
        $order = $orderModel->getOrder($orderId);
        $user = $userModel->getUser($order['userid']);
        $items = $orderModel->getItemsInOrder($order['id']);
    ?>
    <div class="container">
        <p>Mã hóa đơn: <?php echo $order['id'];?></p>
        <p>Người đặt hàng: <?php echo $user['firstname']." ".$user['lastname'];?></p>
        <p>Email: <?php echo $user['email'];?></p>
        <p>Địa điểm: <?php echo $user['address'];?></p>
        <p>Thời gian đặt hàng: <?php echo $order['createdat'];?></p>
        <p>Tình trạng: <?php if($order['status'] == 'pending') echo 'Đang xử lý'; else if($order['status'] == 'confirmed') echo 'Đã xác nhận'; else echo 'Đã từ chối'; ?></p>
        <div>
            <p style="font-weight: 700;">Các sản phẩm được đặt: </p>
            <table style="margin: 50px auto;">
                <!-- <caption style="font-size: 30px">Giỏ hàng</caption> -->
                <thead>
                    <tr>
                        <th class="center-align">STT</th>
                        <th class="center-align">Tên sản phẩm</th>
                        <th class="center-align">Đơn giá</th>
                        <th class="center-align">Số lượng được đặt</th>
                        <th class="center-align">Số lượng hàng trong kho</th>
                        <th class="center-align">Thành tiền</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $i = 1; $total = 0; foreach($items as $item) {    
                        $product = $productModel->getProduct($item['productid']);
                    ?>
                    <tr id="<?php echo $product['id'];?>">
                        <td class="center-align" ><?php echo $i; ?></td>
                        <td class="center-align"><?php echo $product['name']; ?></td>
                        <td class="center-align product-price"><?php echo number_format(($product['price']),0,",",".")." "; ?>₫</td>
                        <td class="center-align quantity_input"> <?php echo $item['quantity']; ?></td>
                        <td class="center-align "> <?php echo $product['inStock']; ?></td>
                        <td class="center-align price-display"><?php echo number_format(($item['quantity'] * $product['price']),0,",",".")." ";?>₫</td>
                    </tr>
                    <?php $i++; $total+= $product['price'] * $item['quantity']; } ?>
                </tbody>
            </table>
            <div class="order-summary">
                <div>
                    <span>Tổng tiền: </span>
                    <div style="display: inline; font-weight: 700;" class="total-price"><?php echo number_format(($total),0,",",".")." "; ?>₫</div>
                </div>
                <?php if ($order['status'] != 'confirmed') { ?>
                <div class="btn-options">
                    <a class="btn waves-effect waves-light" href=<?php echo "../../controller/confirmorder.php?id=".$order['id']; ?>>Xác nhận</a>
                    <a class="btn waves-effect waves-light" href=<?php echo "../../controller/declineorder.php?id=".$order['id']; ?>>Từ chối</a>
                </div>
                <?php } ?>
            </div>
        </div>
        
    </div>
</body>
</html>