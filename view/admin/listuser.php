<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Compiled and minified CSS -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">
    
    <!-- Compiled and minified JavaScript -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/css/all.min.css">
    <link rel="stylesheet" href="../css/listuser.css">
    <link rel="stylesheet" href="../css/style.css">
    <style>
        .pagination li.active {
            background-color: var(--primary-orange) !important;
        }
    </style>
    <title>Danh sách người dùng</title>
</head>
<body>
    <?php include('./header.php');?>
    <main>
    <?php
        $servername = "localhost";
        $username = "root";
        $password = "";
        $dbname = "demo";
        // $dbname = "db_web_assignment";
        if (isset($_GET['pageno'])) {
            $pageno = $_GET['pageno'];
        } else {
            $pageno = 1;
        }
        $no_of_records_per_page = 10;
        $offset = ($pageno-1) * $no_of_records_per_page;

        $conn=mysqli_connect($servername,$username,$password,$dbname);
        // Check connection
        if (mysqli_connect_error()){
            echo "Failed to connect to MySQL: " . mysqli_connect_error();
            die();
        }

        $total_pages_sql = "SELECT COUNT(*) FROM user";
        $result = mysqli_query($conn,$total_pages_sql);
        $total_rows = mysqli_fetch_array($result)[0];
        $total_pages = ceil($total_rows / $no_of_records_per_page);

        $sql = "SELECT * FROM user LIMIT $offset, $no_of_records_per_page";
        $res_data = mysqli_query($conn,$sql);
    ?>
        <div>Có tổng cộng <span style='font-weight: bold'><?php echo $total_pages ?></span> trang</div>
        <table class="table table-striped">
            <thead>
                <tr>
                    <th style='text-align:center'>Số thứ tự</th>
                    <th style='text-align:center'>Họ</th>
                    <th style='text-align:center'>Tên</th>
                    <th style='text-align:center'>Email</th>
                    <th style='text-align:center'>Tuổi</th>
                    <th style='text-align:center'>Giới tính</th>
                    <th style='text-align:center'>Số điện thoại</th>
                    <th style='text-align:center'>Địa chỉ</th>
                    <th style='text-align:center'>Trạng thái</th>
                    <th style='text-align:center'>Hành động</th>
                </tr>
            </thead>
            <tbody>
                <?php
                $ordinal = 1 + $offset;
                while($row = mysqli_fetch_array($res_data)){
                    //here goes the data
                    ?>
                        <tr>
                            <td style='text-align:center'><?php echo $ordinal; ?></td>
                            <td style='text-align:center'><?php echo $row['firstname']; ?></td>
                            <td style='text-align:center'><?php echo $row['lastname']; ?></td>
                            <td style='text-align:center'><?php echo $row['email']; ?></td>
                            <td style='text-align:center'><?php echo $row['age']; ?></td>
                            <td style='text-align:center'><?php echo $row['gender']; ?></td>
                            <td style='text-align:center'><?php echo $row['phone']; ?></td>
                            <td style='text-align:center'><?php echo $row['address']; ?></td>
                            <td style='text-align:center'><?php if($row['active'] == 1){ echo 'Đã kích hoạt';}else{ echo 'Chờ kích hoạt';} ; ?></td>
                            <td style='text-align:center'><a class='waves-effect waves-light btn-small btn-small modal-trigger' href=<?php echo '#modal'.$row['id'];?>><i class="fas fa-user-check"></i></a>
                                
                                    <div id=<?php echo "modal".$row['id'];?> class="modal">
                                        <div class="modal-content">
                                            <h5>Xác nhận thay đổi trạng thái của người dùng: <?php echo $row['firstname'].' '.$row['lastname'] ?></h5>
                                        </div>
                                        <div class="modal-footer">
                                            <a class="modal-close waves-effect waves-green btn-flat">Quay lại</a>
                                            <a href="<?php echo "../../controller/switchActive.php?id=".$row['id']."&page=".$pageno; ?>" class="modal-close waves-effect waves-green btn-flat">Đồng ý</a>
                                        </div>
                                    </div> 
                            </td>
                            </tr>
                    <?php
                    $ordinal++;
                }
                mysqli_close($conn);
            ?>
            </tbody>
        </table>
        <ul class="pagination center-align" style="margin-top: 10px;">
            <li class="<?php if($pageno == 1){ echo 'disabled'; } else { echo 'waves-effect';} ?>"><a href="?pageno=1"><i class="material-icons">first_page</i></a></li>
                <li class="<?php if($pageno <= 1){ echo 'disabled'; } else { echo 'waves-effect';} ?>">
                    <a href="<?php if($pageno <= 1){ echo '#'; } else { echo "?pageno=".($pageno - 1); } ?>"><i class="material-icons">chevron_left</i></a>
                </li>
                <!-- Nút đầu tiên -->
                <?php if($total_pages == 1){ ?>
                    <li class="active"><a href="<?php echo '?pageno='.$pageno ?>"><?php echo $pageno ?></a></li>
                <?php }else if($total_pages == 2){ ?>
                    <li class="<?php if($pageno == 1){ echo 'active'; } else { echo 'waves-effect';} ?>"><a href="<?php echo '?pageno=1' ?>"><?php echo 1 ?></a></li>
                <?php } else{ ?>
                <li class="<?php if($pageno == 1){ echo 'active'; } else { echo 'waves-effect';} ?>"><a href="<?php echo '?pageno='.($pageno == 1 ? 1 : ($pageno == $total_pages ? ($pageno - 2 > 0 ? $pageno - 2 : $pageno - 1) : $pageno - 1)) ?>"><?php echo $pageno == 1 ? 1 : ($pageno == $total_pages ? $pageno - 2 : $pageno - 1)?></a></li>
                    <?php } ?>
                <!-- Nút thứ 2 -->
                <?php if($total_pages == 2){ ?>
                <li class="<?php if($pageno == 1){ echo 'waves-effect'; } else { echo 'active';} ?>"><a href="<?php echo '?pageno=2' ?>"><?php echo $pageno == 1 ? 2 : $pageno?></a></li>
                <?php } ?>

                <?php if($total_pages >= 3){ ?>
                <li class="<?php if($pageno == 1 || $pageno == $total_pages){ echo 'waves-effect'; } else { echo 'active';} ?>"><a href="<?php echo '?pageno='.($pageno == 1 ? 2 : ($pageno == $total_pages ? $pageno - 1 : $pageno)) ?>"><?php echo $pageno == 1 ? 2 : ($pageno == $total_pages ? $pageno - 1 : $pageno) ?></a></li>
                <?php } ?>

                <?php if($total_pages >=3){ ?>
                <li class="<?php if($pageno == $total_pages){ echo 'active'; } else { echo 'wave-effect';} ?>"><a href="<?php echo '?pageno='.($pageno == 1 ? 3 : ($pageno + 1 > $total_pages ? $pageno : $pageno + 1)) ?>"><?php echo $pageno == 1 ? 3 : ($pageno + 1 > $total_pages ? $pageno : $pageno + 1)?></a></li>
                <?php } ?>


                <li class="<?php if($pageno >= $total_pages){ echo 'disabled'; } else { echo 'waves-effect';} ?>">
                    <a href="<?php if($pageno >= $total_pages){ echo '#'; } else { echo "?pageno=".($pageno + 1); } ?>"><i class="material-icons">chevron_right</i></a>
                </li>
                <li class="<?php if($pageno == $total_pages){ echo 'disabled'; } else { echo 'waves-effect';} ?>"><a href="?pageno=<?php echo $total_pages; ?>"><i class="material-icons">last_page</i></a></li>
        </ul>
    </main>
    <script>
        $('.user-nav').addClass('active');
    </script>
    <script>
        document.addEventListener('DOMContentLoaded', function() {
            var elems = document.querySelectorAll('.modal');
            var instances = M.Modal.init(elems);
        });
    </script>
</body>
</html>