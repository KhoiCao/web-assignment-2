<!DOCTYPE html>
<html lang="vi">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css"
        integrity="sha512-UJfAaOlIRtdR+0P6C3KUoTDAxVTuy3lnSXLyLKlHYJlcSU8Juge/mjeaxDNMlw9LgeIotgz5FP8eUQPhX1q10A=="
        crossorigin="anonymous" />
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href="./css/style.css">
    <link rel="stylesheet" href="css/products.css">
    <link rel="stylesheet" href="css/footer.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <title>Guitar</title>
</head>

<body>
    <?php include('./header.php') ?>
    <?php
        $servername = "localhost";
        $username = "root";
        $password = "";
        $dbname = "demo";
        // $dbname = "db_web_assignment";
        if (isset($_GET['pageno'])) {
            $pageno = $_GET['pageno'];
        } else {
            $pageno = 1;
        }
        if (isset($_GET['sortby'])) {
            if($_GET['sortby'] == 'name:asc'){
                $name_column='ORDER BY name ASC';
                $current_sort='&sortby='.$_GET['sortby'];
            } 
            else if($_GET['sortby'] == 'name:desc'){
                $name_column='ORDER BY name DESC';
                $current_sort='&sortby='.$_GET['sortby'];
            }else if($_GET['sortby'] == 'price:asc'){
                $name_column='ORDER BY price ASC';
                $current_sort='&sortby='.$_GET['sortby'];
            }else if($_GET['sortby'] == 'price:desc'){
                $name_column='ORDER BY name DESC';
                $current_sort='&sortby='.$_GET['sortby'];
            }
        } else {
            $name_column = "";
            $current_sort="";
        }
        $current_page="?pageno=" .$pageno;
        $no_of_records_per_page = 2;
        $offset = ($pageno-1) * $no_of_records_per_page;

        $conn=mysqli_connect($servername,$username,$password,$dbname);
        // Check connection
        if (mysqli_connect_error()){
            echo "Failed to connect to MySQL: " . mysqli_connect_error();
            die();
        }

        $total_pages_sql = "SELECT COUNT(*) FROM product WHERE type='Guitar'";
        $result = mysqli_query($conn,$total_pages_sql);
        $total_rows = mysqli_fetch_array($result)[0];
        $total_pages = ceil($total_rows / $no_of_records_per_page);

        $sql = "SELECT * FROM  product WHERE type='guitar' $name_column LIMIT $offset, $no_of_records_per_page" ;
        $res_data = mysqli_query($conn,$sql);?>
    <div class="container-products">
        <ul class="mybreadcrumb">
            <li><a href="./homepage.php">Trang chủ</a></li>
            <li><span>Guitar</span></li>
        </ul>
        <div class="row">
            <aside class="col s12 l3 m6">
                <aside class="aside-item colection-category">
                    <div class="aside-title">
                        <h3 class="title-head">
                            <span> Danh mục </span>
                        </h3>
                    </div>
                    <div class="aside- content">
                        <ul class="nav-pills">
                            <li><a href="./keyboard.php" class="nav-item">Keyboard - Violon</a></li>
                            <li><a href="./piano.php" class="nav-item" >Piano - Organs</a></li>
                            <li><a href="./guitar.php" class="nav-item active" >Guitar</a></li>
                        </ul>
                    </div>
                    <div class="aside-filter">
                        <aside class="aside-item filter-price">
                            <div class="aside-title">
                                <h2 class="title-head">
                                    <span>
                                        Giá sản phẩm
                                    </span>
                                </h2>
                            </div>
                            <div class="aside-content filter-group">
                                <ul>
                                    <li class="filter-item">
                                        <span>
                                            <label for="filter-duoi-100">
                                                <input type="checkbox" id="filter-duoi-100" value="1" />
                                                <span>Giá dưới 1.000.000đ</span>
                                            </label>
                                        </span>
                                    </li>
                                    <li class="filter-item">
                                        <span>
                                            <label for="filter-100-200">
                                                <input type="checkbox" id="filter-100-200" value="2" />
                                                <span>1.000.000vnđ - 2.000.000đ</span>
                                            </label>
                                        </span>
                                    </li>
                                    <li class="filter-item">
                                        <span>
                                            <label for="filter-200-300">
                                                <input type="checkbox" id="filter-200-300" value="3" />
                                                <span>2.000.000vnđ - 3.000.000đ</span>
                                            </label>
                                        </span>
                                    </li>
                                    <li class="filter-item">
                                        <span>
                                            <label for="filter-300-500">
                                                <input type="checkbox" id="filter-300-500" value="4" />
                                                <span>3.000.000vnđ - 5.000.000đ</span>
                                            </label>
                                        </span>
                                    </li>
                                    <li class="filter-item">
                                        <span>
                                            <label for="filter-500-1000">
                                                <input type="checkbox" id="filter-500-1000" value="5" />
                                                <span>5.000.000vnđ - 10.000.000đ</span>
                                            </label>
                                        </span>
                                    </li>
                                    <li class="filter-item">
                                        <span>
                                            <label for="filter-tren-1000">
                                                <input type="checkbox" id="filter-tren-1000" value="6" />
                                                <span>Giá trên 10.000.000vnđ</span>
                                            </label>
                                        </span>
                                    </li>
                                </ul>
                            </div>
                        </aside>
                        <aside class="aside-item filter-price">
                            <div class="aside-title">
                                <h2 class="title-head">
                                    <span>
                                        Thương hiệu
                                    </span>
                                </h2>
                            </div>
                            <div class="aside-content filter-group">
                                <ul>
                                    <li class="filter-item">
                                        <span>
                                            <label for="filter-moza-shop">
                                                <input type="checkbox" id="filter-moza-shop" value="<100000" />
                                                <span>Moza Shop</span>
                                            </label>
                                        </span>
                                    </li>
                                    <li class="filter-item">
                                        <span>
                                            <label for="filter-tien-dat">
                                                <input type="checkbox" id="filter-tien-dat" value="<100000" />
                                                <span>Tiến Đạt</span>
                                            </label>
                                        </span>
                                    </li>
                                    <li class="filter-item">
                                        <span>
                                            <label for="filter-yamaha">
                                                <input type="checkbox" id="filter-yamaha" value="<100000" />
                                                <span>Yamaha</span>
                                            </label>
                                        </span>
                                    </li>
                                    <li class="filter-item">
                                        <span>
                                            <label for="filter-yamaha-pacifica">
                                                <input type="checkbox" id="filter-yamaha-pacifica" value="<100000" />
                                                <span>Yamaha Pacifica</span>
                                            </label>
                                        </span>
                                    </li>
                                </ul>
                            </div>
                        </aside>
                    </div>
                </aside>
            </aside>
            <div class="col l9 s12">
                <div class="sortPagiBar">
                    <div class="row" style="margin-bottom: 0px;" >
                        <div class="col s5 m5 btn-group" id="btn-group">
                            <button id="btn-list" style="width: 40px; height: 40px;" onclick="viewMode('list')"><i
                                    class="fa fa-bars" aria-hidden="true"></i></button>
                            <button id="btn-grid" class="active-btn" style="width: 40px; height: 40px;"
                                    onclick="viewMode('grid')"><i class="fa fa-th" aria-hidden="true"></i></button>
                        </div>
                        <div class="input-field col s7 m7">
                            <select name="a" style="display: initial;" onchange="sortChanged(this)">
                                <option value="" disabled <?php if($current_sort == '') echo 'selected' ?>>Sắp xếp</option>
                                <option value="1" <?php if($current_sort == '&sortby=name:asc') echo 'selected' ?> >A -> Z</option>
                                <option value="2" <?php if($current_sort == '&sortby=name:desc') echo 'selected' ?>>Z -> A</option>
                                <option value="3" <?php if($current_sort == '&sortby=price:asc') echo 'selected' ?>>Giá tăng dần</option>
                                <option value="4" <?php if($current_sort == '&sortby=price:desc') echo 'selected' ?>>Giá giảm dần</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div id="grid" class="col s12 row center-align">
                <?php
                $results=array();
                while($row = mysqli_fetch_array($res_data)){
                    $results[] = $row;
                    //here goes the data
                    ?>
                    <div class="col s6 m4 l4">
                        <div class="card hoverable">
                            <div class="card-image">
                                <img src="<?php echo $row['thumbnailurl']; ?>" alt="">
                                <!-- <a class="btn-floating halfway-fab waves-effect waves-light red"><i class="material-icons">add</i></a> -->
                            </div>
                            <div class="card-content" style="padding:24px 20px">
                                <a href="<?php echo './productInfo.php?id='.$row['id'];?>" class="card-product-name"><?php echo $row['name']; ?></a>
                                <div class="rating">
                                    <?php for($i = 1; $i <= (int)$row['rating']; $i++) echo '<i class="material-icons">star</i>';?>
                                    <?php if($row['rating'] - (int)$row['rating'] >= 0.5) echo '<i class="material-icons">star_half</i>';?>
                                    <?php for($i = round($row['rating']); $i < 5; $i++) echo '<i class="material-icons">star_border</i>';?>
                                </div>
                                <p class="card-product-price"><?php echo number_format(($row['price']),0,",",".")." ";?>₫</p>
                            </div>
                        </div>
                    </div>
                    <?php
                    }
                    ?>
                </div>
                <div id="list" style="display: none;">
                    <?php foreach ($results as $key => $row) 
                    {
                        ?>
                    <div class="col s12 product-box">
                        <div class="img-list">
                            <img src="<?php echo $row['thumbnailurl']; ?>" alt="Guitar 1" class="responsive-img">
                        </div>
                        <div class="product-info-list">
                            <div class="product-name"><?php echo $row['name']; ?></div>
                            <div class="product-price"><?php echo number_format(($row['price']),0,",",".")." ";?>₫</div>
                            <div class="product-summary"><?php echo $row['description']; ?></div>
                            <div class="rating">
                                    <?php for($i = 1; $i <= (int)$row['rating']; $i++) echo '<i class="material-icons">star</i>';?>
                                    <?php if($row['rating'] - (int)$row['rating'] >= 0.5) echo '<i class="material-icons">star_half</i>';?>
                                    <?php for($i = round($row['rating']); $i < 5; $i++) echo '<i class="material-icons">star_border</i>';?>
                            </div>
                        </div>
                    </div>
                    <?php
                    }
                    ?>
                </div>
            </div>
                <ul class="pagination center-align">
        <li class="<?php if($pageno == 1){ echo 'disabled'; } else { echo 'waves-effect';} ?>"><a href=<?php $current_page="?pageno=1"; echo $current_page .$current_sort ?>><i class="material-icons">first_page</i></a></li>
        <li class="<?php if($pageno <= 1){ echo 'disabled'; } else { echo 'waves-effect';} ?>">
            <a href=<?php if($pageno <= 1){ echo '#'; } else { $current_page = "?pageno=".($pageno - 1); echo $current_page .$current_sort; } ?>><i class="material-icons">chevron_left</i></a>
        </li>
        <!-- Nút đầu tiên -->
        <?php if($total_pages == 1){ ?>
            <li class="active"><a href="<?php $current_page='?pageno='.$pageno ;echo $current_page .$current_sort ?>"><?php echo $pageno ?></a></li>
        <?php }else if($total_pages == 2){ ?>
            <li class="<?php if($pageno == 1){ echo 'active'; } else { echo 'waves-effect';} ?>"><a href="<?php $current_page='?pageno=1';echo $current_page .$current_sort ?>"><?php echo 1 ?></a></li>
        <?php } else{ ?>
        <li class="<?php if($pageno == 1){ echo 'active'; } else { echo 'waves-effect';} ?>"><a href="<?php $current_page='?pageno='.($pageno == 1 ? 1 : ($pageno == $total_pages ? ($pageno - 2 > 0 ? $pageno - 2 : $pageno - 1) : $pageno - 1));echo $current_page .$current_sort ?>"><?php echo $pageno == 1 ? 1 : ($pageno == $total_pages ? $pageno - 2 : $pageno - 1)?></a></li>
            <?php } ?>
        <!-- Nút thứ 2 -->
        <?php if($total_pages == 2){ ?>
        <li class="<?php if($pageno == 1){ echo 'waves-effect'; } else { echo 'active';} ?>"><a href="<?php $current_page='?pageno=2'; echo $current_page .$current_sort ?>"><?php echo $pageno == 1 ? 2 : $pageno?></a></li>
        <?php } ?>

        <?php if($total_pages >= 3){ ?>
        <li class="<?php if($pageno == 1 || $pageno == $total_pages){ echo 'waves-effect'; } else { echo 'active';} ?>"><a href="<?php $current_page='?pageno='.($pageno == 1 ? 2 : ($pageno == $total_pages ? $pageno - 1 : $pageno));echo $current_page.$current_sort ?>"><?php echo $pageno == 1 ? 2 : ($pageno == $total_pages ? $pageno - 1 : $pageno) ?></a></li>
        <?php } ?>

        <?php if($total_pages >=3){ ?>
        <li class="<?php if($pageno == $total_pages){ echo 'active'; } else { echo 'wave-effect';} ?>"><a href="<?php $current_page='?pageno='.($pageno == 1 ? 3 : ($pageno + 1 > $total_pages ? $pageno : $pageno + 1));echo $current_page .$current_sort ?>"><?php echo $pageno == 1 ? 3 : ($pageno + 1 > $total_pages ? $pageno : $pageno + 1)?></a></li>
        <?php } ?>


        <li class="<?php if($pageno >= $total_pages){ echo 'disabled'; } else { echo 'waves-effect';} ?>">
            <a href="<?php if($pageno >= $total_pages){ echo '#'; } else { $current_page="?pageno=".($pageno + 1);echo $current_page .$current_sort; } ?>"><i class="material-icons">chevron_right</i></a>
        </li>
        <li class="<?php if($pageno == $total_pages){ echo 'disabled'; } else { echo 'waves-effect';} ?>"><a href=<?php $current_page="?pageno=".$total_pages ;echo $current_page .$current_sort ?>><i class="material-icons">last_page</i></a></li>
    </ul>
            </div>

        </div>
    </div>
    
    <?php include('./footer.php'); ?>

    <button class="btn-floating waves-effect waves-light" onclick="topFunction()" id="scrollTop" title="Go to top"><i
            class="material-icons">arrow_upward</i></button>
    <script src="./js/scrollToTop.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"
        integrity="sha512-bLT0Qm9VnAYZDflyKcBaQ2gg0hSYNQrJ8RilYldYQ1FxQYoCLtUjuuRuZo+fjqhx/qtq/1itJ0C2ejDxltZVFg=="
        crossorigin="anonymous">
    </script>
    <script src="./js/jquerymatchHeight.js"></script>
    <script language="javascript">
        function sortChanged(obj){
            var value = obj.value;
            console.log(value)
            switch (value) {
                case "1":
                    <?php
                        $current_sort = "&sortby=name:asc";
                        ?>
                    window.location.href = "<?php echo "?pageno=".$pageno.$current_sort ?>";
                    break;
                case "2":
                    <?php
                        $current_sort = "&sortby=name:desc";
                        ?>
                    window.location.href = "<?php echo "?pageno=".$pageno.$current_sort ?>";
                    break;
                case "3":
                    <?php
                        $current_sort = "&sortby=price:asc";
                        ?>
                    window.location.href = "<?php echo "?pageno=".$pageno.$current_sort?>";
                    break;
                case "4":
                    <?php
                        $current_sort = "&sortby=price:desc";
                        ?>
                    window.location.href = "<?php echo "?pageno=".$pageno.$current_sort?>";
                    break;
            }
        }   
    </script>
    <script>
        function viewMode(viewMode) {
            var grid = document.getElementById('grid');
            var list = document.getElementById('list');
            var gridbtn = document.getElementById('btn-grid');
            var listbtn = document.getElementById('btn-list');
            if (viewMode == 'grid') {
                grid.style.display = "block";
                list.style.display = "none";
                document.getElementById("btn-grid").className = 'active-btn';
                document.getElementById("btn-list").className = '';
                setView('grid');
            }
            else {
                grid.style.display = "none";
                list.style.display = "block"
                document.getElementById("btn-list").className = 'active-btn';
                document.getElementById("btn-grid").className = '';
                setView('list');
            }
        }
        function setView(view) {
            localStorage.setItem('view', view);
        }
        function getView() {
            return localStorage.getItem('view');
        }
    </script>
    <script>
        var x = getView();
        viewMode(x);
    </script>
    <script>
        $(function () {
            $('.card').matchHeight();
            $('.card-image').matchHeight();
        });
        document.addEventListener('DOMContentLoaded', function () {
            var elems = document.querySelectorAll('.sidenav');
            var instances = M.Sidenav.init(elems);
        });
        $('.guitar-nav').addClass('active');
    </script>
</body>

</html>