<?php
    include("../model/banner.php");
    $bannerModel = new BannerModel();
    $conn = $bannerModel->conn;
    $bannerId = $_GET['id'];
    if ($bannerModel->deleteBanner($bannerId)) {
        header("Location: ../view/admin/listbanner.php");
    } else {
        echo "Error updating record: " . $conn->error;
        exit();
    }
?>