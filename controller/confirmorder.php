<?php 
    if (isset($_GET['id'])) {
        $result = array('success' => TRUE, 'message' => '');
        include('../model/order.php');
        include('../model/product.php');
        $orderModel = new OrderModel();
        $productModel = new ProductModel();

        $orderId = $_GET['id'];
        $order = $orderModel->getOrder($orderId);
        $items = $orderModel->getItemsInOrder($order['id']);
        if ($order['status'] != 'confirmed') {
            foreach($items as $item) {
                $product = $productModel->getProduct($item['productid']);            
                if ($product['inStock'] < $item['quantity']) exit();
            }
            $orderModel->updateOrder($orderId, 'confirmed');
            foreach($items as $item) {
                $product = $productModel->getProduct($item['productid']);            
                if ($product['inStock'] < $item['quantity']) exit();
                $productModel->updateAmountAfterSold($item['productid'], $item['quantity']);            
            }
        }
        header('location: ../view/admin/orderdetail.php?id='.$_GET['id']);
    }
?>