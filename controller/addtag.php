<?php
    session_start();
    include "../model/tag.php";


    $result = array('success'=> TRUE, 'message' => '');
    $tagModel = new TagModel();
    $conn = $tagModel->conn;
    if (!isset($_POST['tagname'])) exit();
    $tagName = $_POST['tagname'];
    if (strlen($tagName) > 0 ) {
        if(!$tagModel->addTag($tagName)) $result['success'] = FALSE;
    }
    else $result['success'] = FALSE;
    echo json_encode($result);
?>