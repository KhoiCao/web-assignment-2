<?php
  session_start();
  include "../model/article.php";

  $articleModel = new ArticleModel();
  $conn = $articleModel->conn;
  function validatesAsInt($number)
  {
    $number = filter_var($number, FILTER_VALIDATE_INT);
    return ($number !== FALSE);
  }
  if(isset($_POST['submit'])) {
    $title = mysqli_real_escape_string($conn, $_POST['title']);
    $filename = $_FILES['fileUpload']['name'];
    $filetype = $_FILES['fileUpload']['type'];
    $content = isset($_POST['content']) ? mysqli_real_escape_string($conn, $_POST['content']) : "";
    if (strlen($title) < 3 or strlen($title) > 100) {
      echo "Name's length must from 3 to 50"."<br>";
      exit();
    } 
    
    $target_dir = "../view/images/articles/";
    $target_file = $target_dir . basename($filename);
    $uploadOk = 1;
    // Check if image file is a actual image or fake image
    $check = getimagesize($_FILES['fileUpload']["tmp_name"]);
    if($check !== false) {
      echo "File is an image - " . $check["mime"] . ".";
      $uploadOk = 1;
    } else {
      echo "File is not an image.";
      $uploadOk = 0;
      exit();
    }
    $allowed = array("image/jpeg", "image/gif", "image/png");
    if(!in_array($filetype, $allowed)) {
      $error_message = 'Only jpg, gif, and png files are allowed.';
      echo $error_message;
      $uploadOk = 0;
      exit();
    }
    // Check file size
    if ($_FILES['fileUpload']["size"] > 500000) {
      echo "Sorry, your file is too large.";
      $uploadOk = 0;
      exit();
    }
    // Check if $uploadOk is set to 0 by an error
    if ($uploadOk == 0) {
      echo "Sorry, your file was not uploaded.";
      exit();
    // if everything is ok, try to upload file
    } else {
      if (move_uploaded_file($_FILES['fileUpload']["tmp_name"], $target_file)) {
        echo "The file ". htmlspecialchars( basename( $_FILES['fileUpload']["name"])). " has been uploaded.";
      } else {
        echo "Sorry, there was an error uploading your file.";
        exit();
      }
    }
    if($articleModel->addArticle($_SESSION['userId'], $title, $content, $target_file)) {
      header("Location: ../view/articles.php");
    } else {
      echo "Error updating record: " . $conn->error;
      exit();
    }
  } else {
    echo "Must submit form before rendering this.";
  }
?>