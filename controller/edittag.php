<?php
    include "../model/tag.php";

    $tagModel = new TagModel();
    $conn = $tagModel->conn;
    $result = array('success' => TRUE, 'message' => '');

    if(isset($_POST['id'])) {
        $tagId = $_POST['id'];
        $tagName = $_POST['tagname'];
        if(!$tagModel->updateTag($tagId, $tagName)) $result['success'] = FALSE;
    }
    else {
        $result['success'] = FALSE;
    }

    echo json_encode($result);
?>