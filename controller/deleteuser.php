<?php
    include "../model/user.php";

    $userModel = new UserModel();
    $conn = $userModel->conn;
    $result = array('success' => TRUE, 'message' => '');

    if(isset($_GET['id'])) {
        $userId = $_GET['id'];
        if(!$userModel->deleteUser($userId)) $result['success'] = FALSE;
    }
    else {
        $result['success'] = FALSE;
    }

    header('location: ../view/admin/listuser.php');
?>