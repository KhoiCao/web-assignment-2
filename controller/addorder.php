<?php
    session_start();
    include('../model/order.php');
    $orderModel = new OrderModel();
    $result = array('success' => TRUE, 'message' => '');

    if (!isset($_SESSION['userId'])) {
        $result['success'] = FALSE;
        $result['message'] = 'Vui lòng đăng nhập để sử dụng tính năng này';
    }
    else {
        if (!isset($_COOKIE[$_SESSION['userId']])) $cart_data = array();
        else {
            $userId = $_SESSION['userId'];
            $cookie_data = stripslashes($_COOKIE[$userId]);
            $cart_data = json_decode($cookie_data, true);
            if ($orderModel->addOrder($userId, 'pending')) {
                $order_id = $orderModel->conn->insert_id;
                foreach($cart_data as $item) {
                    if ( $item['quantity'] > 0 ) $orderModel->addProductToOrder($order_id, $item['productid'], $item['quantity']);
                }
            }
            setcookie($userId, "", time() - 3600, '/');
        }
    }
    echo json_encode($result);
    header('location: ../view/cart.php');
?>
 