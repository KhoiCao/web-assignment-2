<?php

include("../model/banner.php");
$bannerModel = new BannerModel();
$conn = $bannerModel->conn;
$bannerModel->conn->set_charset("utf8");

if(isset($_POST['add_banner'])) {
    $filename = $_FILES['fileUpload']['name'];
    $filetype = $_FILES['fileUpload']['type'];

    $target_dir = "../view/images/sliders/";
    $target_file = $target_dir . basename(time() . '-' . $filename);
    $uploadOk = 1;
    // Check if image file is a actual image or fake image
    $check = getimagesize($_FILES['fileUpload']["tmp_name"]);
    if($check !== false) {
        echo "File is an image - " . $check["mime"] . ".";
        $uploadOk = 1;
    } else {
        echo "File is not an image.";
        $uploadOk = 0;
        exit();
    }
    $allowed = array("image/jpeg", "image/gif", "image/png");
    if(!in_array($filetype, $allowed)) {
        $error_message = 'Only jpg, gif, and png files are allowed.';
        echo $error_message;
        $uploadOk = 0;
        exit();
    }
    // Check file size
    if ($_FILES['fileUpload']["size"] > 500000) {
        echo "Sorry, your file is too large.";
        $uploadOk = 0;
        exit();
    }
    if ($uploadOk == 0) {
        echo "Sorry, your file was not uploaded.";
        exit();
      // if everything is ok, try to upload file
    } else {
        if (move_uploaded_file($_FILES['fileUpload']["tmp_name"], $target_file)) {
            echo "The file ". htmlspecialchars( basename( $_FILES['fileUpload']["name"])). " has been uploaded.";
        } else {
            echo "Sorry, there was an error uploading your file.";
            exit();
        }
    }
    if($bannerModel->addBanner($target_file)) {
        header("Location: ../view/admin/listbanner.php");
      } else {
        echo "Error updating record: " . $conn->error;
        exit();
    }
} else {
    echo "Must submit form before rendering this.";
}

?>