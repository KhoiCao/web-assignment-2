<?php 
    include('../model/user.php');
    $result = array('success' => TRUE, 'message' => '');
    $userModel = new UserModel();
    if (!$userModel->switchActive($_GET['id'])) $result['success'] = FALSE;
    echo json_encode($result);
    header('location: ../view/admin/listuser.php?pageno='.$_GET['page']);
?>