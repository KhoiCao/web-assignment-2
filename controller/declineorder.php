<?php 
    if (isset($_GET['id'])) {
        include('../model/order.php');
        $orderModel = new OrderModel();
        $orderId = $_GET['id'];
        $orderModel->updateOrder($orderId, 'declined');
        header('location: ../view/admin/orderdetail.php?id='.$orderId);
    }
?>