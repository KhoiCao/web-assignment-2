<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Compiled and minified CSS -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">

    <!-- Compiled and minified JavaScript -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/css/all.min.css">
    <link rel="stylesheet" href="../view/css/style.css">
    <script src="https://code.jquery.com/jquery-3.5.1.min.js"
        integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
    <title>Chỉnh sửa banner</title>
</head>

<body>
    <div class="container">
        <div class="row">
            <h4 class="center" ><span style="color: #455a64;">Cập nhật banner</span></h4>
            <form enctype="multipart/form-data" method="POST" onsubmit="return checkForm();">
                <?php
                    include("../model/banner.php");
                    $bannerId = $_GET['id'];
                    $bannerModel = new BannerModel();
                    $conn = $bannerModel->conn;
                    $banner = $bannerModel->getBanner($bannerId)
                ?>
                <div class="file-field input-field">
                    <div class="btn">
                        <span>Tải banner lên</span>
                        <input id="fileImage" type="file" name='fileUpload' onchange="previewFile()" accept="image/*">
                    </div>
                    <div class="file-path-wrapper">
                        <input class="file-path validate" type="text">
                    </div>
                </div>
                <img class="col s8 offset-s2 responsive-img center-align" id="imageUpload"
                    src="<?php echo $banner['bannerurl']; ?>" />
                <span class="col s2"></span>
                <div class="col s12 center-align" style="margin-top: 15px;">
                    <input class="btn" type="submit" value="Cập nhật banner" name="update_banner">
                </div>
            </form>
            <?php 
                if(isset($_POST['update_banner'])) {
                    $target_file = $banner['bannerurl'];
                    if (is_uploaded_file($_FILES['fileUpload']['tmp_name'])) {
                        $filename = $_FILES['fileUpload']['name'];
                        $filetype = $_FILES['fileUpload']['type'];
                        $target_dir = "../view/images/sliders/";
                        $target_file = $target_dir . basename(time() . '-' . $filename);
                        $uploadOk = 1;
                        // Check if image file is a actual image or fake image
                        $check = getimagesize($_FILES['fileUpload']["tmp_name"]);
                        if($check !== false) {
                            echo "File is an image - " . $check["mime"] . ".";
                            $uploadOk = 1;
                        } else {
                            echo "File is not an image.";
                            $uploadOk = 0;
                            exit();
                        }
                        $allowed = array("image/jpeg", "image/gif", "image/png");
                        if(!in_array($filetype, $allowed)) {
                            $error_message = 'Only jpg, gif, and png files are allowed.';
                            echo $error_message;
                            $uploadOk = 0;
                            exit();
                        }
                        // Check file size
                        if ($_FILES['fileUpload']["size"] > 500000) {
                            echo "Sorry, your file is too large.";
                            $uploadOk = 0;
                            exit();
                        }
                        // Check if $uploadOk is set to 0 by an error
                        if ($uploadOk == 0) {
                            echo "Sorry, your file was not uploaded.";
                            exit();
                        // if everything is ok, try to upload file
                        } else {
                            if (move_uploaded_file($_FILES['fileUpload']["tmp_name"], $target_file)) {
                                echo "The file ". htmlspecialchars( basename( $_FILES['fileUpload']["name"])). " has been uploaded.";
                            } else {
                                echo "Sorry, there was an error uploading your file.";
                                exit();
                            }
                        }
                    }
                    if($bannerModel->updateBanner($bannerId, $target_file)) {
                        header("Location: ../view/admin/listbanner.php");
                      } else {
                        echo "Error updating record: " . $conn->error;
                        exit();
                    }
                }
            ?>
        </div>
    </div>
    <script>
    function previewFile() {
        const preview = document.querySelector("img#imageUpload");
        const file = document.querySelector("input[type=file]").files[0];
        const reader = new FileReader();

        reader.addEventListener(
            "load",
            function() {
                // convert image file to base64 string
                preview.src = reader.result;
                preview.alt = "image-product";
            },
            false
        );

        if (file) {
            reader.readAsDataURL(file);
        }
    }

    function getExtension(filename) {
        let parts = filename.split('.');
        return parts[parts.length - 1];
    }

    function isImage(filename) {
        let ext = getExtension(filename);
        switch (ext.toLowerCase()) {
            case 'jpg':
            case 'gif':
            case 'bmp':
            case 'png':
                //etc
                return true;
        }
        return false;
    }

    const checkForm = () => {
        let form = document.forms[0];
        let filePath = form.querySelector('input[id="fileImage"]').value;
        let partsFile = filePath.split('\\');
        let fileName = partsFile[partsFile.length - 1]

        if (fileName) {
            if (!isImage(fileName)) {
                alert("File phải là ảnh");
                return false;
            } else {
                alert("Cập nhật banner thành công");
                return true;
            }
        } else {
            alert("Cập nhật banner thành công");
            return true;
        }
    }
    </script>
</body>

</html>