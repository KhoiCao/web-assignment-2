<?php
    include('../model/user.php');
    include('helpers.php');

    $fname = $lname = $email = $password = "";
    $pattern = '/^([a-zA-ZÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚĂĐĨŨƠàáâãèéêìíòóôõùúăđĩũơƯĂẠẢẤẦẨẪẬẮẰẲẴẶẸẺẼỀỀỂưăạảấầẩẫậắằẳẵặẹẻẽềềểỄỆỈỊỌỎỐỒỔỖỘỚỜỞỠỢỤỦỨỪễệỉịọỏốồổỗộớờởỡợụủứừỬỮỰỲỴÝỶỸửữựỳỵỷỹ\s]+)$/i';
    // $result = array('fnameErr'=> FALSE, 'lnameErr'=> FALSE, 'emailErr'=> FALSE, 'passwordErr'=> FALSE);
    $result = array('success' => TRUE);
    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        $fname = test_input($_POST["fname"]);
        if (strlen($fname) < 2 || strlen($fname)> 20 || !preg_match($pattern, $fname)) $result['success'] = FALSE;

        $lname = test_input($_POST["lname"]);
        if (strlen($lname) < 2 || strlen($lname) > 20 || !preg_match($pattern, $lname)) $result['success'] = FALSE;

        $email = test_input($_POST["email"]);
        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            $result['success'] = FALSE;
        }

        $password = test_input($_POST["password"]);
        if (strlen($password) < 6) $result['success'] = FALSE;

        if ($result['success']) {
            $userModel = new UserModel();
            $added = $userModel->addUser($fname, $lname, 0, '', '', '', $email, $password, 0, 1, '');
            if(!$added) $result['success'] = FALSE;
        }
        echo json_encode($result);
    }
?>