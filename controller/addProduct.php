<?php
  include "../model/product.php";

  $product = new ProductModel();
  $conn = $product->conn;
  $product->conn->set_charset("utf8");
  function validatesAsInt($number)
  {
    $number = filter_var($number, FILTER_VALIDATE_INT);
    return ($number !== FALSE);
  }
  if(isset($_POST['submit'])) {
    $nameP = mysqli_real_escape_string($conn, $_POST['name']);
    $priceP = mysqli_real_escape_string($conn, $_POST['price']);
    $qtyP = mysqli_real_escape_string($conn, $_POST['qty']);
    $tags = $_POST['tag'];
    $typeP = $_POST['typeProduct'];
    $filename = $_FILES['fileUpload']['name'];
    $filetype = $_FILES['fileUpload']['type'];
    $description = isset($_POST['desc']) ? mysqli_real_escape_string($conn, $_POST['desc']) : "";
    $detail = isset($_POST['detail']) ? mysqli_real_escape_string($conn, $_POST['detail']) : "";
    $soldAmount = isset($_POST['soldout']) ? mysqli_real_escape_string($conn, $_POST['soldout']) : "";
    $rating = isset($_POST['rating']) ? mysqli_real_escape_string($conn, $_POST['rating']) : "";
    $brand = isset($_POST['brand']) ? mysqli_real_escape_string($conn, $_POST['brand']) : "";
    //check name 
    if (strlen($nameP) < 3 or strlen($nameP) > 50) {
      echo "Name's length must from 3 to 20"."<br>";
      exit();
    } 
    if ($priceP == 0) {
      echo "Price must greater than 0"."<br>";
      exit();
    }
    if (!validatesAsInt($priceP)) {
      echo "Price must be integer"."<br>";
      exit();
    }
    if ($qtyP == 0) {
      echo "Quantity must greater than 0"."<br>";
      exit();
    }
    if (!validatesAsInt($qtyP)) {
      echo "Quantity must be integer"."<br>";
      exit();
    }
    if (!isset($_POST['tag'])) {
      echo "You did not choose a tag"."<br>";
      exit();
    } 
    if (!isset($_POST['typeProduct'])) {
      echo "You did not choose a type"."<br>";
      exit();
    }
    $target_dir = "../view/images/products/";
    $target_file = $target_dir . basename(time() . '-' . $filename);
    $uploadOk = 1;
    // Check if image file is a actual image or fake image
    $check = getimagesize($_FILES['fileUpload']["tmp_name"]);
    if($check !== false) {
      echo "File is an image - " . $check["mime"] . ".";
      $uploadOk = 1;
    } else {
      echo "File is not an image.";
      $uploadOk = 0;
      exit();
    }
    $allowed = array("image/jpeg", "image/gif", "image/png");
    if(!in_array($filetype, $allowed)) {
      $error_message = 'Only jpg, gif, and png files are allowed.';
      echo $error_message;
      $uploadOk = 0;
      exit();
    }
    // Check file size
    if ($_FILES['fileUpload']["size"] > 500000) {
      echo "Sorry, your file is too large.";
      $uploadOk = 0;
      exit();
    }
    // Check if $uploadOk is set to 0 by an error
    if ($uploadOk == 0) {
      echo "Sorry, your file was not uploaded.";
      exit();
    // if everything is ok, try to upload file
    } else {
      if (move_uploaded_file($_FILES['fileUpload']["tmp_name"], $target_file)) {
        echo "The file ". htmlspecialchars( basename( $_FILES['fileUpload']["name"])). " has been uploaded.";
      } else {
        echo "Sorry, there was an error uploading your file.";
        exit();
      }
    }
    if($product->addProduct($typeP, $nameP, $priceP, $brand, $rating, $description, $detail, $qtyP, $soldAmount, $target_file)) {
      $last_id = $conn->insert_id;
      foreach($tags as $tag) {
        $product->addProductTag($last_id, $tag);
      }
      header("Location: ../view/admin/listproduct.php");
    } else {
      echo "Error updating record: " . $conn->error;
      exit();
    }
  } else {
    echo "Must submit form before rendering this.";
  }
?>