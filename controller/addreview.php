<?php
    session_start();
    include "../model/product.php";
    $result = array('success' => TRUE, 'message' => '');
    $productModel = new ProductModel();
    $conn = $productModel->conn;
    if (isset($_POST['userid'])) {
        $userId = $_POST['userid'];
        $content = $_POST['content'];
        $productId = $_POST['productid'];

        if(!($productModel->addProductReview($productId, $_SESSION['userId'], $content))) {
            $result['success'] = FALSE;
            $result['message'] = $conn->error;
        }
    }
    else $result['success'] = FALSE;
    echo json_encode($result);
?>