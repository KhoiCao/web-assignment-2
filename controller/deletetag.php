<?php
    include "../model/tag.php";

    $tagModel = new TagModel();
    $conn = $tagModel->conn;
    $result = array('success' => TRUE, 'message' => '');

    if(isset($_GET['id'])) {
        $tagId = $_GET['id'];
        if(!$tagModel->deleteTag($tagId)) $result['success'] = FALSE;
    }
    else {
        $result['success'] = FALSE;
    }

    header('location: ../view/admin/listtag.php');
?>