<?php
    include('../model/user.php');
    include('helpers.php');
    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        $email = $_POST['email'];
        $password = $_POST['password'];

        $email = test_input($email);
        $password = test_input($password);

        $userModel = new UserModel();
        $user = $userModel->authenticateUser($email, $password);
        if($user) {
            session_start();
            $_SESSION['userId'] = $user['id'];
            $_SESSION['firstname'] = $user['firstname'];
            $_SESSION['lastname'] = $user['lastname'];
            $_SESSION['email'] = $user['email'];
            $_SESSION['isAdmin'] = $user['isAdmin'];
            
            echo json_encode(array('success' => TRUE, 'isAdmin' => $user['isAdmin']));
        }
        else {
            echo json_encode(array('success' => FALSE));
        }
    }
?>