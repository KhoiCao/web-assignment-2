<?php 
    include('../model/article.php');
    include('../model/user.php');
    $articleModel = new ArticleModel();
    $userModel = new UserModel();

    $articleId = $_POST['articleid'];
    $from = $_POST['offset'];
    $limit = $_POST['page_size'];
    $comments = $articleModel->getArticleCommentsByRange($articleId, $from, $limit);

    if ($comments) {
        foreach($comments as $comment) { $user = $userModel->getUser($comment['userid']); ?>
            <div class="col s12 article-comment-box">
                <div class="user-avatar">
                    <img src=<?php if ($user['avatarurl'] != '') echo $user['avatarurl']; else echo "https://www.unionmedicalcentre.com.au/wp-content/uploads/2019/04/avatar-male.jpg";?> alt="Guitar 1" class="responsive-img circle">
                </div>
                <div class="comment-info">
                    <div class="user-name">
                        <?php echo $user['firstname']." ".$user['lastname']; ?>
                    </div>
                    <div class="comment-content">
                        <?php echo $comment['content']; ?>
                    </div>
                </div>
            </div>
<?php
        }
    }
?>