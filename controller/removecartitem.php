<?php 
    session_start();
    $userId = $_SESSION['userId'];
    $cookie_data = stripslashes($_COOKIE[$userId]);
    $cart_data = json_decode($cookie_data, true);

    foreach($cart_data as $keys => $values) {
        if($cart_data[$keys]['productid'] == $_GET["id"]) {
            unset($cart_data[$keys]);
            $encode_data = json_encode($cart_data);
            setcookie($_SESSION['userId'], $encode_data, time() + (86400 * 30), '/');
        }
    }

    header("location: ../view/cart.php");

?>