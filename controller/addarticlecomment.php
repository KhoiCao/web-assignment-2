<?php
    session_start();
    include "../model/article.php";
    $result = array('success' => TRUE, 'message' => '');
    $articleModel = new ArticleModel();
    $conn = $articleModel->conn;
    $content = $_POST['content'];
    $articleId = $_POST['articleid'];

    if(!($articleModel->addArticleComment($articleId, $_SESSION['userId'], $content))) {
        $result['success'] = FALSE;
        $result['message'] = $conn->error;
    }
    echo json_encode($result);
?>