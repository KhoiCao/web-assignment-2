<?php
    include "../model/product.php";

    $productModel = new ProductModel();
    $conn = $productModel->conn;
    $result = array('success' => TRUE, 'message' => '');

    if(isset($_GET['id'])) {
        $productId = $_GET['id'];
        if(!$productModel->deleteProduct($productId)) $result['success'] = FALSE;
    }
    else {
        $result['success'] = FALSE;
    }
    // echo json_encode($result);

    header('location: ../view/admin/listproduct.php?pageno='.$_GET['page']);
?>