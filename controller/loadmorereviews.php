<?php 
    include('../model/product.php');
    include('../model/user.php');
    $productModel = new ProductModel();
    $userModel = new UserModel();

    $productId = $_POST['productid'];
    $from = $_POST['offset'];
    $limit = $_POST['page_size'];
    $reviews = $productModel->getProductReviews($productId, $from, $limit);

    if ($reviews) {
        foreach($reviews as $review) { $reviewer = $userModel->getUser($review['userid']); ?>
            <div class="user-review">
                <div class="row">
                    <div class="user-box">
                        <div class="user-image">
                            <img src="<?php echo $reviewer['avatarurl']; ?>" alt="user.png"
                                class="circle responsive-img">
                        </div>
                        <div class="user-name center-align">
                            <span style="font-size: 15px;"><?php echo $reviewer['lastname']; ?></span>
                        </div>
                    </div>
                    <div class="review-box">
                        <div class="review-title center-align">
                            <span style="font-size: 15px;"><?php echo $review['content']; ?></span>
                        </div>
                        <div class="review-star center-align">
                            <span><i class="fas fa-star checked"></i></span>
                            <span><i class="fas fa-star checked"></i></span>
                            <span><i class="fas fa-star checked"></i></span>
                            <span><i class="fas fa-star checked"></i></span>
                            <span><i class="fas fa-star"></i></span>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="user-comment">
                        <h6 style="font-size: 15px;"><?php echo $review['createdat'];?></h6>
                    </div>
                </div>
            </div>
<?php
        }
    }
?>