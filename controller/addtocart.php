<?php
    session_start();
    $result = array('success' => TRUE, 'message' => '');

    if (!isset($_SESSION['userId'])) {
        $result['success'] = FALSE;
        $result['message'] = 'Vui lòng đăng nhập để sử dụng tính năng này';
    }
    else {
        if (!isset($_COOKIE[$_SESSION['userId']])) $cart_data = array();
        else {
            $userId = $_SESSION['userId'];
            $cookie_data = stripslashes($_COOKIE[$userId]);
            $cart_data = json_decode($cookie_data, true);
        }

        $item_id_list = array_column($cart_data, 'productid');
        
        if(in_array($_POST["productid"], $item_id_list)) {
            $result['success'] = FALSE;
            $result['message'] = 'Product already in cart';
        }
        else {
            $new_product = array(
                'productid'   => $_POST["productid"],
                'quantity'  => $_POST["quantity"]
            );
            $cart_data[] = $new_product;
        }

        $encode_data = json_encode($cart_data);
        setcookie($_SESSION['userId'], $encode_data, time() + (86400 * 30), '/');
    }
    header('location: ../view/cart.php');
    echo json_encode($result);
?>
 