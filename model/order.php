<?php
require_once("dbconnect.php");
class OrderModel extends DBConnect
{
    function __construct() {
        $this->connectDB();
    }

    function getNumberOfOrders() {
        $query = "SELECT COUNT(*) from orders";
        $result = $this->conn->query($query);
        return mysqli_fetch_array($result)[0];
    }

    function getAllOrders() {
        $query = "SELECT * from orders";
        $result = $this->conn->query($query);
        $orders = array();
        if($result->num_rows > 0) {
            while($order = mysqli_fetch_assoc($result)){
                $orders[] = $order;
            }
        }
        return $orders;
    }

    function getOrdersByRange($from, $limit) {
        $query = "SELECT * from orders ORDER BY id LIMIT $from, $limit";
        $result = $this->conn->query($query);
        $orders = array();
        if($result->num_rows > 0) {
            while($order = mysqli_fetch_assoc($result)){
                $orders[] = $order;
            }
        }
        return $orders;
    }

    function getOrder($orderId) {
        $query = "SELECT * from orders WHERE id = $orderId";
        $result = $this->conn->query($query);
        $order = mysqli_fetch_assoc($result);
        return $order;
    }

    function getItemsInOrder($orderId) {
        $query = "SELECT * from orderproduct WHERE orderid = $orderId";
        $result = $this->conn->query($query);
        $products = array();
        if($result->num_rows > 0) {
            while($product = mysqli_fetch_assoc($result)){
                $products[] = $product;
            }
        }
        return $products;
    }

    function addOrder($userId, $status) {
        $query = "INSERT into orders (userid, status) values ('$userId','$status')";
        return $this->conn->query($query);
    }

    function addProductToOrder($orderId, $productId, $quantity) {
        $query = "INSERT into orderproduct (orderid, productid, quantity) values ('$orderId','$productId', '$quantity')";
        return $this->conn->query($query);
    }

    function updateOrder($orderId, $status) {
        $query = " UPDATE orders SET status='{$status}' WHERE id = $orderId ";
        return $this->conn->query($query);
    }

    function deleteOrder($orderId) {
        $query = "DELETE from orders WHERE id = $orderId";
        return $this->conn->query($query);
    }

    function __destruct() {
        mysqli_close($this->conn);
    }
}
?>