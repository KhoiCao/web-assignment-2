<?php
require_once("dbconnect.php");
class ArticleModel extends DBConnect
{
    function __construct() {
        $this->connectDB();
    }

    function getNumberOfArticles() {
        $query = "SELECT COUNT(*) from article";
        $result = $this->conn->query($query);
        return mysqli_fetch_array($result)[0];
    }

    function getAllArticles() {
        $query = "SELECT * from article";
        $result = $this->conn->query($query);
        $articles = array();
        if($result->num_rows > 0) {
            while($article = mysqli_fetch_assoc($result)){
                $articles[] = $article;
            }
        }
        return $articles;
    }

    function getArticlesByRange($from, $limit) {
        $query = "SELECT * from article ORDER BY id LIMIT $from, $limit";
        $result = $this->conn->query($query);
        $articles = array();
        if($result->num_rows > 0) {
            while($article = mysqli_fetch_assoc($result)){
                $articles[] = $article;
            }
        }
        return $articles;
    }

    function getArticle($articleId) {
        $query = "SELECT * from article WHERE id = $articleId";
        $result = $this->conn->query($query);
        $article = mysqli_fetch_assoc($result);
        return $article;
    }

    function getArticleCommentsByRange($articleId, $from, $limit) {
        $query = "SELECT * from articlecomment WHERE articleid=$articleId ORDER BY createdat DESC LIMIT $from, $limit";
        $result = $this->conn->query($query);
        $comments = array();
        if($result->num_rows > 0) {
            while($comment = mysqli_fetch_assoc($result)){
                $comments[] = $comment;
            }
        }
        return $comments;
    }

    function getNumberOfArticleComments($articleId) {
        $query = "SELECT COUNT(*) from articlecomment WHERE articleid=$articleId";
        $result = $this->conn->query($query);
        return mysqli_fetch_array($result)[0];
    }

    function addArticle($userId, $title, $content, $thumbnailurl, $likes = 0, $dislikes = 0) {
        $query = "INSERT into article (userid, title, content, likes, dislikes, thumbnailurl) values ('$userId', '$title', '$content', '$likes', '$dislikes', '$thumbnailurl')";
        return $this->conn->query($query);
    }

    function addArticleComment($articleId, $userId, $content) {
        $query = "INSERT into articlecomment (articleid, userid, content) values ('$articleId', '$userId', '$content')";
        return $this->conn->query($query);
    }

    function updateArticle($articleId, $userId, $title, $content, $likes = 0, $dislikes = 0) {
        $query = " UPDATE article SET userid='{$userId}', title='{$title}', content='{$content}', likes='{$likes}', dislikes='{$dislikes}' WHERE id = $articleId";
        return $this->conn->query($query);
    }

    function incrementLike($articleId) {
        $query = " UPDATE article SET likes=likes+1 WHERE id = $articleId";
        return $this->conn->query($query);
    }

    function incrementDislike($articleId) {
        $query = " UPDATE article SET dislikes=dislikes+1 WHERE id = $articleId";
        return $this->conn->query($query);
    }

    function decrementLike($articleId) {
        $query = " UPDATE article SET likes=likes-1 WHERE id = $articleId AND likes > 0";
        return $this->conn->query($query);
    }

    function decrementDisLike($articleId) {
        $query = " UPDATE article SET dislikes=dislikes-1 WHERE id = $articleId AND dislikes > 0";
        return $this->conn->query($query);
    }

    function deleteArticle($articleId) {
        $query = "DELETE from article WHERE id = $articleId";
        return $this->conn->query($query);
    }

    function __destruct() {
        mysqli_close($this->conn);
    }
}
?>