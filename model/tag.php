<?php
require_once("dbconnect.php");
class TagModel extends DBConnect
{
    function __construct() {
        $this->connectDB();
    }

    function getNumberOfTags() {
        $query = "SELECT COUNT(*) from tag";
        $result = $this->conn->query($query);
        return mysqli_fetch_array($result)[0];
    }

    function getAllTags() {
        $query = "SELECT * from tag";
        $result = $this->conn->query($query);
        $tags = array();
        if($result->num_rows > 0) {
            while($tag = mysqli_fetch_assoc($result)){
                $tags[] = $tag;
            }
        }
        return $tags;
    }

    function getTagsByRange($from, $limit) {
        $query = "SELECT * from tag ORDER BY id LIMIT $from, $limit";
        $result = $this->conn->query($query);
        $tags = array();
        if($result->num_rows > 0) {
            while($tag = mysqli_fetch_assoc($result)){
                $tags[] = $tag;
            }
        }
        return $tags;
    }

    function getTag($tagId) {
        $query = "SELECT * from tag WHERE id = $tagId";
        $result = $this->conn->query($query);
        $tag = mysqli_fetch_assoc($result);
        return $tag;
    }

    function addTag($tagName) {
        $query = "INSERT into tag (tagname) values ('$tagName')";
        return $this->conn->query($query);
    }

    function updateTag($tagId, $tagName) {
        $query = " UPDATE tag SET tagname='{$tagName}' WHERE id = $tagId";
        return $this->conn->query($query);
    }

    function deleteTag($tagId) {
        $query = "DELETE from tag WHERE id = $tagId";
        return $this->conn->query($query);
    }

    function __destruct() {
        mysqli_close($this->conn);
    }
}
?>