<?php
require_once("dbconnect.php");
class ProductModel extends DBConnect
{
    function __construct() {
        $this->connectDB();
    }

    function getAllProducts() {
        $query = "SELECT * from product ORDER BY id DESC";
        $result = $this->conn->query($query);
        $products = array();
        if($result->num_rows > 0) {
            while($product = mysqli_fetch_assoc($result)){
                $products[] = $product;
            }
        }
        return $products;
    }

    function getProductsByRange ($from, $limit) {
        $query = "SELECT * from product ORDER BY id DESC LIMIT $from, $limit";
        $result = $this->conn->query($query);
        $products = array();
        if($result->num_rows > 0) {
            while($product = mysqli_fetch_assoc($result)){
                $products[] = $product;
            }
        }
        return $products;
    }

    function getProductsByType ($type, $from=0, $limit=0) {
        if ($limit == 0) $query = "SELECT * from product WHERE type='$type' ORDER BY id DESC";
        else $query = " SELECT * from product WHERE type='$type' ORDER BY id DESC LIMIT $from, $limit";
        $result = $this->conn->query($query);
        $products = array();
        if($result->num_rows > 0) {
            while($product = mysqli_fetch_assoc($result)){
                $products[] = $product;
            }
        }
        return $products;
    }

    function getProductsByName ($name) {
        $query = "SELECT * from product WHERE name LIKE '$name' ";
        $result = $this->conn->query($query);
        $products = array();
        if($result->num_rows > 0) {
            while($product = mysqli_fetch_assoc($result)){
                $products[] = $product;
            }
        }
        return $products;
    }

    function getProduct($productId) {
        $query = "SELECT * from product WHERE id = $productId";
        $result = $this->conn->query($query);
        $product = mysqli_fetch_assoc($result);
        return $product;
    }

    function addProduct($type, $name, $price, $brand, $rating, $description, $detail, $inStock, $soldAmount, $thumbnailUrl) {
        $query = "INSERT into product (type, name, price, brand, rating, description, detail, inStock, soldAmount, thumbnailurl) 
        values ('$type', '$name', '$price', '$brand', '$rating', '$description', '$detail', '$inStock', '$soldAmount', '$thumbnailUrl')";
        return $this->conn->query($query);
    }

    function updateProduct($productId, $type, $name, $price, $brand, $rating, $description, $detail, $inStock, $soldAmount, $thumbnailUrl) {
        $query = " UPDATE product SET type='{$type}', name='{$name}', price='{$price}', brand='{$brand}', rating='{$rating}', description='{$description}', detail ='{$detail}', inStock='{$inStock}', soldAmount='{$soldAmount}', thumbnailurl='{$thumbnailUrl}' WHERE id = $productId";
        return $this->conn->query($query);
    }

    function updateAmountAfterSold($productId, $amountSold) {
        $product = $this->getProduct($productId);
        $inStock = $product["inStock"] - $amountSold;
        if ($inStock < 0) return FALSE;
        $soldAmount = $product["soldAmount"] + $amountSold;
        $query = " UPDATE product SET inStock='{$inStock}', soldAmount='{$soldAmount}' WHERE id = $productId";
        return $this->conn->query($query);
    }

    function addProductTag($productId, $tagId) {
        $query = "INSERT into producttag (productid, tagid) values ('$productId', '$tagId')";
        return $this->conn->query($query);
    }

    function removeProductTag($productId, $tagId) {
        $query = "DELETE from producttag WHERE tagid = $tagId AND productid = $productId";
        return $this->conn->query($query);
    }

    function deleteProduct($productId) {
        $query = "DELETE from product WHERE id = $productId";
        return $this->conn->query($query);
    }

    function getProductReviews($productId, $from, $limit) {
        $query = "SELECT * from productreview WHERE productid = $productId ORDER BY createdat DESC LIMIT $from, $limit";
        $result = $this->conn->query($query);
        $reviews = array();
        if($result->num_rows > 0) {
            while($review = mysqli_fetch_assoc($result)){
                $reviews[] = $review;
            }
        }
        return $reviews;
    }

    function addProductReview($productId, $userId, $content) {
        $query = "INSERT into productreview (productid, userid, content) values ('$productId', '$userId', '$content')";
        return $this->conn->query($query);
    }

    function getProductTags($productId) {
        $query = "SELECT * from producttag, product WHERE productid = $productId AND product.id=producttag.productid";
        $result = $this->conn->query($query);
        $tags = array();
        if($result->num_rows > 0) {
            while($tag = mysqli_fetch_assoc($result)){
                $tags[] = $tag;
            }
        }
        return $tags;
    }

    function getBestSoldProducts() {
        $query = "SELECT * from product ORDER BY soldAmount DESC";
        $result = $this->conn->query($query);
        $products = array();
        if($result->num_rows > 0) {
            while($product = mysqli_fetch_assoc($result)){
                $products[] = $product;
            }
        }
        return $products;
    }

    function __destruct() {
        mysqli_close($this->conn);
    }
}
?>