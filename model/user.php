<?php
require_once("dbconnect.php");
class UserModel extends DBConnect
{
    function __construct() {
        $this->connectDB();
    }
    function getAllUsers() {
        $query = "SELECT * from user";
        $result = $this->conn->query($query);
        $users = array();
        if($result->num_rows > 0) {
            while($user = mysqli_fetch_assoc($result)){
                $users[] = $user;
            }
        }
        return $users;
    }
    function getUsersByRange($from, $limit) {
        $query = "SELECT * from user ORDER BY id LIMIT $from, $limit";
        $result = $this->conn->query($query);
        $users = array();
        if($result->num_rows > 0) {
            while($user = mysqli_fetch_assoc($result)){
                $users[] = $user;
            }
        }
        return $users;
    }

    function getUser($userId) {
        $query = "SELECT * from user WHERE id = $userId";
        $result = $this->conn->query($query);
        $user = mysqli_fetch_assoc($result);
        return $user;
    }

    function authenticateUser($email, $password) {
        $query = " SELECT * from user WHERE email = '$email' AND password='$password' ";
        $result = $this->conn->query($query);
        $user = mysqli_fetch_assoc($result);
        return $user;
    }

    function addUser($firstName, $lastName, $age, $address, $gender, $phone, $email, $password, $isAdmin, $active, $avatarUrl) {
        $query = "INSERT into user (firstname, lastname, age, address, gender, phone, email, password, isAdmin, active, avatarurl) 
        values ('$firstName', '$lastName', '$age', '$address', '$gender', '$phone', '$email', '$password', '$isAdmin', '$active', '$avatarUrl')";
        return $this->conn->query($query);
    }

    function updateUser($userId, $firstName, $lastName, $age, $address, $gender, $phone, $email, $password, $isAdmin, $active, $avatarUrl) {
        $query = " UPDATE user SET firstname='{$firstName}', lastname='{$lastName}', age='{$age}', address='{$address}', gender='{$gender}', phone='{$phone}', email ='{$email}', password='{$password}', isAdmin='{$isAdmin}', active='{$active}', avatarurl='{$avatarUrl}' WHERE id = $userId";
        return $this->conn->query($query);
    }

    function updateProfile($user, $userId){
        $query = " UPDATE user SET firstname='{$user["firstname"]}', lastname='{$user["lastname"]}', age='{$user["age"]}', address='{$user["address"]}', gender='{$user["gender"]}', phone='{$user["phone"]}', email ='{$user["email"]}', password='{$user["password"]}', isAdmin='{$user["isAdmin"]}', active='{$user["active"]}', avatarurl='{$user["avatarurl"]}' WHERE id = $userId";
        return $this->conn->query($query);
    }

    function deactivateUser($userId) {
        $query = " UPDATE user SET active=0 WHERE id = $userId";
        return $this->conn->query($query);
    }

    function activateUser($userId) {
        $query = " UPDATE user SET active=1 WHERE id = $userId";
        return $this->conn->query($query);
    }

    function switchActive($userId) {
        $query = " UPDATE user SET active=NOT active WHERE id = $userId";
        return $this->conn->query($query);
    }

    function deleteUser($userId) {
        $query = "DELETE from user WHERE id = $userId";
        return $this->conn->query($query);
    }

    function __destruct() {
        mysqli_close($this->conn);
    }
}
?>