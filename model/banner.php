<?php
require_once("dbconnect.php");
class BannerModel extends DBConnect
{
    function __construct() {
        $this->connectDB();
    }
    function getAllBanners() {
        $query = "SELECT * from banner";
        $result = $this->conn->query($query);
        $banners = array();
        if($result->num_rows > 0) {
            while($banner = mysqli_fetch_assoc($result)){
                $banners[] = $banner;
            }
        }
        return $banners;
    }

    function getBanner($bannerId) {
        $query = "SELECT * from banner WHERE id = $bannerId";
        $result = $this->conn->query($query);
        $banner = mysqli_fetch_assoc($result);
        return $banner;
    }

    function addBanner($bannerUrl) {
        $query = "INSERT into banner (bannerurl) values ('$bannerUrl')";
        return $this->conn->query($query);
    }

    function updateBanner($bannerId, $bannerUrl) {
        $query = " UPDATE banner SET bannerurl='{$bannerUrl}' WHERE id = $bannerId";
        return $this->conn->query($query);
    }

    function deleteBanner($bannerId) {
        $query = "DELETE from banner WHERE id = $bannerId";
        return $this->conn->query($query);
    }

    function __destruct() {
        mysqli_close($this->conn);
    }
}
?>