-- phpMyAdmin SQL Dump
-- version 5.0.3
-- https://www.phpmyadmin.net/
--
-- Máy chủ: 127.0.0.1
-- Thời gian đã tạo: Th12 27, 2020 lúc 11:19 AM
-- Phiên bản máy phục vụ: 10.4.14-MariaDB
-- Phiên bản PHP: 7.4.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Cơ sở dữ liệu: `db_web_assignment`
--

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `article`
--

CREATE TABLE `article` (
  `id` int(11) NOT NULL,
  `userid` int(11) NOT NULL,
  `title` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `content` text COLLATE utf8_unicode_ci NOT NULL,
  `createdAt` datetime NOT NULL DEFAULT current_timestamp(),
  `likes` int(11) NOT NULL DEFAULT 0,
  `dislikes` int(11) NOT NULL DEFAULT 0,
  `thumbnailurl` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `article`
--

INSERT INTO `article` (`id`, `userid`, `title`, `content`, `createdAt`, `likes`, `dislikes`, `thumbnailurl`) VALUES
(1, 4, 'Bài viết demo', '<p>Nội dung b&agrave;i viết demo</p>\r\n', '2020-12-17 22:32:25', 0, 0, '../view/images/articles/DB.png');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `articlecomment`
--

CREATE TABLE `articlecomment` (
  `id` int(11) NOT NULL,
  `articleid` int(11) NOT NULL,
  `userid` int(11) NOT NULL,
  `content` text COLLATE utf8_unicode_ci NOT NULL,
  `createdat` datetime NOT NULL DEFAULT current_timestamp(),
  `likes` int(11) NOT NULL,
  `dislikes` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `articlecomment`
--

INSERT INTO `articlecomment` (`id`, `articleid`, `userid`, `content`, `createdat`, `likes`, `dislikes`) VALUES
(1, 1, 4, 'Bài viết rất bổ ích', '2020-12-18 09:04:20', 0, 0),
(2, 1, 4, 'Cảm ơn tác giả', '2020-12-18 09:06:13', 0, 0),
(3, 1, 4, 'Hóng bài viết tiếp theo của tác giả', '2020-12-18 09:08:51', 0, 0),
(4, 1, 4, '...', '2020-12-18 09:09:56', 0, 0),
(5, 1, 4, 'Lại bình luận', '2020-12-18 09:11:43', 0, 0),
(6, 1, 2, 'Đồng ý với bạn', '2020-12-18 15:09:25', 0, 0);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `banner`
--

CREATE TABLE `banner` (
  `id` int(11) NOT NULL,
  `bannerurl` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `banner`
--

INSERT INTO `banner` (`id`, `bannerurl`) VALUES
(1, '../view/images/sliders/slider_1.jpg'),
(2, '../view/images/sliders/slider_2.jpg');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `orderproduct`
--

CREATE TABLE `orderproduct` (
  `orderid` int(11) NOT NULL,
  `productid` int(11) NOT NULL,
  `quantity` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `orderproduct`
--

INSERT INTO `orderproduct` (`orderid`, `productid`, `quantity`) VALUES
(2, 3, 3);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `orders`
--

CREATE TABLE `orders` (
  `id` int(11) NOT NULL,
  `userid` int(11) NOT NULL,
  `status` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `createdat` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `orders`
--

INSERT INTO `orders` (`id`, `userid`, `status`, `createdat`) VALUES
(1, 39, 'Đã xác nhận', '2020-12-14 10:27:36'),
(2, 8, 'confirmed', '2020-12-27 17:04:16');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `product`
--

CREATE TABLE `product` (
  `id` int(11) NOT NULL,
  `type` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `price` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `brand` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `rating` float NOT NULL,
  `thumbnailurl` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `detail` text COLLATE utf8_unicode_ci NOT NULL,
  `inStock` int(11) NOT NULL,
  `soldAmount` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `product`
--

INSERT INTO `product` (`id`, `type`, `name`, `price`, `brand`, `rating`, `thumbnailurl`, `description`, `detail`, `inStock`, `soldAmount`) VALUES
(1, 'Guitar', 'Đàn SILENT guitar Yamaha SLG-200S', '20000000', 'Yamaha', 3.5, '../view/images/products/dan-silent-guitar-slg110n.jpg', '<ul>\r\n	<li>Đ&agrave;n Guitar điện Yamaha Pacifica</li>\r\n	<li>Mặt ph&iacute;m đ&agrave;n bằng gỗ th&iacute;ch</li>\r\n	<li>Bộ phận cảm ứng &acirc;m thanh chữ V bằng nh&ocirc;m</li>\r\n	<li>Chức năng Dập Cu', '<h2>Đ&agrave;n Guitar điện Yamaha Pacifica&nbsp;</h2>\r\n\r\n<p>- Mặt ph&iacute;m đ&agrave;n bằng gỗ th&iacute;ch<br />\r\n- Bộ phận cảm ứng &acirc;m thanh chữ V bằng nh&ocirc;m<br />\r\n- &nbsp;Chức năng Dập Cuộn</p>\r\n\r\n<p>Model Pacifica112V mặt ph&iacute;m đ&agrave;n bằng gỗ th&iacute;ch mang lại sự th&acirc;m nhập v&agrave; đ&aacute;p &acirc;m tốt. C&ugrave;ng với c&aacute;c bộ phận cảm ứng &acirc;m thanh bằng nh&ocirc;m v&agrave; c&aacute;c chức năng cuộn cảm, loại đ&agrave;n n&agrave;y tạo cảm hứng cho tất cả những người chơi với &acirc;m thanh động cả tr&ecirc;n v&agrave; b&ecirc;n ngo&agrave;i s&acirc;n khấu.</p>\r\n\r\n<p><strong>TH&Ocirc;NG SỐ KỸ THUẬT :&nbsp;</strong></p>\r\n\r\n<table border=\"1\" cellpadding=\"1\" cellspacing=\"1\">\r\n	<tbody>\r\n		<tr>\r\n			<td>Cấu tr&uacute;c</td>\r\n			<td>Loại thường</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Độ d&agrave;i &acirc;m giai</td>\r\n			<td>647.7mm</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Mặt ph&iacute;m đ&agrave;n</td>\r\n			<td>Gỗ th&iacute;ch</td>\r\n		</tr>\r\n		<tr>\r\n			<td>B&aacute;n k&iacute;nh</td>\r\n			<td>350mm</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Ph&iacute;m đ&agrave;n</td>\r\n			<td>22</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Th&acirc;n</td>\r\n			<td>gỗ trăn</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Cần/cổ đ&agrave;n</td>\r\n			<td>Gỗ th&iacute;ch</td>\r\n		</tr>\r\n		<tr>\r\n			<td>M&aacute;y l&ecirc;n d&acirc;y</td>\r\n			<td>Đ&uacute;c khu&ocirc;n</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n', 10, 5),
(2, 'Guitar', 'Đàn Guitar điện Yamaha Pacifica 112VM', '16000000', 'Yamaha', 2, '../view/images/products/dan-guitar-dien-yamaha-pacifica-112vm-2.jpg', '<ul>\r\n	<li>Đ&agrave;n Guitar điện Yamaha Pacifica</li>\r\n	<li>Mặt ph&iacute;m đ&agrave;n bằng gỗ th&iacute;ch</li>\r\n	<li>Bộ phận cảm ứng &acirc;m thanh chữ V bằng nh&ocirc;m</li>\r\n	<li>Chức năng Dập Cu', '<h2>Đ&agrave;n Guitar điện Yamaha Pacifica&nbsp;</h2>\r\n\r\n<p>- Mặt ph&iacute;m đ&agrave;n bằng gỗ th&iacute;ch<br />\r\n- Bộ phận cảm ứng &acirc;m thanh chữ V bằng nh&ocirc;m<br />\r\n- &nbsp;Chức năng Dập Cuộn</p>\r\n\r\n<p>Model Pacifica112V mặt ph&iacute;m đ&agrave;n bằng gỗ th&iacute;ch mang lại sự th&acirc;m nhập v&agrave; đ&aacute;p &acirc;m tốt. C&ugrave;ng với c&aacute;c bộ phận cảm ứng &acirc;m thanh bằng nh&ocirc;m v&agrave; c&aacute;c chức năng cuộn cảm, loại đ&agrave;n n&agrave;y tạo cảm hứng cho tất cả những người chơi với &acirc;m thanh động cả tr&ecirc;n v&agrave; b&ecirc;n ngo&agrave;i s&acirc;n khấu.</p>\r\n\r\n<p><strong>TH&Ocirc;NG SỐ KỸ THUẬT :</strong></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<table border=\"1\" cellpadding=\"1\" cellspacing=\"1\">\r\n	<tbody>\r\n		<tr>\r\n			<td>Cấu tr&uacute;c</td>\r\n			<td>Loại thường</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Độ d&agrave;i &acirc;m giai</td>\r\n			<td>647.7mm</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Mặt ph&iacute;m đ&agrave;n</td>\r\n			<td>Gỗ th&iacute;ch</td>\r\n		</tr>\r\n		<tr>\r\n			<td>B&aacute;n k&iacute;nh</td>\r\n			<td>350mm</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Ph&iacute;m đ&agrave;n</td>\r\n			<td>22</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Th&acirc;n</td>\r\n			<td>gỗ trăn</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Cần/cổ đ&agrave;n</td>\r\n			<td>Gỗ th&iacute;ch</td>\r\n		</tr>\r\n		<tr>\r\n			<td>M&aacute;y l&ecirc;n d&acirc;y</td>\r\n			<td>Đ&uacute;c khu&ocirc;n</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n', 20, 10),
(3, 'Guitar', 'Đàn Guitar Classic Valote VC-303F', '1000000', 'Yamaha', 2, '../view/images/products/img8-1.png', '<ul>\r\n	<li>Đ&agrave;n Guitar điện Yamaha Pacifica</li>\r\n	<li>Mặt ph&iacute;m đ&agrave;n bằng gỗ th&iacute;ch</li>\r\n	<li>Bộ phận cảm ứng &acirc;m thanh chữ V bằng nh&ocirc;m</li>\r\n	<li>Chức năng Dập Cu', '<p><strong>Đ&agrave;n Acoustic guitar Yamaha F310 M&agrave;u gỗ tự nhi&ecirc;n</strong></p>\r\n\r\n<p>Guitar Acoustic l&agrave; loại guitar d&acirc;y sắt, ph&ugrave; hợp để chơi c&aacute;c thể loại nhạc trẻ, đệm h&aacute;t... D&ograve;ng guitar Acoustic d&agrave;nh cho thị trường Th&aacute;i Lan Mặt được l&agrave;m bằng gỗ .</p>\r\n\r\n<h3><strong>TH&Ocirc;NG SỐ KỸ THUẬT&nbsp;:&nbsp;</strong></h3>\r\n\r\n<p>D&ograve;ng đ&agrave;n Guitar Acoustic hệ FS/F c&oacute; chất lượng v&agrave; &acirc;m thanh với gi&aacute; b&igrave;nh d&acirc;n l&agrave; điểm nổi bật của guitar hệ F. Sự chia sẻ niềm đam m&ecirc; của những c&acirc;y đ&agrave;n guitar n&agrave;y đ&atilde; l&agrave; phần thưởng k&iacute;ch th&iacute;ch cho ch&uacute;ng t&ocirc;i tạo ra những sản phẩm tuyệt hảo nhất cho sinh vi&ecirc;n cũng như những người chơi đ&agrave;n thời vụ</p>\r\n\r\n<p>Mặt đ&agrave;n &nbsp; &nbsp;: &nbsp;Gỗ v&acirc;n sam</p>\r\n\r\n<p>Lưng v&agrave; H&ocirc;ng đ&agrave;n: &nbsp;Gỗ Meranti</p>\r\n\r\n<p>Đ&agrave;n &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; : &nbsp;Gỗ Nato</p>\r\n\r\n<p>B&agrave;n ph&iacute;m : &nbsp;Gỗ hồng sắc</p>\r\n\r\n<p>Ngựa &nbsp; &nbsp; &nbsp; &nbsp; : &nbsp;Gỗ hồng sắc</p>\r\n\r\n<p>Độ d&agrave;y th&ugrave;ng đ&agrave;n &nbsp;: &nbsp;96 - 116mm</p>\r\n\r\n<p>Thang &acirc;m d&acirc;y đ&agrave;n: &nbsp;634mm</p>\r\n\r\n<p>Kh&oacute;a l&ecirc;n d&acirc;y đ&agrave;n &nbsp;: &nbsp;Mạ Chrome&nbsp;</p>\r\n\r\n<p>Xuất xứ: Indonesia</p>\r\n\r\n<p>Tặng k&egrave;m bao đ&agrave;n + M&oacute;ng gẩy</p>\r\n', 27, 13),
(5, 'Keyboard', 'Amly guitar acoustic Stagg 20AA', '2500000', 'Yamaha', 2.5, '../view/images/products/1501558776639-2229434.jpg', '<ul>\r\n	<li>Thiết kế đẹp, kiểu d&aacute;ng nhỏ gọn,&nbsp;chất lượng tốt</li>\r\n	<li>Thuận tiện cho c&aacute;c buổi biểu diễn trong nh&agrave; v&agrave;&nbsp;ngo&agrave;i trời</li>\r\n	<li>Mức gi&aacute; h', '<h2>Th&ocirc;ng số sản phẩm</h2>\r\n\r\n<table>\r\n	<tbody>\r\n		<tr>\r\n			<td><strong>Chất liệu</strong></td>\r\n			<td>Nhựa, da, kim loại</td>\r\n		</tr>\r\n		<tr>\r\n			<td><strong>Model</strong></td>\r\n			<td>20AA</td>\r\n		</tr>\r\n		<tr>\r\n			<td><strong>K&iacute;ch thước</strong></td>\r\n			<td>45 x 40 x 15 (cm)</td>\r\n		</tr>\r\n		<tr>\r\n			<td><strong>Xuất xứ</strong></td>\r\n			<td>Trung Quốc</td>\r\n		</tr>\r\n		<tr>\r\n			<td><strong>M&agrave;u sắc</strong></td>\r\n			<td>Đen, n&acirc;u</td>\r\n		</tr>\r\n		<tr>\r\n			<td><strong>Trọng lượng</strong></td>\r\n			<td>6,5 (kg)</td>\r\n		</tr>\r\n		<tr>\r\n			<td><strong>C&ocirc;ng suất</strong></td>\r\n			<td>20 (W)</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n\r\n<h2>Th&ocirc;ng tin sản phẩm</h2>\r\n\r\n<h3>Amly guitar acoustic Stagg 20AA</h3>\r\n\r\n<p><strong>Amly guitar acoustic Stagg 20AA</strong><strong>&nbsp;</strong>l&agrave; sự lựa chọn tuyệt vời cho sinh vi&ecirc;n v&agrave; người bắt đầu chơi guitar. Với c&ocirc;ng suất 20W, bạn c&oacute; thể điểu chỉnh &acirc;m thanh to nhỏ với bass, middle, treble, volume v&agrave;&nbsp;gain, cũng c&oacute; thể d&ugrave;ng tai nghe để luyện tập rất dễ d&agrave;ng.</p>\r\n\r\n<p>Một bộ khuếch đại &acirc;m thanh l&agrave; kh&ocirc;ng thể thiếu nếu bạn d&ugrave;ng guitar điện. Đối với guitar acoustic (d&acirc;y th&eacute;p &amp; nylon) th&igrave; bạn n&ecirc;n hiểu ampli giống như một kỹ năng được cải thiện, &acirc;m thanh qua Amly nghe ấm v&agrave; vang hơn (điều n&agrave;y kh&ocirc;ng hẳn do bạn đ&aacute;nh tốt, tuy nhi&ecirc;n người nghe lại nghĩ kh&aacute;c). Chiếc Amly gi&uacute;p bạn giống như một nghệ sĩ chơi solo trong căn ph&ograve;ng nhỏ của m&igrave;nh.</p>\r\n\r\n<p><strong>Amly guitar acoustic Stagg 20AA&nbsp;</strong>c&oacute;&nbsp;thiết kế đẹp, chất lượng tốt, th&iacute;ch hợp cho c&aacute;c buổi biểu diễn. C&aacute;c sản phẩm của Stagg được c&aacute;c nghệ sỹ tin d&ugrave;ng như Peter Lockette, Mark Schulman, Paul Robinson, Peter Puke and Jamie Olive&nbsp;với gi&aacute; cả hấp dẫn nhất cho tất cả mọi người.</p>\r\n', 25, 0);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `productreview`
--

CREATE TABLE `productreview` (
  `id` int(11) NOT NULL,
  `userid` int(11) NOT NULL,
  `productid` int(11) NOT NULL,
  `content` text COLLATE utf8_unicode_ci NOT NULL,
  `rating` float NOT NULL DEFAULT 0,
  `createdat` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `producttag`
--

CREATE TABLE `producttag` (
  `productid` int(11) NOT NULL,
  `tagid` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `producttag`
--

INSERT INTO `producttag` (`productid`, `tagid`) VALUES
(1, 3),
(2, 3),
(3, 3),
(5, 8);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `tag`
--

CREATE TABLE `tag` (
  `id` int(11) NOT NULL,
  `tagname` varchar(10) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `tag`
--

INSERT INTO `tag` (`id`, `tagname`) VALUES
(1, 'Piano'),
(2, 'Organ'),
(3, 'Guitar'),
(4, 'Trống'),
(8, 'Keyboard');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `firstname` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lastname` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `age` int(11) DEFAULT NULL,
  `address` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `gender` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `avatarurl` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `isAdmin` tinyint(1) DEFAULT NULL,
  `active` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `user`
--

INSERT INTO `user` (`id`, `firstname`, `lastname`, `age`, `address`, `gender`, `phone`, `email`, `password`, `avatarurl`, `isAdmin`, `active`) VALUES
(1, 'cm', 'k', 20, '111hcm', 'male', '123456789', 'aaa@aa.com', 'admin', NULL, 1, 1),
(2, 'Vinh', 'NT', 20, 'abcxyz', 'male', '12345', 'vinh@hcmut.edu.vn', 'vinh', NULL, 1, 1),
(3, 'Luu', 'Le', 20, 'abcxyzdef', 'male', '123456', 'luu@hcmut.edu.vn', 'luu', NULL, 1, 1),
(4, 'Khoi', 'Cao', 20, 'a', 'male', '0327174199', 'khoi@hcmut.edu.vn', 'khoi', NULL, 1, 1),
(5, 'Tho', 'Trinh', 20, 'ajpsoajfpjf', 'male', '0932420', 'tho@hcmut.edu.vn', 'tho', NULL, 1, 1),
(6, 'JAOdi', 'PFJA', 20, 'afijpsaf', 'female', 'fq39', 'pojpi@gmail.com', 'oajfpas', NULL, 0, 1),
(8, 'test', 'user', 25, 'q1 HCM', 'male', '03919592', 'testuser@gmai.com', '12345', '', 0, 1),
(39, 'Họ', 'Tên', 0, '', '', '', 'hoten@gmail.com', 'hovaten', '', 0, 1),
(40, 'Ho', 'Ten', 0, '', '', '', 'tenho@yahoo.com.vn', 'tenvaho', '', 0, 1),
(41, 'Lee', 'Nguyen', 0, '', '', '', 'lenguyen@gmail.com', 'lenguyen', '', 0, 0),
(42, 'Nguyễn', 'Lê', 0, '', '', '', 'nguyenle@gmail.com', 'nguyenle', '', 0, 1),
(43, 'Lê9', 'Văn', 0, '', '', '', 'levan@gmail.com', 'leevan', '', 0, 1),
(44, 'Nguyễn Văn', 'Aa', 0, '', '', '', 'nguyenvana@gmail.com', 'nguyenvana', '', 0, 1);

--
-- Chỉ mục cho các bảng đã đổ
--

--
-- Chỉ mục cho bảng `article`
--
ALTER TABLE `article`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_user_article` (`userid`);

--
-- Chỉ mục cho bảng `articlecomment`
--
ALTER TABLE `articlecomment`
  ADD PRIMARY KEY (`id`),
  ADD KEY `articleid` (`articleid`,`userid`),
  ADD KEY `fk_user_articlecomment` (`userid`);

--
-- Chỉ mục cho bảng `banner`
--
ALTER TABLE `banner`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `orderproduct`
--
ALTER TABLE `orderproduct`
  ADD PRIMARY KEY (`orderid`,`productid`),
  ADD KEY `orderid` (`orderid`,`productid`),
  ADD KEY `fk_product_orderproduct` (`productid`);

--
-- Chỉ mục cho bảng `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`),
  ADD KEY `fk_user_order` (`userid`);

--
-- Chỉ mục cho bảng `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`);

--
-- Chỉ mục cho bảng `productreview`
--
ALTER TABLE `productreview`
  ADD PRIMARY KEY (`id`),
  ADD KEY `userid` (`userid`,`productid`),
  ADD KEY `fk_product_productreview` (`productid`);

--
-- Chỉ mục cho bảng `producttag`
--
ALTER TABLE `producttag`
  ADD PRIMARY KEY (`productid`,`tagid`),
  ADD KEY `productid` (`productid`,`tagid`),
  ADD KEY `fk_tag_producttag` (`tagid`);

--
-- Chỉ mục cho bảng `tag`
--
ALTER TABLE `tag`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`);

--
-- Chỉ mục cho bảng `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`);

--
-- AUTO_INCREMENT cho các bảng đã đổ
--

--
-- AUTO_INCREMENT cho bảng `article`
--
ALTER TABLE `article`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT cho bảng `articlecomment`
--
ALTER TABLE `articlecomment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT cho bảng `banner`
--
ALTER TABLE `banner`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT cho bảng `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT cho bảng `product`
--
ALTER TABLE `product`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT cho bảng `productreview`
--
ALTER TABLE `productreview`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT cho bảng `tag`
--
ALTER TABLE `tag`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT cho bảng `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=45;

--
-- Các ràng buộc cho các bảng đã đổ
--

--
-- Các ràng buộc cho bảng `article`
--
ALTER TABLE `article`
  ADD CONSTRAINT `fk_user_article` FOREIGN KEY (`userid`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Các ràng buộc cho bảng `articlecomment`
--
ALTER TABLE `articlecomment`
  ADD CONSTRAINT `fk_article_articlecomment` FOREIGN KEY (`articleid`) REFERENCES `article` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_user_articlecomment` FOREIGN KEY (`userid`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Các ràng buộc cho bảng `orderproduct`
--
ALTER TABLE `orderproduct`
  ADD CONSTRAINT `fk_order_orderproduct` FOREIGN KEY (`orderid`) REFERENCES `orders` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_product_orderproduct` FOREIGN KEY (`productid`) REFERENCES `product` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Các ràng buộc cho bảng `orders`
--
ALTER TABLE `orders`
  ADD CONSTRAINT `fk_user_order` FOREIGN KEY (`userid`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Các ràng buộc cho bảng `productreview`
--
ALTER TABLE `productreview`
  ADD CONSTRAINT `fk_product_productreview` FOREIGN KEY (`productid`) REFERENCES `product` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_user_productreview` FOREIGN KEY (`userid`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Các ràng buộc cho bảng `producttag`
--
ALTER TABLE `producttag`
  ADD CONSTRAINT `fk_product_producttag` FOREIGN KEY (`productid`) REFERENCES `product` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_tag_producttag` FOREIGN KEY (`tagid`) REFERENCES `tag` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
