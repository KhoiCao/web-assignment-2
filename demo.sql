-- phpMyAdmin SQL Dump
-- version 5.0.3
-- https://www.phpmyadmin.net/
--
-- Máy chủ: 127.0.0.1
-- Thời gian đã tạo: Th12 30, 2020 lúc 04:50 AM
-- Phiên bản máy phục vụ: 10.4.14-MariaDB
-- Phiên bản PHP: 7.4.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Cơ sở dữ liệu: `demo`
--

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `article`
--

CREATE TABLE `article` (
  `id` int(11) NOT NULL,
  `userid` int(11) NOT NULL,
  `title` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `content` text COLLATE utf8_unicode_ci NOT NULL,
  `createdAt` datetime NOT NULL DEFAULT current_timestamp(),
  `likes` int(11) NOT NULL DEFAULT 0,
  `dislikes` int(11) NOT NULL DEFAULT 0,
  `thumbnailurl` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `article`
--

INSERT INTO `article` (`id`, `userid`, `title`, `content`, `createdAt`, `likes`, `dislikes`, `thumbnailurl`) VALUES
(1, 4, 'Bài viết demo', '<p>Nội dung b&agrave;i viết demo</p>\r\n', '2020-12-17 22:32:25', 0, 0, '../view/images/articles/DB.png'),
(2, 41, 'Tìm hiểu đàn ukulele', '<h2>Đ&agrave;n ukulele c&oacute; một đặc điểm thế mạnh l&agrave; nhỏ gọn, dễ d&agrave;ng mang theo, thuận tiện cho những người th&iacute;ch giao du, hay đi du lịch, hay phải đi lại. N&oacute; cũng rất dễ học, ph&ugrave; hợp với nhiều người muốn chơi được một loại nhạc cụ đơn giản n&agrave;o đ&oacute;.</h2>\r\n\r\n<h3>1.&nbsp;Xuất xứ đ&agrave;n Ukulele</h3>\r\n\r\n<p><strong>Đ&agrave;n Ukulele</strong>&nbsp;l&agrave; nhạc cụ xuất ph&aacute;t từ quần đảo Hawaii. Ở Anh, người ta đọc l&agrave; Ukelele, v&agrave; được viết tắt l&agrave; Uke.</p>\r\n\r\n<h3>2.&nbsp;Chi tiết đ&agrave;n ukulele</h3>\r\n\r\n<p><strong>Đ&agrave;n ukulele</strong>&nbsp;thuộc họ guitar, v&agrave; thường c&oacute; 4 d&acirc;y</p>\r\n\r\n<p>Thường người ta d&ugrave;ng&nbsp;<strong>đ&agrave;n Ukulele</strong>&nbsp;để đệm h&aacute;t, đ&ocirc;i khi cũng độc tấu. Tiếng giọng v&agrave; &acirc;m lượng của&nbsp;<strong>đ&agrave;n Ukulele</strong>&nbsp;thay đổi theo k&iacute;ch thước v&agrave; cấu tạo của n&oacute;. Tuy tiếng vang nhỏ, nhưng đ&agrave;n Ukulele khi đệm nghe cũng r&eacute;o rắt do &acirc;m sắc kh&aacute; đặc biệt.</p>\r\n\r\n<p>Thường&nbsp;<strong>đ&agrave;n Ukulele</strong>&nbsp;c&oacute; 4 cỡ: Soprano, Concert, Tenor, v&agrave; Baritone cộng th&ecirc;m một số loại đặc biệt.</p>\r\n\r\n<p>- Phổ biến nhất l&agrave; Soprano Ukulele chuẩn, l&agrave; loại c&oacute; k&iacute;ch thước nhỏ nhất, d&agrave;i khoảng 53 cm (21&Prime;), thường c&oacute; 12 đến 14 ph&iacute;m, v&agrave; được l&ecirc;n d&acirc;y A-E-C-G (La-Mi-Do-Sol) (thứ tự d&acirc;y số 1-2-3-4).</p>\r\n\r\n<p>- To hơn một ch&uacute;t l&agrave; Concert Ukulele, d&agrave;i khoảng 58 cm (23&Prime;). N&oacute; cũng được l&ecirc;n d&acirc;y A-E-C-G v&agrave; thường c&oacute; 14 đến 17 ph&iacute;m, nhưng v&igrave; th&acirc;n đ&agrave;n to hơn, n&ecirc;n &acirc;m thanh ấm hơn so với Soprano Ukulele. Ngo&agrave;i ra, cần đ&agrave;n cũng rộng bản hơn để ph&ugrave; hợp với b&agrave;n tay to.</p>\r\n\r\n<p>- Kiểu thứ ba l&agrave; Tenor Ukulele, d&agrave;i khoảng 66 cm (26&Prime;), gồm 17 đến 19 ph&iacute;m, v&agrave; được chọn th&iacute;ch hợp để độc tấu. N&oacute; cũng được l&ecirc;n d&acirc;y A-E-C-G.</p>\r\n\r\n<p>- Kiểu thứ tư l&agrave; Baritone Ukulele, d&agrave;i khoảng 76 cm (30&Prime;). N&oacute; được l&ecirc;n d&acirc;y E-B-G-D (Mi-Si-Sol-Re), nghĩa l&agrave; giống 4 d&acirc;y 1, 2, 3, 4 của Guitar, v&agrave; thường c&oacute; từ 19 đến 21 ph&iacute;m. Kiểu Ukulele n&agrave;y c&oacute; &acirc;m thanh ấm v&agrave; đầy đặn nhất trong 4 kiểu.</p>\r\n\r\n<p>- Loại Ukulele đặc biệt c&oacute; t&ecirc;n l&agrave; U-Bass. N&oacute; được l&ecirc;n d&acirc;y đ&uacute;ng theo c&aacute;c loại đ&agrave;n bass truyền thống : Sol-Re-La-Mi (G-D-A-E) (d&acirc;y số 1-2-3-4). C&aacute;c d&acirc;y đ&agrave;n l&agrave;m bằng vật liệu cao su dầy, cho &acirc;m thanh trầm, ấm rất ri&ecirc;ng cho U-Bass. N&oacute; c&oacute; loại kh&ocirc;ng ph&iacute;m v&agrave; c&oacute; ph&iacute;m.</p>\r\n\r\n<p>- Dĩ nhi&ecirc;n c&ograve;n c&oacute; nhiều biến thể của Ukulele như Ukulele cộng hưởng (Resonator Ukulele), Ukulele điện (Electric Ukulele), v&agrave; Banjo Ukulele l&agrave; th&ocirc;ng dụng nhất.</p>\r\n\r\n<p>- Xem th&ecirc;m&nbsp;Kh&oacute;a học đ&agrave;n Ukulele cấp tốc&nbsp;tại Trường nhạc Việt Thương Music School</p>\r\n\r\n<h3>3.&nbsp;Tổng kết</h3>\r\n\r\n<p><strong>Ukulele</strong>&nbsp;nhỏ nhắn xinh xinh v&agrave; &iacute;t d&acirc;y như vậy n&ecirc;n rất th&iacute;ch hợp cho c&aacute;c b&eacute; lứa tuổi nhi đồng. Tr&ecirc;n YouTube ta c&oacute; thể t&igrave;m cả chục b&eacute; trổ t&agrave;i chơi Ukulele rất dễ thương. Dĩ nhi&ecirc;n, người lớn ghiền&nbsp;<strong>Ukulele</strong>&nbsp;cũng kh&ocirc;ng &iacute;t. V&agrave; đương nhi&ecirc;n cũng c&oacute; những nghệ sĩ biểu diễn Ukulele rất giỏi.</p>\r\n', '2020-12-29 22:50:35', 0, 0, '../view/images/articles/tim-hieu-dan-ukulele-1.jpg'),
(3, 42, 'Những nhạc cụ nào dễ chơi nhất hiện nay ?', '<h2>Những nhạc cụ n&agrave;o dễ chơi nhất hiện nay. Bạn c&oacute; thể lựa chọn c&aacute;ch chơi đ&agrave;n guitar, piano hay học thổi s&aacute;o, thổi harmonica..cho ph&ugrave; hợp.</h2>\r\n\r\n<p>&Acirc;m nhạc lu&ocirc;n l&agrave; một phần trong cuộc sống con người, trong cuộc sống nhiều căng thẳng ng&agrave;y nay th&igrave; nhiều người th&iacute;ch chơi một loiạ nhạc cụ n&agrave;o đ&oacute; để thư giản. Vậy&nbsp;<strong>những nhạc cụ n&agrave;o dễ chơi nhất hiện nay</strong>&nbsp;? T&ugrave;y v&agrave;o sở th&iacute;ch v&agrave; điều kiện m&agrave; mỗi người c&oacute; c&aacute;ch lựa chọn cho ri&ecirc;ng m&igrave;nh.</p>\r\n\r\n<h2>Những nhạc cụ n&agrave;o dễ chơi nhất ng&agrave;y nay</h2>\r\n\r\n<h3>Chơi đ&agrave;n piano</h3>\r\n\r\n<p>Piano l&agrave; một loại nhạc cụ được nhiều người y&ecirc;u th&iacute;ch v&agrave; theo đuổi học v&igrave; n&oacute; mang vẻ sang trọng v&agrave; qu&yacute; ph&aacute;i cho người chơi. Tiếng đ&agrave;n của piano vừa du dương vừa trầm bổng thể hiện được với c&aacute;c b&agrave;i nhạc. Lướt ng&oacute;n tay tr&ecirc;n ph&iacute;m đ&agrave;n tạo n&ecirc;n c&aacute;c &acirc;m thanh thật trong trẻo lu&ocirc;n tạo ra nhiều x&uacute;c cảm thật tuyệt vời. Tr&ecirc;n c&aacute;c phim ảnh nước ngo&agrave;i h&igrave;nh ảnh ch&agrave;ng trai đ&agrave;n piano lu&ocirc;n l&agrave;m cho c&aacute;c kh&aacute;n giả nữ thần tượng hết mức.</p>\r\n\r\n<p>Học piano cũng dễ, kh&ocirc;ng qu&aacute; kh&oacute;, chỉ cần ki&ecirc;n tr&igrave; một ch&uacute;t v&agrave; th&acirc;m gia c&aacute;c kh&oacute;a học l&agrave; c&oacute; thể đ&agrave;n được. Tuy nhi&ecirc;n gi&aacute; th&agrave;nh để mua đ&agrave;n piano kh&aacute; cao kh&ocirc;ng ph&ugrave; hợp với nhiều người n&ecirc;n cũng kh&oacute; m&agrave; sở hữu một chiếc piano mới. Với lại piano kh&aacute; to v&agrave; cồng kềnh với lại cần bảo dưỡng tốt. V&igrave; vậy học piano rất tuyệt vời nhưng cần c&oacute; điều kiện.</p>\r\n\r\n<h3>Chơi đ&agrave;n guitar</h3>\r\n\r\n<p>Đ&acirc;y l&agrave; một loại nhạc cụ kh&aacute; phổ biến d&agrave;nh cho mọi người nhất l&agrave; sinh vi&ecirc;n. Học piano bạn cần c&oacute; sự ki&ecirc;n tr&igrave; nhất định v&agrave; c&oacute; sự chỉ dẫn để thực h&agrave;nh cũng như c&aacute;c hiểu biết về &acirc;m nhạc. &Acirc;m sắc của đ&agrave;n guitar kh&aacute; đa dạng n&ecirc;n bạn c&oacute; thể diễn tả đầy dủ c&aacute;c &acirc;m thanh. Nhượt điểm l&agrave; &acirc;m thanh đ&agrave;n guitar kh&ocirc;ng trong như của đ&agrave;n piano v&agrave; kh&ocirc;ng ph&ugrave; hợp chơi ở c&aacute;c nơi sang trọng, lễ đặc biệt.</p>\r\n\r\n<h3>Học c&aacute;ch thổi s&aacute;o</h3>\r\n\r\n<p>Thổi s&aacute;o l&agrave; mộ c&aacute;ch theo &acirc;m nhạc cổ truyền của nước ta. &Acirc;m sắc của s&aacute;o trầm bổng v&agrave; du dương thể hiện tốt c&aacute;c b&agrave;i h&aacute;t buồn man m&aacute;c. C&acirc;y s&aacute;o tiện dụng, nhỏ gọn v&agrave; &iacute;t chi ph&iacute; n&ecirc;n sẽ l&agrave; bạn đồng h&agrave;nh tốt c&oacute; thể mang đi khắp nơi. Đ&acirc;y l&agrave; nhạc cụ được nhiều thiếu ni&ecirc;n ưa th&iacute;ch, tuy nhi&ecirc;n người tập phải c&oacute; một luồn hơi khỏe để tập luyện.</p>\r\n\r\n<h3><strong>Học thổi harmonica</strong></h3>\r\n\r\n<p>Thổi hamonica được nhiều người y&ecirc;u th&iacute;ch, bởi sự tiện lợi v&agrave; nhỏ gọn của n&oacute;. &Acirc;m sắc của n&oacute; cao r&eacute;o rắt, nếu muốn học n&oacute; bạn phải c&oacute; một hơi thở tốt để thực h&agrave;nh v&agrave; chơi nhạc.</p>\r\n\r\n<p>L&agrave; người y&ecirc;u &acirc;m nhạc th&igrave; bạn n&ecirc;n học c&aacute;ch chơi một loại nhạc cụ n&agrave;o đ&oacute;. Kh&ocirc;ng c&oacute;&nbsp;<strong>những nhạc cụ n&agrave;o dễ chơi nhất hiện nay</strong>.&nbsp;D&ugrave; l&agrave; hoc đ&agrave;n guitar, piano hay học thổi s&aacute;o, thổi harmonica th&igrave; chỉ cần bạn đam m&ecirc; v&agrave; c&oacute; quyết t&acirc;m bạn sẽ chơi th&agrave;nh thạo loại nhạc cụ đ&oacute; v&agrave; tạo n&ecirc;n những bản nhạc tuyệt hay.</p>\r\n', '2020-12-29 22:57:43', 0, 0, '../view/images/articles/nhac-cu-nao-de-hoc-nhat-jpg.jpg'),
(4, 43, 'Học mẹ Nhật cách rèn con từ thuở lọt lòng', '<p>C&aacute;ch nu&ocirc;i dạy con của mẹ Nhật kh&ocirc;ng g&igrave; hơn l&agrave; k&iacute;ch th&iacute;ch sự ph&aacute;t triển cả 5 gi&aacute;c quan ở nơi trẻ.&nbsp;</p>\r\n\r\n<p>Nhật Bản nổi tiếng l&agrave; một quốc gia đ&agrave;o tạo v&agrave; gi&aacute;o dục con người th&ocirc;ng qua c&aacute;c truyền thống v&agrave; kỷ luật. Đ&oacute; l&agrave; l&yacute; do tại sao nu&ocirc;i dạy trẻ theo c&aacute;ch của người Nhật trở th&agrave;nh một trong những đề t&agrave;i được phụ huynh quan t&acirc;m đặc biệt.&nbsp;</p>\r\n\r\n<p>C&aacute;ch nu&ocirc;i dạy con của mẹ Nhật kh&ocirc;ng g&igrave; hơn l&agrave; k&iacute;ch th&iacute;ch sự ph&aacute;t triển cả 5 gi&aacute;c quan ở nơi trẻ. B&agrave;i viết sau đ&acirc;y sẽ chia sẻ c&aacute;c bước cụ thể m&agrave; mẹ Nhật đ&atilde; r&egrave;n c&aacute;c con m&igrave;nh từ thuở lọt l&ograve;ng:</p>\r\n\r\n<p><strong>Ph&aacute;t triển thị gi&aacute;c cho trẻ từ 0-3 th&aacute;ng</strong></p>\r\n\r\n<p>Từ 0-3 th&aacute;ng tuổi l&agrave; giai đoạn trẻ c&oacute; khả năng tiếp nhận lượng lớn th&ocirc;ng tin lớn. Do đ&oacute;, c&aacute;c bố mẹ Nhật thường tập trung ph&aacute;t triển tr&iacute; th&ocirc;ng minh cho con m&igrave;nh trong giai đoạn n&agrave;y.</p>\r\n\r\n<p>Xung quanh giường của trẻ sơ sinh, c&aacute;c b&agrave; Nhật thường treo c&aacute;c ảnh phong cảnh nổi tiếng tr&ecirc;n thế giới v&igrave; trẻ sơ sinh nh&igrave;n nhận thế giới qua c&aacute;c h&igrave;nh ảnh v&agrave; m&agrave;u sắc phong ph&uacute;. Tr&ecirc;n kệ đồ, họ trưng b&agrave;y c&aacute;c đồ chơi với m&agrave;u sắc tươi s&aacute;ng hoặc c&aacute;c khối gỗ được điểm t&ocirc; chi tiết.</p>\r\n\r\n<p>Nếu trẻ dưới một th&aacute;ng tuổi, mẹ Nhật thường cho trẻ nh&igrave;n ngắm c&aacute;c đường sọc m&agrave;u đen v&agrave; trắng mỗi 3 ph&uacute;t/ng&agrave;y v&agrave; li&ecirc;n tục trong một tuần. Bằng c&aacute;ch n&agrave;y, khả năng tập trung của trẻ từ chưa đầy 5 gi&acirc;y sẽ tăng l&ecirc;n 60-90 gi&acirc;y. Nhờ khả năng tập trung cao độ, c&aacute;c b&eacute; sẽ c&oacute; đủ tiềm năng để học tập c&aacute;c m&ocirc;n học sau n&agrave;y.</p>\r\n\r\n<p>Nếu trẻ dưới 9 th&aacute;ng tuổi, hệ thần kinh chưa ph&aacute;t triển đầy đủ, trẻ sẽ kh&ocirc;ng thể ph&acirc;n biệt giữa c&aacute;c m&agrave;u đỏ, xanh l&aacute; c&acirc;y v&agrave; m&agrave;u v&agrave;ng</p>\r\n\r\n<p>Nếu trẻ dưới 9 th&aacute;ng tuổi, hệ thần kinh chưa ph&aacute;t triển đầy đủ, trẻ sẽ kh&ocirc;ng thể ph&acirc;n biệt giữa c&aacute;c m&agrave;u đỏ, xanh l&aacute; c&acirc;y v&agrave; m&agrave;u v&agrave;ng. Mẹ sẽ thấy khi được 6 th&aacute;ng tuổi, trẻ tỏ ra ch&aacute;n c&aacute;c đồ chơi sọc ngang, sọc dọc. L&uacute;c n&agrave;y, c&aacute;c mẹ h&atilde;y cố gắng chơi với con bằng c&aacute;ch di chuyển c&aacute;c đồ vật c&oacute; sọc nhỏ hơn. Nếu con vẫn kh&ocirc;ng quan t&acirc;m, n&ecirc;n tạm thời g&aacute;c lại c&aacute;c m&oacute;n đồ chơi sọc ngang, dọc.</p>\r\n\r\n<p>Gần giường của b&eacute;, n&ecirc;n treo bảng chữ cỡ lớn với c&aacute;c m&agrave;u đỏ để b&eacute; c&oacute; thể nh&igrave;n r&otilde; r&agrave;ng. Những b&agrave; mẹ trẻ thậm ch&iacute; c&ograve;n cho trẻ l&agrave;m quen với chữ c&aacute;i từ trong bụng mẹ để khi ra đời, b&eacute; sẽ được hưởng nhiều lợi &iacute;ch từ việc n&agrave;y. H&atilde;y &ocirc;m bảng chữ c&aacute;i s&aacute;t v&agrave;o bụng bầu 1 lần/ng&agrave;y v&agrave; k&eacute;o d&agrave;i từ 2-3 gi&acirc;y, sau đ&oacute; lặp lại. Sau nhiều lần thực h&agrave;nh, bạn sẽ thấy em b&eacute; trong bụng khua tay, m&uacute;a ch&acirc;n mỗi khi bạn &aacute;p bảng chữ gần bụng bầu.</p>\r\n\r\n<p><strong>Ph&aacute;t triển th&iacute;nh gi&aacute;c cho trẻ từ 0-3 th&aacute;ng</strong></p>\r\n\r\n<p>Nếu &aacute;p dụng nu&ocirc;i dạy trẻ theo c&aacute;ch của người Nhật, mẹ chắc chắn kh&ocirc;ng thể bỏ qua &acirc;m nhạc. Đ&acirc;y l&agrave; một yếu tố quan trọng m&agrave; c&aacute;c b&agrave; mẹ Nhật dạy trẻ th&ocirc;ng minh ngay trong giai đoạn 0-3 th&aacute;ng. Mỗi ng&agrave;y, bạn n&ecirc;n chọn c&aacute;c thời điểm nhất định để nghe nhạc khoảng 15 ph&uacute;t mỗi lần v&agrave; khoảng 30 ph&uacute;t mỗi ng&agrave;y.</p>\r\n\r\n<p>Đừng để trẻ nghe nhạc với &acirc;m lượng qu&aacute; lớn. Cần lưu &yacute; rằng nếu nghe đĩa CD trong một thời gian d&agrave;i, trẻ sẽ h&igrave;nh th&agrave;nh th&oacute;i quen xấu l&agrave; ph&aacute;t &acirc;m giống như bản ghi &acirc;m hoặc giọng n&oacute;i kh&ocirc;ng c&oacute; biểu cảm như khi được nghe trực tiếp từ mẹ m&igrave;nh.</p>\r\n\r\n<p>Khi cho b&eacute; nghe nhạc, tay của mẹ ẵm n&aacute;ch b&eacute; v&agrave; đặt đầu b&eacute; l&ecirc;n đầu gối của mẹ, nhẹ nh&agrave;ng lắc lư theo điệu nhạc. Sau đ&oacute;, trẻ sẽ trở n&ecirc;n hiền l&agrave;nh v&agrave; say sưa theo điệu nhạc vang. Mẹ v&agrave; b&eacute; c&oacute; thể vừa nghe nhạc vừa m&uacute;a ba l&ecirc;. Điều quan trọng l&agrave; phải n&oacute;i chuyện với b&eacute; thật nhiều sau khi sinh. Khi cho b&uacute;, thay t&atilde; hay tắm cho b&eacute;, mẹ h&atilde;y khẽ kh&agrave;ng n&oacute;i chuyện với b&eacute;. Trong l&uacute;c thay t&atilde;, mẹ h&atilde;y nắm tay, ch&acirc;n của b&eacute; v&agrave; n&oacute;i: &quot;Đ&acirc;y l&agrave; b&agrave;n tay, b&agrave;n tay, b&agrave;n tay,&hellip;&quot;. Cứ thế, mẹ lặp đi lặp lại d&ugrave; b&eacute; kh&ocirc;ng c&oacute; phản ứng g&igrave; đặc biệt. Hoặc c&oacute; thể cầm theo quả b&oacute;ng nhỏ hoặc con b&uacute;p b&ecirc; trong l&uacute;c thay t&atilde; v&agrave; n&oacute;i: &quot;Đ&acirc;y l&agrave; một quả b&oacute;ng, quả b&oacute;ng, quả b&oacute;ng&quot;, &quot;Đ&acirc;y l&agrave; con b&uacute;p b&ecirc;, b&uacute;p b&ecirc;, b&uacute;p b&ecirc;&quot;. Đ&acirc;y l&agrave; một phần rất quan trọng gi&uacute;p ph&aacute;t triển th&iacute;nh gi&aacute;c của b&eacute;.</p>\r\n\r\n<p><strong>Ph&aacute;t triển x&uacute;c gi&aacute;c cho trẻ từ 0-3 th&aacute;ng tuổi</strong></p>\r\n\r\n<p>Những b&agrave;i học nu&ocirc;i dạy trẻ theo c&aacute;ch của người Nhật kh&ocirc;ng bao giờ bỏ qua việc cho con b&uacute; trong giai đoạn đầu đời. Kể từ khi sinh ra, trẻ đ&atilde; bắt đầu học rất nhiều thứ v&agrave; tất cả sẽ trở th&agrave;nh k&yacute; ức, nguy&ecirc;n liệu để trẻ h&igrave;nh th&agrave;nh tư duy r&otilde; r&agrave;ng sau n&agrave;y.</p>\r\n\r\n<p>Cho con b&uacute; l&agrave; b&agrave;i học ph&aacute;t triển x&uacute;c gi&aacute;c đầu ti&ecirc;n của trẻ. H&atilde;y quan s&aacute;t kỹ một đứa trẻ b&uacute; mẹ v&agrave; động t&aacute;c t&igrave;m v&uacute;, giữ n&uacute;m v&uacute; v&agrave; nuốt sữa của trẻ. C&aacute;c thao t&aacute;c n&agrave;y sẽ diễn ra rất nhanh. Ban đầu, trẻ thường chạm cằm hoặc mũi của m&igrave;nh để t&igrave;m n&uacute;m v&uacute; v&agrave; rất kh&oacute; khăn khi đưa n&uacute;m v&uacute; mẹ v&agrave;o miệng của m&igrave;nh một c&aacute;ch ch&iacute;nh x&aacute;c. Nhiều b&agrave; mẹ c&oacute; thể hỗ trợ bằng c&aacute;ch d&ugrave;ng tay đưa n&uacute;m v&uacute; v&agrave;o miệng nhưng dần d&agrave;, c&aacute;c b&eacute; c&oacute; thể tự điều chỉnh.</p>\r\n\r\n<p>C&aacute;c mẹ n&ecirc;n chạm n&uacute;m v&uacute; của m&igrave;nh đến c&aacute;c vị tr&iacute; kh&aacute;c nhau tr&ecirc;n khu&ocirc;n mặt của b&eacute; như m&ocirc;i, miệng, h&agrave;m tr&ecirc;n, h&agrave;m dưới, cằm, m&aacute; phải v&agrave; m&aacute; tr&aacute;i. Việc l&agrave;m n&agrave;y gi&uacute;p b&eacute; nhanh ch&oacute;ng học được c&aacute;ch điều chỉnh kh&ocirc;ng gian v&agrave; cảm nhận vị tr&iacute; tr&ecirc;n - dưới, phải - tr&aacute;i.</p>\r\n\r\n<p>Kh&ocirc;ng chỉ d&ugrave;ng n&uacute;m v&uacute;, mẹ cũng c&oacute; thể sử dụng ng&oacute;n tay, nhẹ nh&agrave;ng ch&agrave; x&aacute;t vải x&ocirc; l&ecirc;n h&agrave;m tr&ecirc;n, h&agrave;m dưới của b&eacute;. C&aacute;c b&eacute; sẽ c&oacute; cảm gi&aacute;c v&agrave; bắt đầu liếm hoặc cắn.</p>\r\n\r\n<p>H&atilde;y để b&eacute; cầm giữ ng&oacute;n tay của mẹ. Trẻ sơ sinh nắm lấy tất cả mọi thứ ngay lập tức như một phản xạ bản năng nhưng phản xạ n&agrave;y sẽ nhanh ch&oacute;ng biến mất. Để phản xạ n&agrave;y kh&ocirc;ng bị mất đi, h&atilde;y gi&uacute;p b&eacute; thực h&agrave;nh n&oacute; thường xuy&ecirc;n. C&oacute; thể d&ugrave;ng một m&oacute;n đồ chơi nhỏ, an to&agrave;n cho b&eacute; nắm giữ nhưng h&atilde;y lu&ocirc;n quan s&aacute;t để tr&aacute;nh b&eacute; tự l&agrave;m tổn thương m&igrave;nh.</p>\r\n\r\n<p><strong>Ph&aacute;t triển vị gi&aacute;c cho trẻ em từ 0-3 th&aacute;ng</strong></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>H&atilde;y thử nh&uacute;ng khăn x&ocirc; v&agrave;o một chậu nước cam, nước chanh v&agrave; nh&uacute;ng l&ecirc;n đầu lưỡi b&eacute;. Đ&acirc;y l&agrave; một c&aacute;ch tuyệt vời để k&iacute;ch hoạt vị gi&aacute;c cho trẻ.</p>\r\n\r\n<p><strong>Ph&aacute;t triển khướu gi&aacute;c cho trẻ em từ 0-3 th&aacute;ng</strong></p>\r\n\r\n<p>H&atilde;y để cho b&eacute; ngửi nhiều m&ugrave;i hương của nhiều loại hoa kh&aacute;c nhau. Như thế, b&eacute; sẽ quen với những m&ugrave;i n&agrave;y. C&agrave;ng tiếp x&uacute;c với nhiều loại hương hoa v&agrave; nhiều m&ugrave;i hương kh&aacute;c nhau, trẻ sẽ c&agrave;ng c&oacute; cơ hội để ph&aacute;t triển khướu gi&aacute;c.</p>\r\n\r\n<p>Tr&ecirc;n đ&acirc;y l&agrave; phương ph&aacute;p nu&ocirc;i dạy trẻ theo c&aacute;ch của người Nhật được &aacute;p dụng với 5 gi&aacute;c quan tương ứng. Mong rằng, th&ocirc;ng tin n&agrave;y sẽ gi&uacute;p &iacute;ch nhiều cho c&aacute;c mẹ khi t&igrave;m kiếm c&aacute;c phương ph&aacute;p gi&aacute;o dục tối ưu.</p>\r\n', '2020-12-29 22:59:51', 0, 0, '../view/images/articles/hoc-me-nhat-cach-ren-con-tu-thuo-lot-long.jpg');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `articlecomment`
--

CREATE TABLE `articlecomment` (
  `id` int(11) NOT NULL,
  `articleid` int(11) NOT NULL,
  `userid` int(11) NOT NULL,
  `content` text COLLATE utf8_unicode_ci NOT NULL,
  `createdat` datetime NOT NULL DEFAULT current_timestamp(),
  `likes` int(11) NOT NULL,
  `dislikes` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `articlecomment`
--

INSERT INTO `articlecomment` (`id`, `articleid`, `userid`, `content`, `createdat`, `likes`, `dislikes`) VALUES
(1, 1, 4, 'Bài viết rất bổ ích', '2020-12-18 09:04:20', 0, 0),
(2, 1, 4, 'Cảm ơn tác giả', '2020-12-18 09:06:13', 0, 0),
(3, 1, 4, 'Hóng bài viết tiếp theo của tác giả', '2020-12-18 09:08:51', 0, 0),
(4, 1, 4, '...', '2020-12-18 09:09:56', 0, 0),
(5, 1, 4, 'Lại bình luận', '2020-12-18 09:11:43', 0, 0),
(6, 1, 2, 'Đồng ý với bạn', '2020-12-18 15:09:25', 0, 0),
(7, 3, 43, 'Cảm ơn tác giả, bài viết rất hay', '2020-12-29 23:00:40', 0, 0),
(8, 3, 41, 'Bài viết rất bổ ích', '2020-12-29 23:01:25', 0, 0),
(9, 3, 41, 'Chờ bài viết tiếp theo của bạn', '2020-12-29 23:04:22', 0, 0),
(10, 1, 41, 'Tuyệt vời', '2020-12-30 10:28:36', 0, 0);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `banner`
--

CREATE TABLE `banner` (
  `id` int(11) NOT NULL,
  `bannerurl` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `banner`
--

INSERT INTO `banner` (`id`, `bannerurl`) VALUES
(3, '../view/images/sliders/slider_1.jpg'),
(4, '../view/images/sliders/slider_2.jpg'),
(5, '../view/images/sliders/1609296231-slider_1.jpg');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `orderproduct`
--

CREATE TABLE `orderproduct` (
  `orderid` int(11) NOT NULL,
  `productid` int(11) NOT NULL,
  `quantity` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `orderproduct`
--

INSERT INTO `orderproduct` (`orderid`, `productid`, `quantity`) VALUES
(2, 3, 3);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `orders`
--

CREATE TABLE `orders` (
  `id` int(11) NOT NULL,
  `userid` int(11) NOT NULL,
  `status` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `createdat` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `orders`
--

INSERT INTO `orders` (`id`, `userid`, `status`, `createdat`) VALUES
(1, 39, 'Đã xác nhận', '2020-12-14 10:27:36'),
(2, 8, 'confirmed', '2020-12-27 17:04:16');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `product`
--

CREATE TABLE `product` (
  `id` int(11) NOT NULL,
  `type` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `price` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `brand` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `rating` float NOT NULL,
  `thumbnailurl` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `detail` text COLLATE utf8_unicode_ci NOT NULL,
  `inStock` int(11) NOT NULL,
  `soldAmount` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `product`
--

INSERT INTO `product` (`id`, `type`, `name`, `price`, `brand`, `rating`, `thumbnailurl`, `description`, `detail`, `inStock`, `soldAmount`) VALUES
(1, 'Guitar', 'Đàn SILENT guitar Yamaha SLG-200S', '20000000', 'Yamaha', 3.5, '../view/images/products/dan-silent-guitar-slg110n.jpg', '<ul>\r\n	<li>Đ&agrave;n Guitar điện Yamaha Pacifica</li>\r\n	<li>Mặt ph&iacute;m đ&agrave;n bằng gỗ th&iacute;ch</li>\r\n	<li>Bộ phận cảm ứng &acirc;m thanh chữ V bằng nh&ocirc;m</li>\r\n	<li>Chức năng Dập Cu', '<h2>Đ&agrave;n Guitar điện Yamaha Pacifica&nbsp;</h2>\r\n\r\n<p>- Mặt ph&iacute;m đ&agrave;n bằng gỗ th&iacute;ch<br />\r\n- Bộ phận cảm ứng &acirc;m thanh chữ V bằng nh&ocirc;m<br />\r\n- &nbsp;Chức năng Dập Cuộn</p>\r\n\r\n<p>Model Pacifica112V mặt ph&iacute;m đ&agrave;n bằng gỗ th&iacute;ch mang lại sự th&acirc;m nhập v&agrave; đ&aacute;p &acirc;m tốt. C&ugrave;ng với c&aacute;c bộ phận cảm ứng &acirc;m thanh bằng nh&ocirc;m v&agrave; c&aacute;c chức năng cuộn cảm, loại đ&agrave;n n&agrave;y tạo cảm hứng cho tất cả những người chơi với &acirc;m thanh động cả tr&ecirc;n v&agrave; b&ecirc;n ngo&agrave;i s&acirc;n khấu.</p>\r\n\r\n<p><strong>TH&Ocirc;NG SỐ KỸ THUẬT :&nbsp;</strong></p>\r\n\r\n<table border=\"1\" cellpadding=\"1\" cellspacing=\"1\">\r\n	<tbody>\r\n		<tr>\r\n			<td>Cấu tr&uacute;c</td>\r\n			<td>Loại thường</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Độ d&agrave;i &acirc;m giai</td>\r\n			<td>647.7mm</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Mặt ph&iacute;m đ&agrave;n</td>\r\n			<td>Gỗ th&iacute;ch</td>\r\n		</tr>\r\n		<tr>\r\n			<td>B&aacute;n k&iacute;nh</td>\r\n			<td>350mm</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Ph&iacute;m đ&agrave;n</td>\r\n			<td>22</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Th&acirc;n</td>\r\n			<td>gỗ trăn</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Cần/cổ đ&agrave;n</td>\r\n			<td>Gỗ th&iacute;ch</td>\r\n		</tr>\r\n		<tr>\r\n			<td>M&aacute;y l&ecirc;n d&acirc;y</td>\r\n			<td>Đ&uacute;c khu&ocirc;n</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n', 10, 5),
(2, 'Guitar', 'Đàn Guitar điện Yamaha Pacifica 112VM', '16000000', 'Yamaha', 2, '../view/images/products/dan-guitar-dien-yamaha-pacifica-112vm-2.jpg', '<ul>\r\n	<li>Đ&agrave;n Guitar điện Yamaha Pacifica</li>\r\n	<li>Mặt ph&iacute;m đ&agrave;n bằng gỗ th&iacute;ch</li>\r\n	<li>Bộ phận cảm ứng &acirc;m thanh chữ V bằng nh&ocirc;m</li>\r\n	<li>Chức năng Dập Cu', '<h2>Đ&agrave;n Guitar điện Yamaha Pacifica&nbsp;</h2>\r\n\r\n<p>- Mặt ph&iacute;m đ&agrave;n bằng gỗ th&iacute;ch<br />\r\n- Bộ phận cảm ứng &acirc;m thanh chữ V bằng nh&ocirc;m<br />\r\n- &nbsp;Chức năng Dập Cuộn</p>\r\n\r\n<p>Model Pacifica112V mặt ph&iacute;m đ&agrave;n bằng gỗ th&iacute;ch mang lại sự th&acirc;m nhập v&agrave; đ&aacute;p &acirc;m tốt. C&ugrave;ng với c&aacute;c bộ phận cảm ứng &acirc;m thanh bằng nh&ocirc;m v&agrave; c&aacute;c chức năng cuộn cảm, loại đ&agrave;n n&agrave;y tạo cảm hứng cho tất cả những người chơi với &acirc;m thanh động cả tr&ecirc;n v&agrave; b&ecirc;n ngo&agrave;i s&acirc;n khấu.</p>\r\n\r\n<p><strong>TH&Ocirc;NG SỐ KỸ THUẬT :</strong></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<table border=\"1\" cellpadding=\"1\" cellspacing=\"1\">\r\n	<tbody>\r\n		<tr>\r\n			<td>Cấu tr&uacute;c</td>\r\n			<td>Loại thường</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Độ d&agrave;i &acirc;m giai</td>\r\n			<td>647.7mm</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Mặt ph&iacute;m đ&agrave;n</td>\r\n			<td>Gỗ th&iacute;ch</td>\r\n		</tr>\r\n		<tr>\r\n			<td>B&aacute;n k&iacute;nh</td>\r\n			<td>350mm</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Ph&iacute;m đ&agrave;n</td>\r\n			<td>22</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Th&acirc;n</td>\r\n			<td>gỗ trăn</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Cần/cổ đ&agrave;n</td>\r\n			<td>Gỗ th&iacute;ch</td>\r\n		</tr>\r\n		<tr>\r\n			<td>M&aacute;y l&ecirc;n d&acirc;y</td>\r\n			<td>Đ&uacute;c khu&ocirc;n</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n', 20, 10),
(3, 'Guitar', 'Đàn Guitar Classic Valote VC-303F', '1000000', 'Yamaha', 2, '../view/images/products/img8-1.png', '<ul>\r\n	<li>Đ&agrave;n Guitar điện Yamaha Pacifica</li>\r\n	<li>Mặt ph&iacute;m đ&agrave;n bằng gỗ th&iacute;ch</li>\r\n	<li>Bộ phận cảm ứng &acirc;m thanh chữ V bằng nh&ocirc;m</li>\r\n	<li>Chức năng Dập Cu', '<p><strong>Đ&agrave;n Acoustic guitar Yamaha F310 M&agrave;u gỗ tự nhi&ecirc;n</strong></p>\r\n\r\n<p>Guitar Acoustic l&agrave; loại guitar d&acirc;y sắt, ph&ugrave; hợp để chơi c&aacute;c thể loại nhạc trẻ, đệm h&aacute;t... D&ograve;ng guitar Acoustic d&agrave;nh cho thị trường Th&aacute;i Lan Mặt được l&agrave;m bằng gỗ .</p>\r\n\r\n<h3><strong>TH&Ocirc;NG SỐ KỸ THUẬT&nbsp;:&nbsp;</strong></h3>\r\n\r\n<p>D&ograve;ng đ&agrave;n Guitar Acoustic hệ FS/F c&oacute; chất lượng v&agrave; &acirc;m thanh với gi&aacute; b&igrave;nh d&acirc;n l&agrave; điểm nổi bật của guitar hệ F. Sự chia sẻ niềm đam m&ecirc; của những c&acirc;y đ&agrave;n guitar n&agrave;y đ&atilde; l&agrave; phần thưởng k&iacute;ch th&iacute;ch cho ch&uacute;ng t&ocirc;i tạo ra những sản phẩm tuyệt hảo nhất cho sinh vi&ecirc;n cũng như những người chơi đ&agrave;n thời vụ</p>\r\n\r\n<p>Mặt đ&agrave;n &nbsp; &nbsp;: &nbsp;Gỗ v&acirc;n sam</p>\r\n\r\n<p>Lưng v&agrave; H&ocirc;ng đ&agrave;n: &nbsp;Gỗ Meranti</p>\r\n\r\n<p>Đ&agrave;n &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; : &nbsp;Gỗ Nato</p>\r\n\r\n<p>B&agrave;n ph&iacute;m : &nbsp;Gỗ hồng sắc</p>\r\n\r\n<p>Ngựa &nbsp; &nbsp; &nbsp; &nbsp; : &nbsp;Gỗ hồng sắc</p>\r\n\r\n<p>Độ d&agrave;y th&ugrave;ng đ&agrave;n &nbsp;: &nbsp;96 - 116mm</p>\r\n\r\n<p>Thang &acirc;m d&acirc;y đ&agrave;n: &nbsp;634mm</p>\r\n\r\n<p>Kh&oacute;a l&ecirc;n d&acirc;y đ&agrave;n &nbsp;: &nbsp;Mạ Chrome&nbsp;</p>\r\n\r\n<p>Xuất xứ: Indonesia</p>\r\n\r\n<p>Tặng k&egrave;m bao đ&agrave;n + M&oacute;ng gẩy</p>\r\n', 27, 13),
(5, 'Keyboard', 'Amly guitar acoustic Stagg 20AA', '2500000', 'Yamaha', 2.5, '../view/images/products/1501558776639-2229434.jpg', '<ul>\r\n	<li>Thiết kế đẹp, kiểu d&aacute;ng nhỏ gọn,&nbsp;chất lượng tốt</li>\r\n	<li>Thuận tiện cho c&aacute;c buổi biểu diễn trong nh&agrave; v&agrave;&nbsp;ngo&agrave;i trời</li>\r\n	<li>Mức gi&aacute; h', '<h2>Th&ocirc;ng số sản phẩm</h2>\r\n\r\n<table>\r\n	<tbody>\r\n		<tr>\r\n			<td><strong>Chất liệu</strong></td>\r\n			<td>Nhựa, da, kim loại</td>\r\n		</tr>\r\n		<tr>\r\n			<td><strong>Model</strong></td>\r\n			<td>20AA</td>\r\n		</tr>\r\n		<tr>\r\n			<td><strong>K&iacute;ch thước</strong></td>\r\n			<td>45 x 40 x 15 (cm)</td>\r\n		</tr>\r\n		<tr>\r\n			<td><strong>Xuất xứ</strong></td>\r\n			<td>Trung Quốc</td>\r\n		</tr>\r\n		<tr>\r\n			<td><strong>M&agrave;u sắc</strong></td>\r\n			<td>Đen, n&acirc;u</td>\r\n		</tr>\r\n		<tr>\r\n			<td><strong>Trọng lượng</strong></td>\r\n			<td>6,5 (kg)</td>\r\n		</tr>\r\n		<tr>\r\n			<td><strong>C&ocirc;ng suất</strong></td>\r\n			<td>20 (W)</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n\r\n<h2>Th&ocirc;ng tin sản phẩm</h2>\r\n\r\n<h3>Amly guitar acoustic Stagg 20AA</h3>\r\n\r\n<p><strong>Amly guitar acoustic Stagg 20AA</strong><strong>&nbsp;</strong>l&agrave; sự lựa chọn tuyệt vời cho sinh vi&ecirc;n v&agrave; người bắt đầu chơi guitar. Với c&ocirc;ng suất 20W, bạn c&oacute; thể điểu chỉnh &acirc;m thanh to nhỏ với bass, middle, treble, volume v&agrave;&nbsp;gain, cũng c&oacute; thể d&ugrave;ng tai nghe để luyện tập rất dễ d&agrave;ng.</p>\r\n\r\n<p>Một bộ khuếch đại &acirc;m thanh l&agrave; kh&ocirc;ng thể thiếu nếu bạn d&ugrave;ng guitar điện. Đối với guitar acoustic (d&acirc;y th&eacute;p &amp; nylon) th&igrave; bạn n&ecirc;n hiểu ampli giống như một kỹ năng được cải thiện, &acirc;m thanh qua Amly nghe ấm v&agrave; vang hơn (điều n&agrave;y kh&ocirc;ng hẳn do bạn đ&aacute;nh tốt, tuy nhi&ecirc;n người nghe lại nghĩ kh&aacute;c). Chiếc Amly gi&uacute;p bạn giống như một nghệ sĩ chơi solo trong căn ph&ograve;ng nhỏ của m&igrave;nh.</p>\r\n\r\n<p><strong>Amly guitar acoustic Stagg 20AA&nbsp;</strong>c&oacute;&nbsp;thiết kế đẹp, chất lượng tốt, th&iacute;ch hợp cho c&aacute;c buổi biểu diễn. C&aacute;c sản phẩm của Stagg được c&aacute;c nghệ sỹ tin d&ugrave;ng như Peter Lockette, Mark Schulman, Paul Robinson, Peter Puke and Jamie Olive&nbsp;với gi&aacute; cả hấp dẫn nhất cho tất cả mọi người.</p>\r\n', 25, 0),
(6, 'Guitar', 'Đàn Guitar Classic Ba Đờn C150', '1700000', 'Yamaha', 5, '../view/images/products/img8-1.png', '<p>- C&oacute; d&aacute;ng dreadought khuyết (cutaway)</p>\r\n\r\n<p>- Được thiết kế với chất liệu ch&iacute;nh l&agrave; gỗ&nbsp;Mahogany</p>\r\n\r\n<p>-&nbsp;Mặt ph&iacute;m v&agrave; ngựa đ&agrave;n được l', '<p>Với hơn 20 năm tồn tại, Tanglewood đ&atilde; chứng minh được m&igrave;nh l&agrave; một trong những thương hiệu guitar acoustic h&agrave;ng đầu. Kh&ocirc;ng chỉ dừng lại ở đ&oacute;, Tanglewood c&ograve;n đạt được danh hiệu &ldquo;H&atilde;ng guitar b&aacute;n chạy nhất tại Anh Quốc&rdquo;.</p>\r\n\r\n<p>Tanglewood đ&atilde; x&acirc;y dựng danh tiếng của m&igrave;nh l&agrave; một nh&agrave; sản xuất nhạc cụ mộc chất lượng cao, thể hiện được sự kết hợp giữa thiết kế truyền thống v&agrave; c&ocirc;ng nghệ sản xuất ti&ecirc;n tiến hiện đại, cho sự người chơi ở mọi cấp độ nhiều sự lựa chọn c&aacute;c c&acirc;y guitar v&agrave; nhiều loại gỗ chất lượng cao kh&aacute;c nhau.</p>\r\n\r\n<p>Một số h&atilde;ng sẽ chuy&ecirc;n sản xuất những c&acirc;y guitar gi&aacute; th&agrave;nh thấp, ph&ugrave; hợp cho người mới tập chơi nhưng sẽ kh&ocirc;ng ph&ugrave; hợp cho người chơi c&oacute; kinh nghiệm v&agrave; nghệ sĩ chuy&ecirc;n nghiệp. Trong khi một số h&atilde;ng sẽ ngược lại, sản xuất ra những c&acirc;y guitar chất lượng nhất những sẽ kh&ocirc;ng ph&ugrave; hợp với t&uacute;i tiền của nhiều người. Tanglewood cung cấp những c&acirc;y guitar ở nhiều mức gi&aacute; kh&aacute;c nhau cho mọi người chơi, sẽ kh&ocirc;ng l&agrave;m lủng t&uacute;i tiền của bạn v&agrave; những người chơi chuy&ecirc;n nghiệp cũng c&oacute; thể đặt niềm tin v&agrave;o những c&acirc;y guitar của Tanglewood khi biểu diễn.</p>\r\n\r\n<p>Trong những h&atilde;ng sản xuất guitar acoustic t&ecirc;n tuổi, &iacute;t c&oacute; h&atilde;ng n&agrave;o sản xuất n&agrave;o cung cấp nhiều c&acirc;y guitar chất lượng tốt với gi&aacute; th&agrave;nh chấp nhận được như vậy.C&oacute; thể n&oacute;i Tanglewood cung cấp những c&acirc;y guitar với chất lượng đ&aacute;ng với gi&aacute; tiền nhất hiện nay.</p>\r\n\r\n<p>Đ&agrave;n gutar acoustic&nbsp;Tanglewood TWCR DCE&nbsp;được thiết kế với chất liệu ch&iacute;nh l&agrave; gỗ&nbsp;Mahogany, với nhiều t&iacute;nh năng vượt trội l&agrave; lựa lựa chọn l&yacute; tưởng cho nhiều nghệ sĩ Guitar.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<h3><strong>C&acirc;y guitar acoustic d&aacute;ng dreadnought</strong></h3>\r\n\r\n<p>Đ&agrave;n guitar Tanglewood TWCR DCE&nbsp;c&oacute; d&aacute;ng dreadought khuyết (cutaway). Th&ugrave;ng đ&agrave;n lớn cho tiếng đ&agrave;n to, ấm v&agrave; đầy hơn, ph&ugrave; hợp cho lối chơi strumming.</p>\r\n\r\n<h3><strong>Phần lớn đ&agrave;n l&agrave;m bằng gỗ&nbsp;mahogany</strong></h3>\r\n\r\n<p>Mặt trước, lưng, h&ocirc;ng v&agrave; cần đ&agrave;n&nbsp;Guitar Acoustic Tanglewood TWCR DCE&nbsp;được l&agrave;m từ chất liệu mahogany, đặc biệt gỗ l&agrave;m mặt đ&agrave;n được tuyển chọn v&ocirc; c&ugrave;ng kỹ lưỡng v&agrave; cẩn thận. Mahogany l&agrave; một loại gỗ bền với thời gian, kh&ocirc;ng bị cong v&ecirc;nh hay nứt nẻ. Thớ gỗ mịn, đẹp, &iacute;t co gi&atilde;n, tất cả tạo n&ecirc;n một sự sang trọng, gi&aacute; trị cho chiếc guitar acoustic n&agrave;y.&nbsp;Hơn nữa, gỗ mahogany c&oacute; xu hướng nở dần ra theo thời gian. V&igrave; vậy, khi sử dụng chiếc guitar n&agrave;y, người chơi sẽ cảm nhận được sự tr&ograve;n đầy của &acirc;m thanh qua thời gian, đ&acirc;y c&oacute; lẽ l&agrave; đặc điểm nổi bật nhất thu h&uacute;t người ti&ecirc;u d&ugrave;ng của sản phẩm.</p>\r\n\r\n<h3><strong>C&aacute;c t&iacute;nh năng nổi bật kh&aacute;c</strong></h3>\r\n\r\n<p>Mặt ph&iacute;m v&agrave; ngựa đ&agrave;n được l&agrave;m từ gỗ Rosewood cho &acirc;m thanh ấm &aacute;p v&agrave; cảm gi&aacute;c thoải m&aacute;i cho người chơi.&nbsp;</p>\r\n\r\n<p>C&acirc;y đ&agrave;n được trang bị hệ thống Tanglewood TW-EX4 EQ cho &acirc;m thanh mạnh mẽ v&agrave; điều chỉnh dễ d&agrave;ng theo từng phong c&aacute;ch v&agrave; hiệu suất của người chơi.&nbsp;</p>\r\n\r\n<h3><strong>Gi&aacute; đ&agrave;n guitar Tanglewood TWCR DCE rất phải chăng</strong></h3>\r\n\r\n<p>Tanglewood TWCR DCE l&agrave; c&acirc;y đ&agrave;n l&yacute; tưởng d&agrave;nh cho những ai đang t&igrave;m kiếm c&acirc;y đ&agrave;n chất lượng với mức gi&aacute; vừa phải. Đ&acirc;y ch&iacute;nh l&agrave; sự lựa chọn tuyệt vời d&agrave;nh cho bạn, bất kể bạn l&agrave; người mới học hay l&agrave; tay chơi guitar c&oacute; nhiều kinh nghiệm.</p>\r\n', 10, 5),
(7, 'Guitar', 'Đàn Guitar Classic Yamaha C40', '2800000', 'Yamaha', 5, '../view/images/products/img11.png', '<p>- Gi&aacute; th&agrave;nh rẻ</p>\r\n\r\n<p>- Chất lượng &acirc;m thanh đạt chuẩn</p>\r\n\r\n<p>- Th&iacute;ch hợp cho người mới chơi, học sinh, sinh vi&ecirc;n...</p>\r\n', '<p><strong>Kapok D118-AC: Guitar gi&aacute; tốt cho người mới</strong></p>\r\n\r\n<p>Đ&agrave;n guitar Kapok D-118AC thuộc ph&acirc;n kh&uacute;c gi&aacute; rẻ, ph&ugrave; hợp t&uacute;i tiền với người mới bắt đầu cần một c&acirc;y đ&agrave;n l&agrave;m quen hay những bạn học sinh, sinh vi&ecirc;n điều kiện kinh tế c&ograve;n hạn chế. Kapok lu&ocirc;n đảm bảo c&aacute;c c&ocirc;ng đoạn sản xuất đều thực hiện theo quy tr&igrave;nh v&agrave; được kiểm so&aacute;t chất lượng gắt gao nhằm đảm bảo tuổi thọ đ&agrave;n v&agrave; chất lượng &acirc;m thanh đạt chuẩn.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>&Acirc;m thanh đạt chuẩn</strong></p>\r\n\r\n<p>Chất lượng &acirc;m thanh đạt chuẩn nhờ c&aacute;c bộ phận được l&agrave;m từ những chất gỗ ổn định v&agrave; được xử l&iacute; c&ocirc;ng nghiệp. Kapok D-118AC th&iacute;ch hợp với nhiều phong c&aacute;ch chơi acoustic kh&aacute;c nhau như hard strumming, fast flatpicking hay delicate fingerpicking.</p>\r\n\r\n<ul>\r\n	<li>Măt đ&agrave;n bằng gỗ spruce &eacute;p cao cấp. Gỗ Spruce &eacute;p l&agrave; loại gỗ c&oacute; độ bền cao v&agrave; rất dẻo dai, thường được sử dụng phổ biến để l&agrave;m mặt đ&agrave;n nhờ c&oacute; tốc độ truyền &acirc;m thanh, độ vang rất tốt v&agrave; giai điệu r&otilde; r&agrave;ng.</li>\r\n	<li>Lưng v&agrave; h&ocirc;ng đ&agrave;n l&agrave;m từ gỗ Linden laminated (gỗ &eacute;p) chất lượng cao mang đến &acirc;m sắc cực kỳ ấn tượng.</li>\r\n	<li>Mặt ph&iacute;m v&agrave; ngựa đ&agrave;n bằng gỗ Rosewood: mang lại cảm gi&aacute;c thoải m&aacute;i v&agrave; lướt &ecirc;m cho người chơi, g&oacute;p phần tạo n&ecirc;n &acirc;m thanh ngọt ng&agrave;o, ấm &aacute;p.</li>\r\n</ul>\r\n\r\n<p><strong>Thiết kế h&agrave;i h&ograve;a</strong></p>\r\n\r\n<p>Guitar Kapok D-118AC nổi bật với thiết kế h&agrave;i h&ograve;a, m&agrave;u sắc bắt mắt. Thiết kế nhỏ gọn tiện dụng, th&iacute;ch hợp cho tập luyện hoặc biểu diễn ở bất k&igrave; m&ocirc;i trường n&agrave;o m&agrave; kh&ocirc;ng g&acirc;y ra sự bất tiện. Kapok D-118AC nổi bật về thiết kế so với c&aacute;c guitar c&ugrave;ng ph&acirc;n kh&uacute;c nhờ lớp sơn b&oacute;ng đẹp khiến D-118AC tr&ocirc;ng thật tuyệt vời dưới &aacute;nh đ&egrave;n s&acirc;n khấu.</p>\r\n\r\n<p>Mặt ph&iacute;m v&agrave; ngựa đ&agrave;n bằng gỗ Rosewood. Viền đ&agrave;n bằng nhựa ABS l&agrave;m cho chiếc đ&agrave;n n&agrave;y trở n&ecirc;n chất lượng hơn.</p>\r\n', 15, 5),
(8, 'Guitar', 'Đàn Guitar Classic Yamaha C70', '3000000', 'Yamaha', 5, '../view/images/products/1497344081036-7931701.jpg', '<p>- Thiết kế hiện đại. Th&ugrave;ng đ&agrave;n lớn cho tiếng đ&agrave;n to, ấm v&agrave; đầy</p>\r\n\r\n<p>- Nhiều t&iacute;nh năng hiện đại, đ&aacute;nh bật mọi cảm x&uacute;c khi chơi.</p>\r\n', '<h2><strong>D&aacute;ng đ&agrave;n single-cutaway dreadnought</strong></h2>\r\n\r\n<p>Đ&agrave;n guitar CD-140SCE c&oacute; d&aacute;ng single-cutaway dreadnought (hay c&ograve;n gọi l&agrave; d&aacute;ng D khuyết). Th&ugrave;ng đ&agrave;n lớn cho tiếng đ&agrave;n to, ấm v&agrave; đầy hơn, ph&ugrave; hợp cho c&aacute;c bạn chơi đệm v&agrave; d&ugrave;ng pick, sử dụng đ&agrave;n trong c&aacute;c buổi đi chơi d&atilde; ngoại.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<h2><strong>Mặt trước đ&agrave;n bằng gỗ spruce rắn chắc</strong></h2>\r\n\r\n<p>Spruce l&agrave; loại gỗ c&oacute; độ bền v&agrave; độ dẻo dai cao, được sử dụng rất phổ biến để l&agrave;m soundboard đ&agrave;n. Đặc t&iacute;nh của gỗ Spruce l&agrave; rất cứng v&agrave; nhẹ, ch&iacute;nh v&igrave; vậy đ&acirc;y cũng l&agrave; loại gỗ c&oacute; tốc độ truyền &acirc;m thanh, độ vang rất cao, giai điệu rất r&otilde; r&agrave;ng v&agrave; đầy đủ, phản ứng tốt với bất kỳ phong c&aacute;ch chơi acoustic n&agrave;o, từ hard strumming đếm fast flatpicking hay delicate fingerpicking.</p>\r\n\r\n<h2><strong>Thanh giằng quartersawn scalloped &ldquo;X&rdquo;</strong></h2>\r\n\r\n<p>Thanh giằng quartersawn scalloped &ldquo;X&rdquo; mỏng hơn nhiều so với thanh giằng th&ocirc;ng thường. Điều đ&oacute; c&oacute; nghĩa rằng m&ocirc; h&igrave;nh giằng b&ecirc;n trong c&acirc;y đ&agrave;n sử dụng &iacute;t gỗ hơn, khiến khối lượng soundboard giảm, cho ph&eacute;p đầu cộng hưởng tự do hơn, dẫn tới những t&ocirc;ng tốt hơn với nhiều sắc th&aacute;i v&agrave; sức lan tỏa hơn.</p>\r\n\r\n<h2><strong>Mặt sau v&agrave; h&ocirc;ng đ&agrave;n bằng gỗ Rosewood</strong></h2>\r\n\r\n<p>Rosewood được nhiều&nbsp;nghệ sĩ guitar tr&ecirc;n thế giới&nbsp;đ&aacute;nh gi&aacute; l&agrave; loại gỗ cho &acirc;m sắc&nbsp;h&agrave;ng đầu trong c&aacute;c gỗ l&agrave;m guitar.</p>\r\n\r\n<h2><strong>Điều khiển cổ đ&agrave;n dễ d&agrave;ng</strong></h2>\r\n\r\n<p>Với c&aacute;c cạnh c&oacute; ng&oacute;n tay lăn thoải m&aacute;i, cổ của chiếc guitar n&agrave;y tạo ra cảm gi&aacute;c thoải m&aacute;i v&ocirc; c&ugrave;ng, l&yacute; tưởng cho người chơi bắt đầu hoặc những người chưa c&oacute; nhiều kinh nghiệm.</p>\r\n\r\n<h2><strong>Hệ thống pickup của Fishman</strong></h2>\r\n\r\n<p>CD-140SCE&nbsp;sử dụng pick-up của Fishman với EQ 3-band với c&aacute;c t&ugrave;y chỉnh: bass, mid, treble v&agrave; một n&uacute;t điều chỉnh &acirc;m lượng. Đặc biệt c&ograve;n c&oacute; chức năng chromatic tuner t&iacute;ch hợp sẵn gi&uacute;p bạn giữ c&acirc;y đ&agrave;n đ&uacute;ng t&ocirc;ng, v&agrave; chromatic tuner gi&uacute;p bạn dễ d&agrave;ng chỉnh d&acirc;y theo những t&ocirc;ng kh&aacute;c nhau (Half step down, D tuning, Open E...) hơn những loại tuner th&ocirc;ng thường. Preamp của Fishman&reg; mang lại hiệu suất khuếch đại v&ocirc; song cho c&acirc;y đ&agrave;n của bạn, ph&ugrave; hợp với những ai c&oacute; nhu cầu chơi biểu diễn tr&ecirc;n s&acirc;n khấu với hệ thống &acirc;m thanh lớn.</p>\r\n\r\n<h2><strong>Ngựa đ&agrave;n, lược đ&agrave;n v&agrave; xương đ&agrave;n Nubone của h&atilde;ng Graphtech</strong></h2>\r\n\r\n<p>C&acirc;y CD-140SCE được trang bị ngựa đ&agrave;n gỗ rosewood loại tốt, lược đ&agrave;n v&agrave; xương đ&agrave;n Nubone của h&atilde;ng Graphtech, tăng cường sự bền vững, r&otilde; r&agrave;ng v&agrave; t&ocirc;ng m&agrave;u nhất qu&aacute;n của một nhạc cụ. Kh&ocirc;ng qu&aacute; mềm hoặc qu&aacute; gi&ograve;n, ch&uacute;ng loại bỏ r&agrave;ng buộc chuỗi phiền h&agrave; trong khi mang lại hiệu suất, khả năng hoạt động v&agrave; ngoại h&igrave;nh dễ d&agrave;ng vượt qua c&aacute;c loại nhựa kh&aacute;c, gần giống với xương thật.</p>\r\n', 100, 10),
(9, 'Guitar', 'Đàn Guitar Acoustic Rosen G11 (nhiều màu)', '1800000', 'Yamaha', 5, '../view/images/products/dan-electric-guitar-pacifica212vqm-mau-do-gach.jpg', '<p>- Nổi bật với kiểu d&aacute;ng hiện đại, bắt mắt.</p>\r\n\r\n<p>- Th&ugrave;ng đ&agrave;n được thiết kế d&agrave;y tạo &acirc;m thanh trong v&agrave; vang.</p>\r\n\r\n<p>- Gi&aacute; phải chăng, rất th&iac', '<p><strong>Đ&agrave;n Guitar Suzuki SDG-6NL</strong>&nbsp;với cấu tạo th&ocirc;ng dụng, cần đ&agrave;n đượcgia c&ocirc;ng kỹ lưỡng, giữ độ thẳng cho cần, điều n&agrave;y sẽ gi&uacute;p giảm nhẹ lực bấm đ&agrave;ncho người chơi, đặc biệt đối với người mới sử dụng, tập đ&aacute;nh đ&agrave;n guitar, lựcng&oacute;n tay yếu n&ecirc;n chọn đ&agrave;n guitar SDG-6PK.</p>\r\n\r\n<p><strong>Đ&agrave;n Guitar Suzuki SDG-6NL</strong>&nbsp;l&agrave; d&ograve;ng sản phẩm mới của suzuki,d&ograve;ng đ&agrave;n ch&uacute; trọng về &acirc;m thanh, th&ugrave;ng đ&agrave;n được thiết kế d&agrave;y tạo &acirc;m thanh trongv&agrave; vang, điều n&agrave;y gi&uacute;p người chơi cảm nhận &acirc;m thanh một c&aacute;ch tốt nhất</p>\r\n', 10, 10),
(10, 'Guitar', 'Đàn Guitar Acoustic Rosen G12 (nhiều màu)', '2300000', 'Yamaha', 5, '../view/images/products/img8-1-ab137154-f76d-463c-a998-3f359b535692.png', '<p>- D&aacute;ng đ&agrave;n dreadnought cho &acirc;m thanh d&agrave;y, mạnh mẽ</p>\r\n\r\n<p>- Mặt trước gỗ Spruce kết hợp mặt sau v&agrave; h&ocirc;ng mahogany</p>\r\n\r\n<p>- Cần đ&agrave;n mỏng gỗ mahogany', '<h2><strong>Takamine GD10: Guitar acoustic &acirc;m thanh lớn</strong></h2>\r\n\r\n<p>Takamine GD10 l&agrave; c&acirc;y guitar được thiết kế đẹp mắt với th&ugrave;ng đ&agrave;n lớn d&aacute;ng dreadnought quen thuộc mang tới &acirc;m thanh acoustic cực kỳ mạnh mẽ. C&acirc;y đ&agrave;n l&agrave; lựa chọn tuyệt vời cho bất kỳ người chơi guitar n&agrave;o đang t&igrave;m kiếm một c&acirc;y đ&agrave;n mang tới trải nghiệm chơi thoải m&aacute;i với mức gi&aacute; phải chăng.</p>\r\n\r\n<h2><strong>D&aacute;ng đ&agrave;n Dreadnought</strong></h2>\r\n\r\n<p>Đ&agrave;n guitar Takamine GD10 c&oacute; d&aacute;ng dreadnought quen thuộc, đ&acirc;y l&agrave; d&aacute;ng đ&agrave;n phổ biến nhất trong nửa thế kỷ trước. D&aacute;ng đ&agrave;n n&agrave;y c&oacute; th&ugrave;ng đ&agrave;n lớn, mang tới &acirc;m trầm mạnh mẽ với &acirc;m lượng lớn, &acirc;m thanh ng&acirc;n l&acirc;u trong th&ugrave;ng đ&agrave;n.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<h2><strong>Mặt trước Spruce với mặt sau v&agrave; h&ocirc;ng Mahogany</strong></h2>\r\n\r\n<p>GD10 t&iacute;ch hợp c&aacute;c t&iacute;nh năng nổi trội như mặt trước gỗ spruce tuyển chọn kết hợp với mặt sau v&agrave; h&ocirc;ng gỗ mahogany cho &acirc;m thanh d&agrave;y, đầy đủ v&agrave; c&acirc;n bằng. Đ&acirc;y được xem như sự kết hợp chất liệu gỗ ho&agrave;n hảo cho cấu tr&uacute;c của một c&acirc;y guitar chất lượng, được ứng dụng ở rất nhiều d&ograve;ng guitar cao cấp đắt tiền tr&ecirc;n thế giới.</p>\r\n\r\n<h2><strong>Cấu tr&uacute;c cần đ&agrave;n cho cảm gi&aacute;c chơi mượt m&agrave;</strong></h2>\r\n\r\n<p>Takamine GD10 c&oacute; cần đ&agrave;n mỏng bằng gỗ mahogany với lớp sơn satin v&agrave; mặt ph&iacute;m gỗ avangkol b&aacute;n k&iacute;nh 12&rdquo; gi&uacute;p tối ưu h&oacute;a lối chơi mượt m&agrave; v&agrave; cho bạn cảm gi&aacute;c v&ocirc; c&ugrave;ng thoải m&aacute;i.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<h2><strong>C&aacute;c t&iacute;nh năng tuyệt vời kh&aacute;c</strong></h2>\r\n\r\n<p>C&aacute;c t&iacute;nh năng kh&aacute;c bao gồm một ngựa đ&agrave;n pin-less ovangkol cho việc thay d&acirc;y đ&agrave;n dễ d&agrave;ng hơn, nut v&agrave; xương ngựa đ&agrave;n bằng nhựa tổng hợp, pearloid dot inlays, bộ chỉnh &acirc;m chrome die-cast v&agrave; một m&agrave;u sắc đơn giản m&agrave; kh&ocirc;ng k&eacute;m phần thu h&uacute;t Natural satin.</p>\r\n', 10, 10),
(11, 'Guitar', 'Đàn Guitar Acoustic Yamaha FG800 (nhiều màu)', '6000000', 'Yamaha', 4, '../view/images/products/dan-guitar-dien-yamaha-pacifica-112vm-2.jpg', '<p>- Mặt trước (Top) l&agrave; gỗ th&ocirc;ng nguy&ecirc;n khối (Solid),</p>\r\n\r\n<p>- Mặt h&ocirc;ng v&agrave; lưng được l&agrave;m từ gỗ Mahogany</p>\r\n\r\n<p>-&nbsp;20 ph&iacute;m đ&agrave;n tr&ecirc;n ', '<p>Nổi bật với chất lượng &acirc;m thanh v&agrave; loại gỗ sử dụng, c&acirc;y&nbsp;đ&agrave;n guitar Samick&nbsp;GD-100 SCE&nbsp;hat c&ograve;n gọi l&agrave; Greg Bennett GD-100SCE mang lại một đẳng cấp kh&aacute;c biệt v&agrave; một nền nhạc thu h&uacute;t nhất.</p>\r\n\r\n<p>Greg Bennett GD-100SCE Electro Acoustic c&oacute; lối chơi mượt m&agrave; v&agrave; mang h&igrave;nh d&aacute;ng dreadnought thu h&uacute;t với th&acirc;n đ&agrave;n bằng gỗ Gụ v&agrave; cần đ&agrave;n bằng gỗ hồng mộc với 20 ph&iacute;m bấm. Hệ thống preamp mạnh mẽ Fishman ISYS T với tuner cho ph&eacute;p kết nối đến ampli v&agrave; c&aacute;c hệ thống PA trong khi ngựa đ&agrave;n bằng gỗ hồng mộc, xương đ&agrave;n PPS v&agrave; bộ điều chỉnh bằng cr&ocirc;m kh&ocirc;ng những cho &acirc;m thanh vang dội m&agrave; c&ograve;n tạo cho GD-100SCE c&oacute; được vẻ ngo&agrave;i rất tuyệt vời. Th&acirc;n đ&agrave;n bằng gỗ Gụ v&agrave; mặt trước bằng gỗ Tuyết t&ugrave;ng nguy&ecirc;n miếng Với cấu tr&uacute;c th&acirc;n đ&agrave;n v&agrave; cần đ&agrave;n bằng gỗ Gụ, cho Greg Bennett GD-100SCE một &acirc;m thanh rất vang dội nhưng cũng rất s&acirc;u lắng. Chất gỗ d&agrave;y của gỗ Gụ dội lại &acirc;m thanh rất nhanh mang đến chất &acirc;m ấm &aacute;p v&agrave; lắng đọng ở những qu&atilde;ng trầm nhưng vẫn giữ được tiếng vang r&otilde; r&agrave;ng ở qu&atilde;ng trung v&agrave; qu&atilde;ng cao. Kết hợp với mặt trước bằng gỗ Tuyết t&ugrave;ng nguy&ecirc;n miếng l&agrave;m tăng sự r&otilde; r&agrave;ng trong tiếng vang v&agrave; cho một &acirc;m sắc tươi s&aacute;ng mang đến cho D-2CE một giai điệu độc đ&aacute;o ph&ugrave; hợp với nhiều phong c&aacute;ch chơi guitar kh&aacute;c nhau.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>Ph&iacute;m bấm bằng gỗ hồng mộc</strong></p>\r\n\r\n<p>Hơn 20 ph&iacute;m đ&agrave;n tr&ecirc;n mặt ph&iacute;m rosewood ph&aacute;t huy được tối đa lối chơi mượt m&agrave; linh hoạt của chiếc GD-100SCE, ngo&agrave;i ra b&aacute;n k&iacute;nh b&agrave;n ph&iacute;m 12 inch đủ rộng để dễ d&agrave;ng chơi những giai điệu phức tạp.</p>\r\n\r\n<p><strong>Fishman ISYS T Preamp</strong></p>\r\n\r\n<p>Kết hợp hệ thống preamp Fishman ISYS với tuner loại on board chromatic, GD-100SCE Electro Acoustic c&oacute; thể được kết nối với bộ khuếch đại v&agrave; hệ thống PA để cung cấp một giai điệu &acirc;m thanh d&agrave;y v&agrave; ch&acirc;n thực. Với bộ điều chỉnh &acirc;m lượng, contour v&agrave; kiểm so&aacute;t giai đoạn, ISYS T c&oacute; thể t&igrave;m kiếm v&agrave; điều chỉnh ho&agrave;n hảo c&aacute;c giai điệu khi đang chơi trong khi m&agrave;n h&igrave;nh LED tr&ecirc;n tuner cho ph&eacute;p điều chỉnh &acirc;m điệu dễ d&agrave;ng ngay cả trong điều kiện &aacute;nh s&aacute;ng thấp.</p>\r\n', 20, 20),
(12, 'Organ', 'Đàn Organ Yamaha PSR F51', '2000000', 'Yamaha', 3, '../view/images/products/14732365701-4467159.jpg', '<p>- Bảo h&agrave;nh ch&iacute;nh h&atilde;ng 2 năm</p>\r\n\r\n<p>- Chip &acirc;m thanh&nbsp;AiX. 600&nbsp; &acirc;m thanh</p>\r\n\r\n<p>- M&agrave;n h&igrave;nh hiển thị LCD r&otilde; r&agrave;ng</p>\r\n\r\n<p>-', '<p><strong>1.Nguồn &acirc;m thanh AiX mang đến &acirc;m thanh chất lượng cao v&agrave; khả năng biểu cảm phong ph&uacute;</strong></p>\r\n\r\n<p>D&ograve;ng sản phẩm CT-X đi k&egrave;m với Nguồn &acirc;m thanh AiX, vốn c&oacute; thể tạo ra nhiều chất lượng &acirc;m thanh từ &acirc;m trầm mạnh mẽ cho đến &acirc;m cao r&otilde; r&agrave;ng. Sức mạnh t&iacute;nh to&aacute;n vượt trội của LSI hiệu năng cao t&aacute;i tạo lại sự quyến rũ tự nhi&ecirc;n của &acirc;m thanh nhạc cụ, chẳng hạn như sự thay đổi &acirc;m sắc dễ chịu khi nhấn ph&iacute;m piano, cảm gi&aacute;c rung động khi đ&aacute;nh trống hoặc sự th&aacute;nh th&oacute;t của đ&agrave;n d&acirc;y.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Bộ xử l&yacute; t&iacute;n hiệu số tốc độ cao cũng được &aacute;p dụng cho từng loại &acirc;m thanh, chẳng hạn như giai điệu, bass v&agrave; trống đệm hoặc đ&agrave;n organ. Ngay cả &acirc;m tổng hợp ti&ecirc;u chuẩn cho đ&agrave;n organ vẫn tạo ra &acirc;m thanh ri&ecirc;ng biệt cho từng nhạc cụ để cho ph&eacute;p tr&igrave;nh diễn biểu cảm.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>M&agrave;n h&igrave;nh LCD r&otilde; r&agrave;ng, hữu &iacute;ch, c&oacute; đ&egrave;n nền</strong></p>\r\n\r\n<p>Hiển thị c&aacute;c th&ocirc;ng tin hữu &iacute;ch, thuận tiện khi tr&igrave;nh diễn hoặc tập đ&agrave;n. Đ&egrave;n nền gi&uacute;p dễ đọc.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>Phản hồi chạm gi&uacute;p tr&igrave;nh diễn biểu cảm (Đ&agrave;n organ cảm ứng)</strong></p>\r\n\r\n<p>Tr&igrave;nh diễn biểu cảm với c&aacute;c mức &acirc;m thanh kh&aacute;c nhau thay đổi theo lực nhấn ph&iacute;m, giống như đ&agrave;n piano thật.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>B&agrave;n ph&iacute;m piano gi&uacute;p tr&igrave;nh diễn sống động</strong></p>\r\n\r\n<p>Đ&agrave;n organ c&oacute; c&aacute;c ph&iacute;m piano gi&uacute;p tập đ&agrave;n dễ d&agrave;ng v&agrave; mang lại trải nghiệm tr&igrave;nh diễn trung thực.</p>\r\n\r\n<p><em>Mời c&aacute;c bạn c&ugrave;ng tham khảo những th&ocirc;ng tin về sản phẩm đ&agrave;n organ Casio CT-X800 dưới đ&acirc;y:</em></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<h3><strong>2.Dễ d&agrave;ng chơi nhạc đ&uacute;ng &yacute; bạn muốn</strong></h3>\r\n\r\n<p><strong>- Hợp &acirc;m rải</strong></p>\r\n\r\n<p>Tự động chơi hợp &acirc;m rải v&agrave; đoản kh&uacute;c chỉ bằng c&aacute;ch nhấn c&aacute;c ph&iacute;m. C&aacute;c đoản kh&uacute;c c&agrave;i sẵn như hợp &acirc;m rải guitar v&agrave; chơi kiểu g&otilde; ph&iacute;m hoặc đoản kh&uacute;c ph&ugrave; hợp với m&agrave;n tr&igrave;nh diễn kiểu nhạc điện tử hay nhạc trầm.</p>\r\n', 25, 25),
(13, 'Organ', 'Đàn Organ Yamaha PSR E263', '3500000', 'Yamaha', 5, '../view/images/products/1445652242154-5086533.jpg', '<ul>\r\n	<li>Trang bị Mic and Audio</li>\r\n	<li>61 ph&iacute;m s&aacute;ng</li>\r\n	<li>400 tones and 77 điệu&nbsp;</li>\r\n</ul>\r\n\r\n<p><strong>Sản phẩm bao gồm:</strong>&nbsp;AD9V, CXDON&nbsp;</p>\r\n', '<p>Trong th&aacute;ng 8 năm 2019, tại Summer NAMM show, Casio ra mắt hai d&ograve;ng sản phẩm Keyboard mới d&agrave;nh cho trẻ em, c&oacute; sức h&uacute;t mạnh mẽ với c&aacute;c nh&agrave; đầu tư nhạc cụ cũng như người d&ugrave;ng với ph&acirc;n kh&uacute;c đ&agrave;n Keyboard phổ th&ocirc;ng thuộc d&ograve;ng Casiotone CT-S series v&agrave; Casiotone Keyboard LK-S250.&nbsp;</p>\r\n\r\n<p>Casiotone LK-S250 l&agrave; một model đ&agrave;n được ph&aacute;t triển n&acirc;ng cấp từ d&ograve;ng sản phẩm Organ ph&iacute;m s&aacute;ng v&ocirc; c&ugrave;ng th&agrave;nh c&ocirc;ng của h&atilde;ng n&agrave;y trước đ&oacute;, ở lần ra mắt năm nay, sản phẩm n&agrave;y vẫn hứa hẹn mang về một m&ugrave;a bội thu cho Casio v&igrave; những t&iacute;nh năng vượt trội ho&agrave;n to&agrave;n hướng đến lợi &iacute;ch người d&ugrave;ng.&nbsp;</p>\r\n\r\n<h2><strong>Thiết kế si&ecirc;u mỏng, thời trang v&agrave; s&agrave;nh điệu</strong></h2>\r\n\r\n<p>Casiotone LK-S250 c&oacute; k&iacute;ch thước rất nhỏ gọn 930cm x 256cm x 73mm, c&acirc;n nặng 3,4kg, k&iacute;ch thước n&agrave;y của c&acirc;y đ&agrave;n cho ph&eacute;p người chơi c&oacute; thể mang đ&agrave;n đi bất cứ khu vực n&agrave;o m&agrave; họ muốn, khi quyết định k&iacute;ch thước cuối c&ugrave;ng cho c&acirc;y đ&agrave;n, c&aacute;c kỹ sư Casio đ&atilde; dựa tr&ecirc;n t&iacute;nh c&aacute;ch th&iacute;ch di chuyển của đối tượng c&acirc;y đ&agrave;n l&agrave; trẻ em, n&ecirc;n việc lựa chọn k&iacute;ch thước si&ecirc;u mỏng đ&atilde; ho&agrave;n to&agrave;n chinh phục được trẻ nhỏ hướng về thi&ecirc;n t&iacute;nh hiếu động, ham chơi, ưa hoạt động v&agrave; th&iacute;ch di chuyển của ch&uacute;ng.&nbsp;</p>\r\n\r\n<p>Cũng với những ti&ecirc;u ch&iacute; thiết kế với mục đ&iacute;ch đảm bảo t&iacute;nh linh hoạt cho vị tr&iacute; đặt đ&agrave;n,Casiotone LK-S250 c&ograve;n được thiết kế phần tay cầm ph&iacute;a tr&ecirc;n đầu đ&agrave;n, đảm bảo việc di chuyển an to&agrave;n v&agrave; nhanh ch&oacute;ng cho bất kỳ ai.&nbsp;</p>\r\n\r\n<p>So với những model đ&agrave;n Light Keyboard trước đ&acirc;y của Casio, LK-S250 c&oacute; thời lượng pin sử dụng d&agrave;i hơn, l&ecirc;n đến 16 giờ ( điều n&agrave;y kh&ocirc;ng tuyệt đối v&igrave; c&ograve;n t&ugrave;y thuộc v&agrave;o hiệu suất chơi, nếu bạn nhỏ chơi đ&agrave;n li&ecirc;n tục&nbsp; với những phong c&aacute;ch cần điều chỉnh, phối hợp nhiều th&igrave; năng lượng pin sẽ ti&ecirc;u hao nhanh hơn).</p>\r\n\r\n<p>Những phần g&oacute;c cạnh của c&acirc;y đ&agrave;n cũng được c&aacute;c&nbsp; nh&agrave; thiết kế t&iacute;nh to&aacute;n tỉ mỉ tr&aacute;nh g&acirc;y bất kỳ thương tổn n&agrave;o cho trẻ khi d&ugrave;ng c&aacute;c g&oacute;c bo tr&ograve;n cho c&acirc;y đ&agrave;n, điều n&agrave;y n&oacute;i l&ecirc;n thương hiệu Casio chăm ch&uacute;t từng chi tiết cho Casiotone&nbsp;LK-S250 để hướng đến tối đa lợi &iacute;ch cho người chơi đ&agrave;n v&agrave; c&aacute;c em b&eacute;.&nbsp;</p>\r\n\r\n<h2><strong>Giao diện bắt mắt, đơn giản, dễ sử dụng, hệ thống &acirc;m thanh nổi trội</strong></h2>\r\n\r\n<p>Bảng điều khiển của Casiotone&nbsp;LK-250 v&ocirc; c&ugrave;ng ngọt ng&agrave;o khi được trang tr&iacute; bằng hai tone m&agrave;u chủ đạo, đen v&agrave; xanh l&aacute; c&acirc;y, điều n&agrave;y thu h&uacute;t sự hứng th&uacute; kh&aacute;m ph&aacute; cho bất kỳ ai khi được sở hữu c&acirc;y đ&agrave;n, nhất l&agrave; c&aacute;c em nhỏ.&nbsp;</p>\r\n\r\n<p>C&aacute;c n&uacute;t điều khiển v&ocirc; c&ugrave;ng &iacute;t, với c&aacute;c n&uacute;t chức năng dễ thao t&aacute;c c&ugrave;ng m&agrave;n h&igrave;nh led hiển thị những th&ocirc;ng tin đơn giản, kh&ocirc;ng g&acirc;y sự rối mắt phức tạp cho những người mới chơi. B&agrave;n ph&iacute;m hai m&agrave;u đen trắng, nổi bật với c&aacute;c ph&iacute;m ph&aacute;t s&aacute;ng khi chơi sẽ dẫn dắt&nbsp;người chơi&nbsp;niềm hứng khởi bất tận b&ecirc;n c&acirc;y đ&agrave;n.&nbsp;</p>\r\n\r\n<p>Việc sử dụng loa h&igrave;nh dầu bục 13cm x 6cm với nhiều nam ch&acirc;m hơn được tăng cường mang đến &acirc;m thanh tốt hơn mong đợi cho một c&acirc;y Organ d&ograve;ng ph&iacute;m s&aacute;ng như LK-S250. Ngo&agrave;i ra Casio c&ograve;n t&iacute;ch hợp t&iacute;nh năng bổ sung v&agrave;o hệ thống thoa, gi&uacute;p tối ưu h&oacute;a, c&acirc;n bằng từ &acirc;m trầm đến &acirc;m cao của c&acirc;y đ&agrave;n, mang đến nhiều cảm x&uacute;c hơn cho bản nhạc khi chơi.&nbsp;</p>\r\n\r\n<h2><strong>C&aacute;c hiệu ứng kỹ thuật số mang &acirc;m thanh vui vẻ đến cho căn nh&agrave; của bạn</strong></h2>\r\n\r\n<p>C&acirc;y đ&agrave;n Casiotone LK-S250 được t&iacute;ch hợp hiệu ứng Dance Music bao gồm chế độ cho ph&eacute;p người d&ugrave;ng kết hợp c&aacute;c loại nhạc cụ từ trống, bass để s&aacute;ng tạo ra một giai điệu mới s&ocirc;i động hơn k&egrave;m theo 12 loại &acirc;m thanh kh&aacute;c. Khi muốn sử dụng hiệu ứng n&agrave;y, người chơi chỉ cần nhấn một n&uacute;t tr&ecirc;n bảng điều khiển để c&oacute; th&ecirc;m những điều th&uacute; vị cho bản nhạc đang chơi.&nbsp;</p>\r\n\r\n<h2><strong>Hệ thống ph&iacute;m s&aacute;ng độc đ&aacute;o cho hứng khởi chơi nhạc bất tận</strong></h2>\r\n\r\n<p>Những ph&iacute;m s&aacute;ng của b&agrave;n ph&iacute;m Lighting Keyboard được sử dụng một c&aacute;ch triệt để trong c&aacute;c b&agrave;i học Organ, ch&uacute;ng sẽ s&aacute;ng l&ecirc;n, b&aacute;o trước ph&iacute;m cần phải nhấn tiếp theo tr&ecirc;n b&agrave;n ph&iacute;m, chức năng n&agrave;y gi&uacute;p c&aacute;c bạn nhỏ thực h&agrave;nh dễ d&agrave;ng hơn với c&aacute;c b&agrave;i tập Organ đầu đời.&nbsp;</p>\r\n\r\n<p>Ngo&agrave;i hiệu ứng ph&iacute;m s&aacute;ng thu h&uacute;t, thiết kế b&agrave;n ph&iacute;m của Casio Lk-S250 c&ograve;n m&ocirc; phỏng ho&agrave;n hảo hai ph&iacute;m đen trắng của c&acirc;y đ&agrave;n Piano sang trọng v&agrave; được x&acirc;y dựng trong một thiết kế h&igrave;nh hộp c&oacute; độ phản hồi &acirc;m thanh, mang đến &acirc;m lượng cảm x&uacute;c giống như c&acirc;y Piano cổ điển.&nbsp;</p>\r\n\r\n<h2><strong>Kết nối rộng mở, cải thiện khả năng chơi nhạc nhanh ch&oacute;ng bằng ứng dụng học tập ti&ecirc;n tiến Chordana Play</strong></h2>\r\n\r\n<p>LK-S250 c&oacute; c&aacute;c cổng kết nối USB ( đi k&egrave;m bộ chuyển đổi ri&ecirc;ng), c&ugrave;ng cổng kết nối giắc cắm cho ph&eacute;p người chơi lưu trữ những bản nhạc đ&atilde; chơi, khơi nguồn s&aacute;ng tạo cho người chơi sở hữu c&acirc;y đ&agrave;n.&nbsp;</p>\r\n\r\n<p>Ứng dụng Chordana Play với bộ nhớ 50 b&agrave;i h&aacute;t demo, ứng dụng n&agrave;y cũng c&oacute; thể lưu trữ th&ecirc;m c&aacute;c tệp nhạc được tải xuống từ MIDI, gi&uacute;p bạn&nbsp;c&oacute; thể chơi nhạc với giai điệu m&igrave;nh th&iacute;ch m&agrave; kh&ocirc;ng bị bất kỳ cản trở n&agrave;o.&nbsp;</p>\r\n\r\n<p>T&iacute;nh năng chấm điểm của ứng dụng n&agrave;y đối với c&aacute;c b&agrave;i tập nhạc cũng gi&uacute;p người chơi hoặc&nbsp;phụ huynh c&oacute; thể đ&aacute;nh gi&aacute; được qu&aacute; tr&igrave;nh tiến bộ của b&eacute; th&ocirc;ng qua thời gian tập đ&agrave;n, gi&uacute;p người chơi tận dụng tối đa gi&aacute; trị do c&acirc;y đ&agrave;n.</p>\r\n\r\n<h2><strong>Tổng kết:&nbsp;</strong></h2>\r\n\r\n<p>Một c&acirc;y đ&agrave;n ph&iacute;m s&aacute;ng với c&aacute;c thiết kế được t&iacute;nh to&aacute;n tỉ mỉ như k&iacute;ch thước nhỏ gọn, c&aacute;c g&oacute;c đ&agrave;n bo tr&ograve;n, bảng thiết kế đơn giản, xinh đẹp, c&aacute;c ứng dụng học tập ph&ugrave; hợp, đều hướng đến lợi &iacute;ch tối đa nhất l&agrave; khơi nguồn sự hứng th&uacute; &acirc;m nhạc cho trẻ nhỏ, c&aacute;c kỹ sư thiết kế của thương hiệu Casio đ&atilde; rất th&agrave;nh c&ocirc;ng khi thiết kế những c&acirc;y đ&agrave;n gi&aacute;o dục như thế n&agrave;y. Thật sự sẽ khiến bạn phải nuối tiếc, khi muốn mua một c&acirc;y đ&agrave;n Organ ph&iacute;m s&aacute;ng cho b&eacute; nh&agrave; m&igrave;nh m&agrave; lại bỏ qua Casiotone&nbsp;LK-S250.&nbsp;</p>\r\n', 23, 20),
(14, 'Piano', 'Đàn Piano Upright Yamaha JU109 PE', '60000000', 'Yamaha', 5, '../view/images/products/144548251371-8695719.jpg', '<p>- Trợ gi&aacute; từ nh&agrave; sản xuất</p>\r\n\r\n<p>- Giao h&agrave;ng ngay trong ng&agrave;y khi c&oacute; y&ecirc;u cầu</p>\r\n\r\n<p>- Bảo h&agrave;nh ch&iacute;nh h&atilde;ng 5 năm, bảo tr&igrave; 3&', '<h3><strong>1.Thiết kế của Bản cộng hưởng (Soundboard)</strong></h3>\r\n\r\n<p>Bản cộng hưởng Soundboard v&agrave; sườn đ&agrave;n l&agrave; hai yếu tố quan trọng nhất l&agrave;m n&ecirc;n &acirc;m thanh của một c&acirc;y đ&agrave;n piano. Bản cộng hưởng được l&agrave;m từ gỗ rắn chắc, chắc chắn được lựa chọn cẩn thận v&agrave; thử nghiệm một c&aacute;ch khoa học, để đạt ti&ecirc;u chuẩn độ ng&acirc;n của thanh &acirc;m tốt nhất. Nhờ đ&oacute; tạo ra tiếng đ&agrave;n hay hơn với &acirc;m vực chuẩn hơn.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Bản cộng hưởng của đ&agrave;n, độ d&agrave;i của d&acirc;y v&agrave; số thanh gỗ/xương sườn (ribs) được n&acirc;ng tới mức tối đa để vượt hơn c&aacute;c th&ocirc;ng số kỹ thuật của những đ&agrave;n piano kh&aacute;c, v&agrave; đem lại tiếng đ&agrave;n mạnh mẽ v&agrave; ng&acirc;n vang hơn.&nbsp;Tấm Soundboard nằm thẳng, tạo sự đ&agrave;n hồi, độ vang tốt. C&oacute; thể đ&agrave;n c&aacute;c mức độ từ ppp (cực nhẹ) đến fff (cực mạnh &Acirc;m thanh vang đều v&agrave; thống nhất từ thấp l&ecirc;n cao Kh&ocirc;ng c&oacute; tạp &acirc;m.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<h3><strong>2. Đ&agrave;n Piano Kawai ND-21 với Bộ m&aacute;y cực nhạy v&agrave; b&uacute;a đ&agrave;n bền</strong></h3>\r\n\r\n<p>Gi&aacute; trị cốt l&otilde;i của Kawai ND-21 l&agrave; tạo ra &acirc;m thanh tuyệt hảo, độ nhạy ph&iacute;m đ&agrave;n, lực phản hồi từ b&agrave;n ph&iacute;m khi ng&oacute;n tay chạm v&agrave;o sẽ nhanh v&agrave; nhạy hơn. Ngo&agrave;i bản cộng hưởng được l&agrave;m bằng gỗ v&acirc;n sam rắn chắc, Kawai ND-21 c&ograve;n sử dụng bộ m&aacute;y đ&agrave;n v&agrave; b&uacute;a đ&agrave;n cực nhạy, kết hợp với những kỹ thuật thủ c&ocirc;ng ti&ecirc;n tiến tr&ecirc;n thế giới. Nhờ đ&oacute;, c&aacute;c nghệ sĩ piano cảm nhận được &acirc;m điệu v&agrave; độ nhạy khi chạm v&agrave;o từng ph&iacute;m đ&agrave;n.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>C&aacute;c th&agrave;nh phần cốt l&otilde;i trong bộ m&aacute;y đ&agrave;n được thiết kế với trọng lượng nhẹ hơn v&agrave; cải tiến cấu tr&uacute;c nhằm tối ưu h&oacute;a tốc độ, sự lặp lại v&agrave; kiểm so&aacute;t tốt hơn. Mọi chi tiết ch&iacute;nh x&aacute;c của bộ cơ được ph&acirc;n t&iacute;ch v&agrave; điều chỉnh ph&ugrave; hợp nhằm tạo ra cảm gi&aacute;c th&acirc;n mật giữa &acirc;m nhạc v&agrave; nghệ sĩ. Độ nặng của ph&iacute;m đ&agrave;n Kawai được so s&aacute;nh với c&aacute;c Bộ m&aacute;y cơ Upright Piano của c&aacute;c d&ograve;ng thương hiệu cao cấp kh&aacute;c, gi&uacute;p nghệ sĩ thực hiện c&aacute;c kỹ thuật piano dễ hơn v&agrave; ch&iacute;nh x&aacute;c hơn.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<h3><strong>3. Kawai ND-21 Kiểu d&aacute;ng, thiết kế đẹp v&agrave; h&agrave;i h&ograve;a</strong></h3>\r\n\r\n<p>Kh&aacute;c biệt với c&aacute;c loại nội thất kh&aacute;c, đ&agrave;n piano lu&ocirc;n l&agrave; điểm nhấn của sự sang trọng v&agrave; qu&yacute; ph&aacute;i. Được trang bị tinh tế với kim loại bạc v&agrave; khung bảng cộng hưởng m&agrave;u đen, Kawai ND-21 đ&atilde; tạo được ấn tượng mang t&iacute;nh hiện đại nhưng vẫn t&ocirc;n vinh n&eacute;t đẹp văn h&oacute;a của bất k&igrave; kh&ocirc;ng gian n&agrave;o.</p>\r\n', 40, 20),
(15, 'Piano', 'Đàn Piano Upright Yamaha JU109 PE', '5950000', 'Yamaha', 5, '../view/images/products/1453284246041-3217437.jpg', '<p>- C&oacute; nhiều m&agrave;u sắc, mang đến sự lựa chọn phong ph&uacute; cho người chơi.</p>\r\n\r\n<p>- &Acirc;m thanh của c&acirc;y đ&agrave;n trở n&ecirc;n s&aacute;ng hơn theo thời gian.</p>\r\n\r\n<p>-', '<h3><strong>1. Cảm ứng ph&iacute;m đ&agrave;n piano Kawai K300</strong></h3>\r\n\r\n<p>Sự hoạt động ổn định trong khoảng thời gian d&agrave;i của độ cảm ứng ph&iacute;m chỉ c&oacute; thể đạt được bằng việc đạt đến một thiết kế th&ocirc;ng minh c&ugrave;ng những phương ph&aacute;p v&agrave; vật liệu ti&ecirc;n tiến nhất.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>Bộ cơ đ&agrave;n upright piano&nbsp;Millennium III</strong></p>\r\n\r\n<p>C&aacute;c bộ phận đặc biệt của bộ m&aacute;y cơ Thi&ecirc;n ni&ecirc;n kỷ thế hệ thứ III (Millennium III Action) được l&agrave;m bằng ABS - Carbon, một chất liệu tổng hợp được tạo ra bởi sự pha trộn của sợi carbon v&agrave; ABS styran &ndash; một chất liệu nổi tiếng của&nbsp;<a href=\"https://vietthuong.vn/dan-piano-kawai-upright.html\">piano Kawai</a>. ABS - Carbon l&agrave; chất liệu v&ocirc; c&ugrave;ng mạnh mẽ v&agrave; cứng c&aacute;p, cho ph&eacute;p tạo ra c&aacute;c bộ phận hoạt động nhẹ hơn m&agrave; kh&ocirc;ng phải hao tốn nhiều sức lực. Với chất liệu đặc biệt n&agrave;y, bộ m&aacute;y cơ hoạt động mạnh mẽ hơn, nhanh nhạy hơn v&agrave; mang lại những ưu điểm tuyệt vời: khả năng hoạt động vượt trội, sự kiểm so&aacute;t tốt hơn v&agrave; ổn định hơn so với bộ m&aacute;y cơ l&agrave;m bằng gỗ th&ocirc;ng thường.&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>Gi&aacute; đỡ bộ cơ bằng nh&ocirc;m đ&uacute;c &aacute;p lực</strong></p>\r\n\r\n<p>Piano Kawai K-300 được l&agrave;m từ một khu&ocirc;n mẫu, những gi&aacute; đỡ bộ cơ đ&uacute;c&nbsp;bằng phương ph&aacute;p đ&uacute;c &aacute;p lực được định h&igrave;nh giống nhau một c&aacute;ch chuẩn x&aacute;c nhất. D&ograve;ng Piano K được thiết kế đặc trưng với 3 gi&aacute; đỡ bằng nh&ocirc;m đ&uacute;c &aacute;p lực đ&atilde; vạch ra th&ecirc;m một thước đo cho độ vững chắc v&agrave; t&iacute;nh ổn định của bộ m&aacute;y cơ; qua đ&oacute; đ&aacute;nh gi&aacute; được t&iacute;nh chuẩn x&aacute;c của độ cảm ứng ph&iacute;m theo thời gian.&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>Độ d&agrave;i ph&iacute;m đ&agrave;n piano Kawai K-300&nbsp;được tăng th&ecirc;m</strong></p>\r\n\r\n<p>Độ d&agrave;i ph&iacute;m được tăng th&ecirc;m nhằm gi&uacute;p cho việc bấm ph&iacute;m dễ d&agrave;ng hơn v&agrave; thậm ch&iacute; tạo ra độ phản ứng tốt hơn từ gốc trong đến cạnh ngo&agrave;i của bề mặt hoạt động của ph&iacute;m đ&agrave;n.&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<h3><strong>2. &Acirc;m thanh đ&agrave;n piano Kawai K300</strong></h3>\r\n\r\n<p><strong>Bảng cộng hưởng (soundboard) được l&agrave;m bằng gỗ v&acirc;n sam nguy&ecirc;n tấm gọt thon</strong></p>\r\n\r\n<p>Bản cộng hưởng l&agrave; tr&aacute;i tim của piano. Bản cộng hưởng c&oacute; chức năng chuyển đổi c&aacute;c rung động của d&acirc;y đ&agrave;n th&agrave;nh những &acirc;m thanh đầy đặn v&agrave; vang rền. Kawai chỉ sử dụng những thớ gỗ vẫn sam thẳng th&oacute;m v&agrave; rắn chắc (được xẻ l&agrave;m bốn) cho bản cộng hưởng của d&ograve;ng K Series. Từng thớ gỗ sẽ được b&agrave;o thoải dần một c&aacute;ch tinh tế để tối đa h&oacute;a khả năng khuyếch đại &acirc;m thanh. Chỉ c&oacute; những thớ gỗ đ&aacute;p ứng được hoặc vượt qua c&aacute;c ti&ecirc;u chuẩn khắt khe mới được lựa chọn để sử dụng cho những c&acirc;y đ&agrave;n piano chuy&ecirc;n nghiệp của Kawai.</p>\r\n', 30, 10),
(16, 'Piano', 'Đàn Piano Upright Yamaha U1J PE', '9500000', 'Yamaha', 5, '../view/images/products/1452846052385-8055785.jpg', '<p>- Thiết kế cực k&igrave; nhỏ gọn v&agrave; di động.</p>\r\n\r\n<p>- 88 ph&iacute;m đ&agrave;n nặng với bộ m&aacute;y Scaled Hammer Action II Keyboard.</p>\r\n\r\n<p>-&nbsp;Thay đổi t&ocirc;ng m&agrave;u tự', '<h3><strong>Thiết kế thon gọn với bề d&agrave;y chỉ 232 mm</strong></h3>\r\n\r\n<p>V&agrave;i năm trở lại đ&acirc;y, c&aacute;c c&acirc;y piano điện mỏng nhẹ v&agrave; nhỏ l&ecirc;n ng&ocirc;i, Casio đ&atilde; ph&aacute;t triển c&aacute;c d&ograve;ng sản phẩm nhỏ gọn của họ v&agrave; đang thực sự muốn tạo ra xu hướng mới, nơi sự nhỏ gọn trở th&agrave;nh t&acirc;m điểm. Casio CDP-S150 c&oacute; k&iacute;ch thước thậm ch&iacute; c&ograve;n nhỏ hơn c&aacute;c d&ograve;ng CDP trước đ&acirc;y. Đ&agrave;n piano nhỏ gọn n&agrave;y thuận tiện để bạn c&oacute; thể chơi ở bất cứ đ&acirc;u, theo phong c&aacute;ch của ri&ecirc;ng bạn.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<h3><strong>Thay đổi t&ocirc;ng m&agrave;u tự nhi&ecirc;n cho &acirc;m thanh piano đầy sống động</strong></h3>\r\n\r\n<p>Đi k&egrave;m với 10 &acirc;m thanh chất lượng cao được t&iacute;ch hợp sẵn, bao gồm cả grand piano. Bạn c&oacute; thể thể hiện sự kh&aacute;c biệt tinh tế trong t&ocirc;ng m&agrave;u bằng c&aacute;ch điều chỉnh lực nhấn ph&iacute;m.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<h3><strong>88 ph&iacute;m đ&agrave;n nặng với bộ m&aacute;y Scaled Hammer Action II Keyboard</strong></h3>\r\n\r\n<p>Để đảm bảo kỹ thuật ng&oacute;n ch&iacute;nh x&aacute;c ngay từ đầu, CDP-S150 sử dụng bộ m&aacute;y Scaled Hammer Action II ho&agrave;n to&agrave;n mới (với c&aacute;c ph&iacute;m đ&agrave;n m&ocirc; phỏng chất liệu gỗ mun v&agrave; ng&agrave; voi) cung cấp cảm gi&aacute;c chơi như những b&agrave;n ph&iacute;m của piano acoustic trong một chiếc đ&agrave;n&nbsp;mỏng đ&aacute;ng kinh ngạc.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<h3><strong>Hỗ trợ hoạt động pin</strong></h3>\r\n\r\n<p>Ngo&agrave;i năng lượng điện qua nguồn sạc, Casio CDP-S150 hỗ trợ hoạt động bằng pin, cho ph&eacute;p người chơi tự do thưởng thức biểu diễn ở bất cứ đ&acirc;u.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<h3><strong>Chế độ song tấu</strong></h3>\r\n\r\n<p>C&aacute;c ph&iacute;m ở b&ecirc;n tr&aacute;i v&agrave; b&ecirc;n phải b&agrave;n ph&iacute;m c&oacute; thể được đặt th&agrave;nh c&ugrave;ng một dải &acirc;m. Chế độ Duet n&agrave;y thuận tiện khi hai người chơi, chẳng hạn như phụ huynh v&agrave; trẻ em hoặc gi&aacute;o vi&ecirc;n v&agrave; học sinh, c&oacute; thể thực h&agrave;nh c&ugrave;ng nhau thật tiện lợi v&agrave; dễ d&agrave;ng.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<h3><strong>Đầu ghi MIDI</strong></h3>\r\n\r\n<p>Bạn c&oacute; thể ghi lại c&aacute;c buổi biểu diễn của ri&ecirc;ng bạn v&agrave;o bộ nhớ trong của CDP-S150. Ph&aacute;t lại c&aacute;c bản ghi n&agrave;y để tự đ&aacute;nh gi&aacute; hoặc chia sẻ với bạn b&egrave;, rất hữu &iacute;ch trong c&aacute;c b&agrave;i học hoặc c&aacute;c t&igrave;nh huống kh&aacute;c.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<h3><strong>Ứng dụng Chordana Play for Piano cung cấp cho người d&ugrave;ng thao t&aacute;c th&ocirc;ng minh v&agrave; dễ sử dụng</strong></h3>\r\n\r\n<p>Ứng dụng d&agrave;nh cho c&aacute;c thiết bị sử dụng hệ điều h&agrave;nh iOS v&agrave; Android, Chordana Play Piano sẽ hướng dẫn bạn chơi hơn 100 t&aacute;c phẩm nhạc classic kinh điển, b&ecirc;n cạnh đ&oacute; ứng dụng c&oacute; thể hoạt động như một bảng điều khiển cho c&acirc;y đ&agrave;n v&agrave; hỗ trợ đọc sheet nhạc định dạng PDF.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<ul>\r\n	<li>\r\n	<p><strong>Điều khiển Piano từ xa</strong></p>\r\n	</li>\r\n</ul>\r\n\r\n<p>Sử dụng thiết bị y&ecirc;u th&iacute;ch của bạn để điều chỉnh c&agrave;i đặt CDP-S series, chọn &Acirc;m v&agrave; hơn thế nữa.</p>\r\n\r\n<ul>\r\n	<li>\r\n	<p><strong>Tr&igrave;nh xem điểm PDF</strong></p>\r\n	</li>\r\n</ul>\r\n\r\n<p>Hiển thị điểm nhạc được lưu dưới dạng tệp PDF, với chuyển trang được k&iacute;ch hoạt bằng b&agrave;n đạp.</p>\r\n', 40, 30);
INSERT INTO `product` (`id`, `type`, `name`, `price`, `brand`, `rating`, `thumbnailurl`, `description`, `detail`, `inStock`, `soldAmount`) VALUES
(17, 'Drum', 'Trống Jazz Unibex JDP-0765B', '7000000', 'Yamaha', 5, '../view/images/products/144781619662-4925508.jpg', '<p>GI&Aacute; LẺ CHƯA BAO GỒM HARDWARE V&Agrave; CYMBAL</p>\r\n', '<p>L&agrave; sản phẩm đến từ Pearl - thương hiệu nổi tiếng tr&ecirc;n to&agrave;n thế giới trong lĩnh vực sản xuất trống v&agrave; nhạc cụ g&otilde;, Pearl DMP925SP/C c&oacute; gi&aacute; cả hợp l&yacute; cho 1 bộ trống biểu diễn chuy&ecirc;n nghiệp d&agrave;nh cho c&aacute;c nhạc c&ocirc;ng hay những tay trống bắt đầu bước v&agrave;o con đường luyện tập chuy&ecirc;n nghiệp.</p>\r\n\r\n<p>Pearl DMP925SP/C Decade Maple gồm 5 chiếc trống nổi bật c&oacute; lớp vỏ Maple mỏng được sơn m&agrave;i sắc n&eacute;t với c&aacute;c m&agrave;u sắc độc đ&aacute;o, khiến cho kh&ocirc;ng gian chơi trống của bạn trở n&ecirc;n sinh động hơn. Chất liệu gỗ Maple (gỗ phong) l&agrave; một loại gỗ chuy&ecirc;n dụng để sản xuất ra những d&ograve;ng trống cao cấp với độ cộng hưởng &acirc;m thanh tuyệt vời, cho dải tần &acirc;m thanh đầy đủ, &acirc;m thanh uy lực.</p>\r\n\r\n<h2>&nbsp;</h2>\r\n\r\n<p><strong>Vỏ trống</strong></p>\r\n\r\n<p>Pearl DMP925SP/C sở hữu lớp vỏ d&agrave;y 5.4mm, bằng gỗ Maple với c&ocirc;ng nghệ độc quyền Superior Shell tạo n&ecirc;n hộp cộng hưởng ho&agrave;n hảo. Việc chế tạo được tỉ mỉ từ kh&acirc;u chọn vật liệu đến việc sắp xếp c&aacute;c lớp gỗ chồng l&ecirc;n nhau gi&uacute;p cho bộ trống c&oacute; được sự cộng hưởng tối ưu với giai điệu ho&agrave;n hảo.</p>\r\n\r\n<p><strong>Trống SNARE</strong></p>\r\n\r\n<p>Pearl DMP925SP/C được x&acirc;y dựng với trống Snare DMP1455S/C 14 &quot;x 5,5&quot;. Loại trống Snare n&agrave;y được thiết kế tinh tế, chọn lọc chất liệu chất lượng cao để cho ra &acirc;m thanh tuyệt với với mức gi&aacute; phải chăng.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>C&ugrave;ng xem tiết mục biểu diễn của học vi&ecirc;n trường Việt Thương Music School:&nbsp;Đặng Ngọc Tuấn Anh</p>\r\n', 20, 20),
(18, 'Drum', 'Trống Jazz Pearl Roadshow RS525SC', '11000000', 'Yamaha', 4, '../view/images/products/1459234325585-2028627.jpg', '<p>- Mua h&agrave;ng trả g&oacute;p 0%, Bảo h&agrave;nh ch&iacute;nh h&atilde;ng</p>\r\n\r\n<p>- Thiết kế ấn tượng, đẹp mắt, thoải m&aacute;i cho người chơi</p>\r\n\r\n<p>- M&agrave;u sắc đa dạng để lựa chọn<', '<p>Pearl DMP925FP/C l&agrave; một trong những th&agrave;nh vi&ecirc;n được y&ecirc;u th&iacute;ch nhất trong gia đ&igrave;nh trống Pearl, bao gồm&nbsp;trống Bass&nbsp; 22&Prime;; trống Floor Tom&nbsp;14&Prime;; Snare&nbsp;14&Prime;;&nbsp;&nbsp;trống Tom 1&nbsp;10&Prime; cuối c&ugrave;ng l&agrave;&nbsp;Tom 2 :&nbsp;12&Prime;. Với vỏ được l&agrave;m từ gỗ maple c&ugrave;ng nhiều t&iacute;nh năng ti&ecirc;n tiến với lớp phủ b&oacute;ng bẩy đa m&agrave;u sắc, đem đến cho người chơi bộ trống tuyệt vời với mức gi&aacute; phải chăng.</p>\r\n', 40, 20),
(19, 'Drum', 'Trống Jazz Pearl Export EXL725SP/C', '12000000', 'Yamaha', 4, '../view/images/products/trong-co-stagg.jpg', '<p>- T&iacute;ch hợp th&ecirc;m c&ocirc;ng nghệ Vỏ Cao Cấp S.S.T. Superior Shell</p>\r\n\r\n<p>- Hệ Thống OPTI-LOC MOUNTING</p>\r\n\r\n<p>- Đa dạng&nbsp;lựa chọn trong bốn m&agrave;u sắc tuyệt đẹp</p>\r\n\r\n<p>-', '<h1><strong>Trống PEARL Export Lacquer EXL725SP Standard</strong>:<strong>&nbsp;Export h&ocirc;m nay - Huyền thoại ng&agrave;y mai!</strong></h1>\r\n\r\n<p>Trống PEARL Export Lacquer EXL725SP Standard l&agrave; t&ecirc;n tuổi m&agrave; mỗi tay trống đều biết đến. Sản phẩm đ&aacute;p ứng nhu cầu của h&agrave;ng ng&agrave;n nghệ sĩ trống bằng c&aacute;ch đưa chất lượng v&agrave; gi&aacute; trị đồng h&agrave;nh với nhau. Export Series giờ đ&atilde; đươc t&iacute;ch hợp th&ecirc;m c&ocirc;ng nghệ Vỏ Cao Cấp S.S.T. Superior Shell, tom-mount Opti-Loc, bộ hardware P830 với pedal P-930, v&agrave; c&aacute;c lựa chọn trong bốn m&agrave;u sắc tuyệt đẹp.</p>\r\n\r\n<ul>\r\n	<li>6 lớp vỏ Mahogany/Poplar Shells 7.5mm</li>\r\n	<li>Mặt snare REMO</li>\r\n	<li>Matching finish</li>\r\n	<li>NEL-55/C Lug</li>\r\n	<li>1.6mm Steel Hoops</li>\r\n	<li>TR-5052 Tension Rods</li>\r\n	<li>Lưới lọc SR-700</li>\r\n</ul>\r\n', 12, 10),
(20, 'Drum', 'Trống Jazz Pearl DMP925SP/C', '17000000', 'Yamaha', 5, '../view/images/products/trong-dien-medeli-55.jpg', '<p>- Mua h&agrave;ng trả g&oacute;p 0%, Bảo h&agrave;nh ch&iacute;nh h&atilde;ng</p>\r\n\r\n<p>- Thiết kế ấn tượng, đẹp mắt, thoải m&aacute;i cho người chơi</p>\r\n\r\n<p>- Hệ thống Pearl Opti-Loc Mounting</p', '<p><a href=\"https://vietthuong.vn/trong-pearl-export-725-standard.html\"><strong>Trống Pearl Export EXX725SP</strong></a><strong>&nbsp;- Huyền thoại của ng&agrave;y mai, h&atilde;y chơi Export ngay h&ocirc;m nay!</strong></p>\r\n\r\n<p>Bộ nhạc cụ đ&atilde; tạo ra h&agrave;ng ng&agrave;n huyền thoại trống đang trở lại, mở ra một thế giới ho&agrave;n to&agrave;n mới v&agrave; cải tiến. Bộ trống Export mới của Pearl được x&acirc;y dựng mạnh mẽ hơn bao giờ hết để mang đến sự bền bỉ vượt thời gian gi&uacute;p người chơi trống c&oacute; được một khởi đầu vững chắc.</p>\r\n', 70, 25),
(21, 'Guitar', 'Đàn Ukulele Soprano Màu Gỗ Đỏ', '350000', 'Yamaha', 4, '../view/images/products/dan-silent-guitar-slg110n.jpg', '<p>- Mặt trước (Top) l&agrave; gỗ th&ocirc;ng nguy&ecirc;n khối (Solid),</p>\r\n\r\n<p>- Mặt h&ocirc;ng v&agrave; lưng được l&agrave;m từ gỗ Mahogany</p>\r\n\r\n<p>-&nbsp;20 ph&iacute;m đ&agrave;n tr&ecirc;n ', '<p>C&acirc;y đàn Fender Ukelele U&rsquo;Uku Soprano mới - phong cách có t&ecirc;n từ ti&ecirc;́ng Hawai &ldquo;nhỏ nhắn&rdquo; (các c&acirc;y đàn soprano uke là loại đàn nhỏ nh&acirc;́t trong dòng đàn ukulele), nhưng ni&ecirc;̀m vui mà nó tạo ra với &acirc;m thanh tuy&ecirc;̣t vời và thi&ecirc;́t k&ecirc;́ trang nhã lại &ldquo;r&acirc;́t lớn&rdquo;</p>\r\n\r\n<p>Các đặc đi&ecirc;̉m bao g&ocirc;̀m th&acirc;n đàn toàn bằng g&ocirc;̃ mahogany với soundboard gia c&ocirc;́ cho &acirc;m sắc trong trẻo, c&acirc;̀n đàn g&ocirc;̃ mahogany 3 mảnh với bàn phím 12 phím g&ocirc;̃ rosewood, th&acirc;n đàn đen và fingerboard khép kín, hình dạng đ&acirc;̀u đàn Fender đ&ocirc;̣c đáo và logo vàng, các tuner open-gear bằng crom, lớp sơn satin tự nhi&ecirc;n mượt mà và các d&acirc;y đ&agrave;n Aquila&reg;.</p>\r\n', 40, 26);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `productreview`
--

CREATE TABLE `productreview` (
  `id` int(11) NOT NULL,
  `userid` int(11) NOT NULL,
  `productid` int(11) NOT NULL,
  `content` text COLLATE utf8_unicode_ci NOT NULL,
  `rating` float NOT NULL DEFAULT 0,
  `createdat` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `productreview`
--

INSERT INTO `productreview` (`id`, `userid`, `productid`, `content`, `rating`, `createdat`) VALUES
(4, 41, 21, 'Sản phẩm rất chất lượng', 0, '2020-12-30 10:31:35'),
(5, 41, 21, 'Giá cả phù hợp với sinh viên', 0, '2020-12-30 10:33:04');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `producttag`
--

CREATE TABLE `producttag` (
  `productid` int(11) NOT NULL,
  `tagid` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `producttag`
--

INSERT INTO `producttag` (`productid`, `tagid`) VALUES
(1, 3),
(2, 3),
(3, 3),
(5, 8);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `tag`
--

CREATE TABLE `tag` (
  `id` int(11) NOT NULL,
  `tagname` varchar(10) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `tag`
--

INSERT INTO `tag` (`id`, `tagname`) VALUES
(1, 'Piano'),
(2, 'Organ'),
(3, 'Guitar'),
(4, 'Trống'),
(8, 'Keyboard');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `firstname` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lastname` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `age` int(11) DEFAULT NULL,
  `address` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `gender` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `avatarurl` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `isAdmin` tinyint(1) DEFAULT NULL,
  `active` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `user`
--

INSERT INTO `user` (`id`, `firstname`, `lastname`, `age`, `address`, `gender`, `phone`, `email`, `password`, `avatarurl`, `isAdmin`, `active`) VALUES
(1, 'cm', 'k', 20, '111hcm', 'male', '123456789', 'aaa@aa.com', 'admin', NULL, 1, 1),
(2, 'Vinh', 'NT', 20, 'abcxyz', 'male', '12345', 'vinh@hcmut.edu.vn', 'vinh', NULL, 1, 1),
(3, 'Luu', 'Le', 20, 'abcxyzdef', 'male', '123456', 'luu@hcmut.edu.vn', 'luu', NULL, 1, 1),
(4, 'Khoi', 'Cao', 20, 'a', 'male', '0327174199', 'khoi@hcmut.edu.vn', 'khoi', NULL, 1, 1),
(5, 'Tho', 'Trinh', 20, 'ajpsoajfpjf', 'male', '0932420', 'tho@hcmut.edu.vn', 'tho', NULL, 1, 1),
(6, 'JAOdi', 'PFJA', 20, 'afijpsaf', 'female', 'fq39', 'pojpi@gmail.com', 'oajfpas', NULL, 0, 1),
(8, 'test', 'user', 25, 'q1 HCM', 'male', '03919592', 'testuser@gmai.com', '12345', '', 0, 1),
(39, 'Họ', 'Tên', 0, '', '', '', 'hoten@gmail.com', 'hovaten', '', 0, 1),
(40, 'Ho', 'Ten', 0, '', '', '', 'tenho@yahoo.com.vn', 'tenvaho', '', 0, 1),
(41, 'Lee', 'Nguyen', 20, '458 Hai Bà Trưng, Quận 1, TPHCM ', 'male', '0935123468', 'lenguyen@gmail.com', 'lenguyen', '1609256545-14117945_580803682092419_3356462267756261411_n.jpg', 0, 0),
(42, 'Nguyễn', 'Lê', 0, '', '', '', 'nguyenle@gmail.com', 'nguyenle', '', 0, 1),
(43, 'Lê', 'Văn', 0, '', '', '', 'levan@gmail.com', 'leevan', '', 0, 1),
(44, 'Nguyễn Văn', 'Aa', 0, '', '', '', 'nguyenvana@gmail.com', 'nguyenvana', '', 0, 1);

--
-- Chỉ mục cho các bảng đã đổ
--

--
-- Chỉ mục cho bảng `article`
--
ALTER TABLE `article`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_user_article` (`userid`);

--
-- Chỉ mục cho bảng `articlecomment`
--
ALTER TABLE `articlecomment`
  ADD PRIMARY KEY (`id`),
  ADD KEY `articleid` (`articleid`,`userid`),
  ADD KEY `fk_user_articlecomment` (`userid`);

--
-- Chỉ mục cho bảng `banner`
--
ALTER TABLE `banner`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `orderproduct`
--
ALTER TABLE `orderproduct`
  ADD PRIMARY KEY (`orderid`,`productid`),
  ADD KEY `orderid` (`orderid`,`productid`),
  ADD KEY `fk_product_orderproduct` (`productid`);

--
-- Chỉ mục cho bảng `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`),
  ADD KEY `fk_user_order` (`userid`);

--
-- Chỉ mục cho bảng `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`);

--
-- Chỉ mục cho bảng `productreview`
--
ALTER TABLE `productreview`
  ADD PRIMARY KEY (`id`),
  ADD KEY `userid` (`userid`,`productid`),
  ADD KEY `fk_product_productreview` (`productid`);

--
-- Chỉ mục cho bảng `producttag`
--
ALTER TABLE `producttag`
  ADD PRIMARY KEY (`productid`,`tagid`),
  ADD KEY `productid` (`productid`,`tagid`),
  ADD KEY `fk_tag_producttag` (`tagid`);

--
-- Chỉ mục cho bảng `tag`
--
ALTER TABLE `tag`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`);

--
-- Chỉ mục cho bảng `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`);

--
-- AUTO_INCREMENT cho các bảng đã đổ
--

--
-- AUTO_INCREMENT cho bảng `article`
--
ALTER TABLE `article`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT cho bảng `articlecomment`
--
ALTER TABLE `articlecomment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT cho bảng `banner`
--
ALTER TABLE `banner`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT cho bảng `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT cho bảng `product`
--
ALTER TABLE `product`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT cho bảng `productreview`
--
ALTER TABLE `productreview`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT cho bảng `tag`
--
ALTER TABLE `tag`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT cho bảng `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=45;

--
-- Các ràng buộc cho các bảng đã đổ
--

--
-- Các ràng buộc cho bảng `article`
--
ALTER TABLE `article`
  ADD CONSTRAINT `fk_user_article` FOREIGN KEY (`userid`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Các ràng buộc cho bảng `articlecomment`
--
ALTER TABLE `articlecomment`
  ADD CONSTRAINT `fk_article_articlecomment` FOREIGN KEY (`articleid`) REFERENCES `article` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_user_articlecomment` FOREIGN KEY (`userid`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Các ràng buộc cho bảng `orderproduct`
--
ALTER TABLE `orderproduct`
  ADD CONSTRAINT `fk_order_orderproduct` FOREIGN KEY (`orderid`) REFERENCES `orders` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_product_orderproduct` FOREIGN KEY (`productid`) REFERENCES `product` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Các ràng buộc cho bảng `orders`
--
ALTER TABLE `orders`
  ADD CONSTRAINT `fk_user_order` FOREIGN KEY (`userid`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Các ràng buộc cho bảng `productreview`
--
ALTER TABLE `productreview`
  ADD CONSTRAINT `fk_product_productreview` FOREIGN KEY (`productid`) REFERENCES `product` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_user_productreview` FOREIGN KEY (`userid`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Các ràng buộc cho bảng `producttag`
--
ALTER TABLE `producttag`
  ADD CONSTRAINT `fk_product_producttag` FOREIGN KEY (`productid`) REFERENCES `product` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_tag_producttag` FOREIGN KEY (`tagid`) REFERENCES `tag` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
